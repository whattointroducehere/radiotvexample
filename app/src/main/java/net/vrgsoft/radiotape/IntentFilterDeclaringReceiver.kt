package net.vrgsoft.radiotape

import android.content.IntentFilter

interface IntentFilterDeclaringReceiver {
    fun getIntentFilter() : IntentFilter
}
