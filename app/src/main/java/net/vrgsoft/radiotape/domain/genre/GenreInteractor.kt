package net.vrgsoft.radiotape.domain.genre

import net.vrgsoft.radiotape.data.model.Genre
import net.vrgsoft.radiotape.domain.base.BaseInteractor

class GenreInteractor(private val genreRepository: GenreRepository) : BaseInteractor() {

    fun getAllGenres(): List<Genre> = genreRepository.getAllGenres()

}