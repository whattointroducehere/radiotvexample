package net.vrgsoft.radiotape.domain.player_action

import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.pipeline.player_actions.*
import net.vrgsoft.radiotape.domain.base.BaseInteractor

class PlayerActionsInteractor(
        private val actions: PlayerActionHub
) : BaseInteractor(){
    fun sendIcyHeaderChange(icyData: IcyData){
        launch(parent = job) {
            actions.sendIcyHeaderChange(icyData)
        }
    }

    fun sendPlayPause(action: PlayPauseAction){
        launch(parent = job) {
            actions.sendPlayPause(action)
        }
    }

    fun sendNextPrevious(action: NextPreviousAction){
        launch(parent = job) {
            actions.sendNextPrevious(action)
        }
    }

    fun sendQualityChange(action: ChangeQuality){
        launch(parent = job) {
            actions.sendQualityChange(action)
        }
    }

    fun sendFavoriteChange(station: RadioStationDB){
        launch(parent = job) {
            actions.sendFavoriteChange(station)
        }
    }

    fun subscribeToIcyHeaderChange() = actions.subscribeToIcyHeaderChange()
    fun subscribeToFavoriteChange() = actions.subscribeToFavoriteChange()
    fun subscribeToQualityChange() = actions.subscribeToQualityChange()
    fun subscribeToNextPrevious() = actions.subscribeToNextPrevious()
    fun subscribeToPlayPause() = actions.subscribeToPlayPause()
}