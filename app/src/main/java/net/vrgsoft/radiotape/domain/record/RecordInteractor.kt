package net.vrgsoft.radiotape.domain.record

import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.pipeline.record.RecordHub
import net.vrgsoft.radiotape.data.pipeline.record.RecordState
import net.vrgsoft.radiotape.domain.base.BaseInteractor

class RecordInteractor(private val recordHub: RecordHub) : BaseInteractor(){
    fun sendRecordState(state: RecordState){
        launch(parent = job) { recordHub.sendRecordState(state) }
    }

    fun subscribeToRecordState() = recordHub.subscribeToRecordState()
}