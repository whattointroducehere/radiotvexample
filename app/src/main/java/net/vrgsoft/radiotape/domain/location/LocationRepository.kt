package net.vrgsoft.radiotape.domain.location

import net.vrgsoft.radiotape.data.model.City
import net.vrgsoft.radiotape.data.model.Country

interface LocationRepository {
    fun getAllCountries(): List<Country>

    fun getAllCities(): List<City>
}