package net.vrgsoft.radiotape.domain.stations

import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.model.City
import net.vrgsoft.radiotape.data.model.Country
import net.vrgsoft.radiotape.data.model.Genre
import net.vrgsoft.radiotape.data.model.InitialJsonObject

interface StationsRepository {

    fun getAllCountries(): List<Country>

    fun getAllCities(): List<City>

    fun getAllStations(): List<RadioStationDB>

    fun saveStations(stations: List<RadioStationDB>)

    fun saveInitialInfo(info: InitialJsonObject)

    fun getAllGenres(): List<Genre>
    fun getLastPlayedStation(): RadioStationDB?
    fun saveLastPlayedStationId(id: Long)
    fun updateStation(stationDB: RadioStationDB)
    fun getSavedStations():List<RadioStationDB>

    fun sendSearchEvent(str: String)
    val updateSearch : ConflatedBroadcastChannel<String>
}
