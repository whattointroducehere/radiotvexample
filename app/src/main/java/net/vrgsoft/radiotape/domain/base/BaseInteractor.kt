package net.vrgsoft.radiotape.domain.base

import kotlinx.coroutines.experimental.Job


abstract class BaseInteractor {
    protected val job = Job()

    fun dispose() = job.cancel()
}