package net.vrgsoft.radiotape.domain.settings

import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.pipeline.player_actions.Quality
import net.vrgsoft.radiotape.data.repository.SharedPreferencesStorage
import net.vrgsoft.radiotape.domain.base.BaseInteractor

class SettingsInteractor(
        private val preferences: SharedPreferencesStorage,
        val equalizerChangeEvent: ConflatedBroadcastChannel<String>
) : BaseInteractor() {

    fun saveQuality(quality: Quality) = preferences.saveQuality(quality.postfix)

    fun getQuality(): Quality = preferences.getQuality(Quality.MEDIUM)

    fun getEqualizerProperties() = preferences.getEqualizerProperties()

    fun setEqualizerProperties(properties: String) {
        preferences.setEqualizerProperties(properties)

        launch {
            equalizerChangeEvent.send(properties)
        }
    }

    fun isEqualizerEnabled(): Boolean = preferences.isEqualizerEnabled()

    fun isAudioCompressionEnabled(): Boolean = preferences.isAudioCompressionEnabled()

    fun setEqualizerEnabled(enabled: Boolean) {
        preferences.setEqualizerEnabled(enabled)
    }

    fun setAudioCompressionEnabled(enabled: Boolean) {
        preferences.setAudioCompressionEnabled(enabled)
    }

    fun getEqualizerPreamp() = preferences.getEqualizerPreamp()

    fun setEqualizerPreamp(value: Int) {
        preferences.setEqualizerPreamp(value)
    }

    fun disableFirstLaunchState() {
        preferences.disableFirstLaunchState()
    }

    fun setLastTimer(time: Long) {
        preferences.setLastTimer(time)
    }

    fun getLastTimerTime(): Long = preferences.getLastTimerTime()

    val isFirstLaunch: Boolean = preferences.isFirstLaunch()
}
