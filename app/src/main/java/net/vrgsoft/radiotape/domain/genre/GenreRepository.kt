package net.vrgsoft.radiotape.domain.genre

import net.vrgsoft.radiotape.data.model.Genre

interface GenreRepository {
    fun getAllGenres(): List<Genre>
}