package net.vrgsoft.radiotape.domain.filters

import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB

interface FiltersRepository {
    fun saveFilters(settingFiltersDB: SettingFiltersDB)

    fun loadFilters(): SettingFiltersDB

    val onFiltersUpdated: ConflatedBroadcastChannel<SettingFiltersDB>
}
