package net.vrgsoft.radiotape.domain.stations

import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.model.InitialJsonObject
import net.vrgsoft.radiotape.domain.base.BaseInteractor

class StationsInteractor(
        private val stationsRepository: StationsRepository
) : BaseInteractor() {
    fun getAllStations() = stationsRepository.getAllStations()

    fun saveStations(stations: List<RadioStationDB>) = stationsRepository.saveStations(stations)

    fun saveInitialInfo(info: InitialJsonObject) = stationsRepository.saveInitialInfo(info)

    fun getLastPlayedStation(): RadioStationDB? {
        return stationsRepository.getLastPlayedStation()
    }

    fun saveLastPlayedStationId(id: Long) {
        stationsRepository.saveLastPlayedStationId(id)
    }

    fun updateStation(stationDB: RadioStationDB) {
        stationsRepository.updateStation(stationDB)
    }

    fun getSavedStations() = stationsRepository.getSavedStations()

    fun sendSearchEvent(str: String) {
        stationsRepository.sendSearchEvent(str)
    }

    fun observeSearchChanges(consumer: (query: String) -> Unit) {
        launch {
            stationsRepository.updateSearch.consumeEach { consumer(it) }
        }
    }
}
