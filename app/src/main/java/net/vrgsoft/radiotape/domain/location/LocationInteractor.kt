package net.vrgsoft.radiotape.domain.location

import net.vrgsoft.radiotape.data.model.Country
import net.vrgsoft.radiotape.domain.base.BaseInteractor

class LocationInteractor(private val locationRepository: LocationRepository) : BaseInteractor() {
    fun getAllCountries(): List<Country> = locationRepository.getAllCountries()
}