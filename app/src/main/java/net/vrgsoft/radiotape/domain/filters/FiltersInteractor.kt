package net.vrgsoft.radiotape.domain.filters

import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB
import net.vrgsoft.radiotape.data.model.Country
import net.vrgsoft.radiotape.data.model.Genre
import net.vrgsoft.radiotape.domain.base.BaseInteractor

class FiltersInteractor(
        private val filtersRepository: FiltersRepository,
        private val filtersChangeEvent: ConflatedBroadcastChannel<Boolean>
) : BaseInteractor() {

    fun saveFilters(countries: List<Country>, genres: List<Genre>, notify: Boolean) {
        val settingFiltersDB = SettingFiltersDB.create(countries, genres)

        filtersRepository.saveFilters(settingFiltersDB)

        launch {
            filtersChangeEvent.send(notify)
        }
    }

    fun observeFiltersChanges() = filtersChangeEvent

    fun loadFilters(): SettingFiltersDB = filtersRepository.loadFilters()
}
