package net.vrgsoft.radiotape

import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.crashlytics.android.Crashlytics
import com.evernote.android.job.JobManager
import com.google.android.gms.analytics.GoogleAnalytics
import io.fabric.sdk.android.Fabric
import net.vrgsoft.radiotape.data.job.RadioJobCreator
import net.vrgsoft.radiotape.data.job.TimerJob
import net.vrgsoft.radiotape.di.appModule
import net.vrgsoft.radiotape.utils.logDebug
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton




/**
 * Created on 04.06.2018.
 */
const val SHARED_PREFERENCES_TAG = "radio_tape_pref"

class App : MultiDexApplication(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(appModule(this@App))

        bind<GoogleAnalytics>() with singleton { GoogleAnalytics.getInstance(this@App) }
    }
    override val kodeinTrigger = KodeinTrigger()

    private val googleAnalytics: GoogleAnalytics by instance()

    //    private val authInteractor: AuthInteractor by instance()
//    private val preferencesStorage: SharedPreferencesStorage by instance()

    override fun onCreate() {
        super.onCreate()
        kodeinTrigger.trigger()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        Fabric.with(this, Crashlytics())
        initGlobalFuel()
        JobManager.create(this).addJobCreator(RadioJobCreator(applicationContext))
        TimerJob.cancelTimer()

        val tracker = googleAnalytics.newTracker(R.xml.global_tracker)
        logDebug("googleAnalytics", "googleAnalytics is initialized: ${googleAnalytics.isInitialized}")
        logDebug("googleAnalytics", "tracker is initialized: ${tracker.isInitialized}")
    }

    private fun initGlobalFuel() {
//        val language = Locale.getDefault().language + "-" + Locale.getDefault().country
//        val headers = mutableMapOf("Accept-Language" to language)
//
//        preferencesStorage.saveApiToken("T35vrO4rdKthuEvjwN-wuecHdbuqU64YdnOnxnKncwErnguN")
//
//        if(!authInteractor.getApiToken().isNullOrBlank()){
//            headers["Authorization"] = "Bearer ${authInteractor.getApiToken()}"
//        }
//
//        FuelManager.instance.baseHeaders = headers
//
//        if(BuildConfig.DEBUG) {
//            FuelManager.instance.addRequestInterceptor(logRequest("HttpRequest"))
//            FuelManager.instance.addResponseInterceptor(logResponse("HttpResponse"))
//        }
    }
}
