package net.vrgsoft.radiotape.di

import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.App
import net.vrgsoft.radiotape.DB_NAME
import net.vrgsoft.radiotape.SHARED_PREFERENCES_TAG
import net.vrgsoft.radiotape.data.db.RadioDatabase
import net.vrgsoft.radiotape.data.db.dao.FiltersDao
import net.vrgsoft.radiotape.data.db.dao.GenreDao
import net.vrgsoft.radiotape.data.db.dao.LocationDao
import net.vrgsoft.radiotape.data.db.dao.RadioStationDao
import net.vrgsoft.radiotape.data.model.mapper.CityMapper
import net.vrgsoft.radiotape.data.model.mapper.CountryMapper
import net.vrgsoft.radiotape.data.model.mapper.GenreMapper
import net.vrgsoft.radiotape.data.model.mapper.SubgenreMapper
import net.vrgsoft.radiotape.data.pipeline.player_actions.IcyData
import net.vrgsoft.radiotape.data.pipeline.player_actions.PlayerActionHub
import net.vrgsoft.radiotape.data.pipeline.player_actions.PlayerActionHubImpl
import net.vrgsoft.radiotape.data.pipeline.record.RecordHub
import net.vrgsoft.radiotape.data.pipeline.record.RecordHubImpl
import net.vrgsoft.radiotape.data.repository.SharedPreferencesStorage
import net.vrgsoft.radiotape.data.repository.filters.FiltersRepositoryImpl
import net.vrgsoft.radiotape.data.repository.filters.FiltersStorageLocal
import net.vrgsoft.radiotape.data.repository.filters.FiltersStorageRemote
import net.vrgsoft.radiotape.data.repository.resources.ResourcesStorage
import net.vrgsoft.radiotape.data.repository.stations.StationsRepositoryImpl
import net.vrgsoft.radiotape.data.repository.stations.StationsStorageLocal
import net.vrgsoft.radiotape.data.repository.stations.StationsStorageRemote
import net.vrgsoft.radiotape.domain.filters.FiltersInteractor
import net.vrgsoft.radiotape.domain.filters.FiltersRepository
import net.vrgsoft.radiotape.domain.genre.GenreInteractor
import net.vrgsoft.radiotape.domain.genre.GenreRepository
import net.vrgsoft.radiotape.domain.location.LocationInteractor
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.domain.record.RecordInteractor
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsRepository
import net.vrgsoft.radiotape.utils.AppEqualizer
import org.kodein.di.Kodein
import org.kodein.di.android.androidModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import saschpe.exoplayer2.ext.icy.IcyHttpDataSourceFactory

fun appModule(context: App) = Kodein.Module("appModule") {
    bind<SharedPreferences>(SHARED_PREFERENCES_TAG) with singleton { context.getSharedPreferences(SHARED_PREFERENCES_TAG, android.content.Context.MODE_PRIVATE) }
    bind<RadioDatabase>() with singleton { Room.databaseBuilder(context, RadioDatabase::class.java, DB_NAME).fallbackToDestructiveMigration().build() }

    import(interactorsModule())
    import(storageModule())
    import(repositoryModule())
    import(dbModule())
    import(pipelineModule())
    import(mappersModule())
    import(exoPlayerModule())
    import(androidModule(context))
}

private fun exoPlayerModule() = Kodein.Module("exoPlayerModule") {
    bind<DefaultBandwidthMeter>() with provider { DefaultBandwidthMeter() }
    bind<TrackSelection.Factory>() with provider { AdaptiveTrackSelection.Factory(instance<DefaultBandwidthMeter>()) }
    bind<TrackSelector>() with provider { DefaultTrackSelector(instance<TrackSelection.Factory>()) }
    bind<IcyHttpDataSourceFactory>() with provider {
        IcyHttpDataSourceFactory.Builder(Util.getUserAgent(instance(), "kjhk31uwfgdgseyhfd5y1g"))
                .setIcyMetadataChangeListener { icyMetadata ->
                    instance<PlayerActionsInteractor>().sendIcyHeaderChange(IcyData(icyMetadata.streamTitle, icyMetadata.streamUrl))
                }
                .build()
    }
    bind<DefaultDataSourceFactory>() with singleton {
        DefaultDataSourceFactory(instance(), instance<DefaultBandwidthMeter>(), instance<IcyHttpDataSourceFactory>())
    }
    bind<ExtractorMediaSource.Factory>() with provider {
        ExtractorMediaSource.Factory(instance<DefaultDataSourceFactory>())
    }

    bind<SimpleExoPlayer>() with singleton {
        ExoPlayerFactory.newSimpleInstance(instance<Context>(), instance())
    }

    bind<AppEqualizer>() with singleton { AppEqualizer(instance()) }
}

private fun interactorsModule() = Kodein.Module("interactorsModule") {
    bind() from provider { StationsInteractor(instance()) }
    bind() from provider { PlayerActionsInteractor(instance()) }
    bind() from provider { SettingsInteractor(instance(), instance("equalizerChangeEvent")) }
    bind() from provider { LocationInteractor(instance()) }
    bind() from provider { GenreInteractor(instance()) }
    bind() from provider { RecordInteractor(instance()) }
    bind() from provider { FiltersInteractor(instance(), instance("filtersChangeEvent")) }
}

private fun repositoryModule() = Kodein.Module("repositoryModule") {
    bind<StationsRepositoryImpl>() with singleton {
        StationsRepositoryImpl(instance(), instance(), instance(),
                instance(), instance(), instance(), instance())
    }

    bind<StationsRepository>() with singleton { instance<StationsRepositoryImpl>() }

    bind<GenreRepository>() with singleton { instance<StationsRepositoryImpl>() }

    bind<FiltersRepository>() with singleton { FiltersRepositoryImpl(instance(), instance()) }
}

private fun mappersModule() = Kodein.Module("mappersModule") {
    bind() from provider { CityMapper() }
    bind() from provider { CountryMapper() }
    bind() from provider { GenreMapper() }
    bind() from provider { SubgenreMapper() }
}

private fun storageModule() = Kodein.Module("storageModule") {
    import(remoteStorageModule())
    import(localStorageModule())
    bind<SharedPreferencesStorage>() with singleton { SharedPreferencesStorage(instance()) }
    bind<ResourcesStorage>() with singleton { ResourcesStorage(instance()) }
}

private fun remoteStorageModule() = Kodein.Module("remoteStorageModule") {
    bind() from singleton { StationsStorageRemote() }
    bind() from singleton { FiltersStorageRemote() }
}

private fun localStorageModule() = Kodein.Module("localStorageModule") {
    bind() from singleton { StationsStorageLocal(instance(), instance(), instance()) }
    bind() from singleton { FiltersStorageLocal(instance()) }
}

private fun dbModule() = Kodein.Module("dbModule") {
    bind<RadioStationDao>() with provider { instance<RadioDatabase>().radioStationDao() }
    bind<LocationDao>() with provider { instance<RadioDatabase>().locationDao() }
    bind<GenreDao>() with provider { instance<RadioDatabase>().genreDao() }
    bind<FiltersDao>() with provider { instance<RadioDatabase>().filtersDao() }
}

private fun pipelineModule() = Kodein.Module("pipelineModule") {
    bind<PlayerActionHub>() with singleton { PlayerActionHubImpl() }
    bind<RecordHub>() with singleton { RecordHubImpl() }
    bind<ConflatedBroadcastChannel<String>>("equalizerChangeEvent") with singleton { ConflatedBroadcastChannel<String>() }
    bind<ConflatedBroadcastChannel<Boolean>>("filtersChangeEvent") with singleton { ConflatedBroadcastChannel<Boolean>() }
    bind<BroadcastChannel<Boolean>>("startDragEvent") with singleton { BroadcastChannel<Boolean>(1) }
}
