package net.vrgsoft.radiotape

const val DEFAULT_PAGE_SIZE = 20

//const val BASE_URL = "http://root.vrgsoft.net/welness-web/web/api/v1"

const val DB_NAME = "DB_RadioTape"

const val ANIMATION_DURATION = 400L
const val TEMP_FILE_NAME = "temp.mp3"

const val DB_VERSION  = 9