package net.vrgsoft.radiotape.data.db.dao

import android.arch.persistence.room.*
import net.vrgsoft.radiotape.data.db.model.RadioStationDB

@Dao
interface RadioStationDao {
    @Query("SELECT * FROM RadioStationDB")
    fun getAll(): List<RadioStationDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(stations: List<RadioStationDB>)

    @Query("SELECT * FROM RadioStationDB WHERE id = :id")
    fun getStationById(id: Long): RadioStationDB?

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateStation(stationDB: RadioStationDB)

    @Query("SELECT * FROM RadioStationDB WHERE isFavorite")
    fun getSavedStations(): List<RadioStationDB>
}