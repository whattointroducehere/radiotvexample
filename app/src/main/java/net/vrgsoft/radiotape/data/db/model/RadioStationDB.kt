package net.vrgsoft.radiotape.data.db.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import net.vrgsoft.radiotape.data.model.Filtrable.Companion.ID_ALL
import net.vrgsoft.radiotape.utils.hasIntersect

@Entity
class RadioStationDB(
        @PrimaryKey var id: Long,
        var uid: Long,
        var stream: String,
        var recomended: Int = 0,
        var created: String,
        var logo: String,
        @SerializedName("country_id")
        var countryId: Long,
        @SerializedName("cities_ids")
        var citiesIds: MutableList<Long>,
        @SerializedName("genres_ids")
        var genresIds: MutableList<Long>,
        @SerializedName("subgenres_ids")
        var subgenresIds: MutableList<Long>,
        var name: String,
        @SerializedName("descr")
        var description: String? = null,
        var isFavorite: Boolean = false
) {
    fun filter(filters: SettingFiltersDB): Boolean {
        val countriesId = filters.countriesId
        val genreIds = filters.genreIds

        if (!(countriesId.contains(ID_ALL) || countriesId.contains(countryId))) {
            if (!citiesIds.hasIntersect(filters.cityIds)) {
                return false
            }
        }

        if (!(genreIds.contains(ID_ALL) || genresIds.hasIntersect(genreIds))) {
            if (!subgenresIds.hasIntersect(filters.subgenreIds)) {
                return false
            }
        }

        return true
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RadioStationDB

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}


