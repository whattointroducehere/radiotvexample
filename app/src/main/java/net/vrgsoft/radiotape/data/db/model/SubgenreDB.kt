package net.vrgsoft.radiotape.data.db.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class SubgenreDB(@PrimaryKey var id: Long, var name: String)