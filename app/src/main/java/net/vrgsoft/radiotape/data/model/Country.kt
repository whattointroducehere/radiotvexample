package net.vrgsoft.radiotape.data.model

class Country(override var id: Long,
              override var name: String = "",
              var cityList: List<City> = listOf(),
              override var checked: Boolean = false) : Filtrable {
    override var expanded: Boolean = false

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Country

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getChild(): List<Filtrable> = cityList

}
