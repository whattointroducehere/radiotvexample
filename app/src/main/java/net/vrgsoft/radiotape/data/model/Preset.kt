package net.vrgsoft.radiotape.data.model

data class Preset(
        val position: Int,
        val title: String
) {
    override fun toString(): String {
        return title
    }
}