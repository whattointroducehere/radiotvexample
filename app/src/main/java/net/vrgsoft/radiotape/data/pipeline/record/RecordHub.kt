package net.vrgsoft.radiotape.data.pipeline.record

import kotlinx.coroutines.experimental.channels.BroadcastChannel

interface RecordHub {

    suspend fun sendRecordState(state: RecordState)
    fun subscribeToRecordState(): BroadcastChannel<RecordState>
}
