package net.vrgsoft.radiotape.data.repository.resources

import android.content.res.Resources
import net.vrgsoft.radiotape.R

class ResourcesStorage(
        private val resources: Resources
) {

    fun getAllCountriesLabel(): String = resources.getString(R.string.all_countries_lbl)

    fun getAllCitiesLabel(): String = resources.getString(R.string.all_cities_lbl)

    fun getAllGenresHeader(): String = resources.getString(R.string.all_genres_lbl)

    fun getAllSubgenresHeader(): String = resources.getString(R.string.all_subgenres_lbl)

    fun getManualPresetString(): String = resources.getString(R.string.manual_preset_title)

}