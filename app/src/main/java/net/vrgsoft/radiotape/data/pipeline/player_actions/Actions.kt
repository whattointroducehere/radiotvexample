package net.vrgsoft.radiotape.data.pipeline.player_actions

import net.vrgsoft.radiotape.data.db.model.RadioStationDB

interface PlayPauseAction
interface NextPreviousAction

class Play(val station: RadioStationDB?) : PlayPauseAction
class Pause(val station: RadioStationDB?) : PlayPauseAction
class Next : NextPreviousAction
class Previous : NextPreviousAction
class ChangeQuality(val quality: Quality)
class IcyData(val streamTitle: String?, val streamUrl: String?)

enum class Quality(val postfix: String) {
    LOW("-low"), MEDIUM("-med"), HIGH("-hi");

    companion object {
        fun byValue(postfix: String) = when (postfix) {
            "-low" -> Quality.LOW
            "-med" -> Quality.MEDIUM
            "-hi" -> Quality.HIGH
            else -> throw RuntimeException("wrong quality postfix")
        }
    }
}