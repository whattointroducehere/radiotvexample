package net.vrgsoft.radiotape.data.db.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

@Entity(primaryKeys = ["genreId", "subgenreId"],
        foreignKeys = [
            (ForeignKey(entity = GenreDB::class, parentColumns = ["id"], childColumns = ["genreId"])),
            (ForeignKey(entity = SubgenreDB::class, parentColumns = ["id"], childColumns = ["subgenreId"]))
        ])
class GenreSubgenreDB(var genreId: Long,
                      var subgenreId: Long)