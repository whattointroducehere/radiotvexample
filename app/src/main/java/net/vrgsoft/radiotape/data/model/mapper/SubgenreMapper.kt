package net.vrgsoft.radiotape.data.model.mapper

import net.vrgsoft.radiotape.data.db.model.SubgenreDB
import net.vrgsoft.radiotape.data.model.Subgenre

class SubgenreMapper {
    fun map(genre: SubgenreDB, genreId: Long): Subgenre {
        return Subgenre(genre.id, genre.name, genreId)
    }

    fun map(dbList: List<SubgenreDB>, genreId: Long): List<Subgenre> {
        val list = mutableListOf<Subgenre>()
        for (genreDB in dbList) {
            list.add(map(genreDB, genreId))
        }

        return list
    }
}