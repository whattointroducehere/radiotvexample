package net.vrgsoft.radiotape.data.pipeline.player_actions

import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.db.model.RadioStationDB

interface PlayerActionHub {

    fun subscribeToQualityChange(): BroadcastChannel<ChangeQuality>
    fun subscribeToNextPrevious(): BroadcastChannel<NextPreviousAction>
    fun subscribeToPlayPause(): ConflatedBroadcastChannel<PlayPauseAction>
    fun subscribeToIcyHeaderChange(): ConflatedBroadcastChannel<IcyData>
    fun subscribeToFavoriteChange(): BroadcastChannel<RadioStationDB>

    suspend fun sendPlayPause(action: PlayPauseAction)
    suspend fun sendNextPrevious(action: NextPreviousAction)
    suspend fun sendQualityChange(action: ChangeQuality)
    suspend fun sendFavoriteChange(station: RadioStationDB)
    suspend fun sendIcyHeaderChange(icyData: IcyData)
}