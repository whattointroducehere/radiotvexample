package net.vrgsoft.radiotape.data.db.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class CountryDB(
        @PrimaryKey var id: Long,
        var name: String)