package net.vrgsoft.radiotape.data.job

import android.content.Context
import com.evernote.android.job.Job
import com.evernote.android.job.JobManager
import com.evernote.android.job.JobRequest
import net.vrgsoft.radiotape.data.pipeline.player_actions.Pause
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.presentation.service.PlayerService
import org.kodein.di.Kodein
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance


const val TIMER_JOB_TAG = "timerJobTag"

class TimerJob(appContext:Context) : Job() {

    private val _parentKodein: Kodein by closestKodein(appContext)
    private val actionsInteractor: PlayerActionsInteractor by _parentKodein.instance()
    private val stationInteractor: StationsInteractor by _parentKodein.instance()

    override fun onRunJob(params: Job.Params): Result {
        actionsInteractor.sendPlayPause(Pause(stationInteractor.getLastPlayedStation()))
        return Result.SUCCESS
    }

    companion object {
        fun scheduleTimer(minutes: Int) {
            JobRequest.Builder(TIMER_JOB_TAG)
                    .setExact(minutes * 60L * 1000L)
                    .setUpdateCurrent(true)
                    .build()
                    .schedule()
        }

        fun cancelTimer() {
            JobManager.instance().cancelAllForTag(TIMER_JOB_TAG)
        }
    }
}