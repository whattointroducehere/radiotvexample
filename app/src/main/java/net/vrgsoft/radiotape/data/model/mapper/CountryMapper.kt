package net.vrgsoft.radiotape.data.model.mapper

import net.vrgsoft.radiotape.data.db.model.CountryDB
import net.vrgsoft.radiotape.data.model.Country

class CountryMapper {
    fun map(countryDB: CountryDB): Country {
        return Country(countryDB.id, countryDB.name)
    }

    fun map(countryDBList: List<CountryDB>): List<Country> {
        val list = mutableListOf<Country>()
        for (countryDB in countryDBList) {
            list.add(map(countryDB))
        }

        return list
    }
}