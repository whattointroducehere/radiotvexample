package net.vrgsoft.radiotape.data.pipeline.record

import kotlinx.coroutines.experimental.channels.BroadcastChannel

class RecordHubImpl : RecordHub {
    private val recordState = BroadcastChannel<RecordState>(1)

    override fun subscribeToRecordState() = recordState

    override suspend fun sendRecordState(state: RecordState){
        recordState.send(state)
    }
}
