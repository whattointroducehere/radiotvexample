package net.vrgsoft.radiotape.data.pipeline.record

interface RecordState

class RecordStarted : RecordState
class RecordFinished(val duration: Long, val filePath: String) : RecordState