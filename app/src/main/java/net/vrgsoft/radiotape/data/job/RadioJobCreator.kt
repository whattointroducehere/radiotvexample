package net.vrgsoft.radiotape.data.job

import android.content.Context
import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator

class RadioJobCreator(var appContext:Context) : JobCreator {
    override fun create(tag: String): Job? =
            when (tag) {
                TIMER_JOB_TAG -> TimerJob(appContext)
                else -> null
            }
}