package net.vrgsoft.radiotape.data.repository.stations

import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.model.City
import net.vrgsoft.radiotape.data.model.Country
import net.vrgsoft.radiotape.data.model.Genre
import net.vrgsoft.radiotape.data.model.InitialJsonObject
import net.vrgsoft.radiotape.data.model.mapper.CityMapper
import net.vrgsoft.radiotape.data.model.mapper.CountryMapper
import net.vrgsoft.radiotape.data.model.mapper.GenreMapper
import net.vrgsoft.radiotape.data.model.mapper.SubgenreMapper
import net.vrgsoft.radiotape.data.repository.SharedPreferencesStorage
import net.vrgsoft.radiotape.domain.genre.GenreRepository
import net.vrgsoft.radiotape.domain.location.LocationRepository
import net.vrgsoft.radiotape.domain.stations.StationsRepository

class StationsRepositoryImpl(
        private val stationsRemote: StationsStorageRemote,
        private val stationsLocal: StationsStorageLocal,
        private val sharedPreferencesStorage: SharedPreferencesStorage,
        private val countryMapper: CountryMapper,
        private val cityMapper: CityMapper,
        private val genreMapper: GenreMapper,
        private val subgenreMapper: SubgenreMapper
) : StationsRepository, GenreRepository, LocationRepository {

    override val updateSearch = ConflatedBroadcastChannel<String>()

    override fun getAllCountries(): List<Country> {
        val allCountriesDB = stationsLocal.getAllCountries()

        val countries: List<Country> = countryMapper.map(allCountriesDB)

        for (country in countries) {
            val citiesForCountryDB = stationsLocal.getCitiesForCountry(country.id)
            val cityList = cityMapper.map(citiesForCountryDB, country.id)
            country.cityList = cityList.sortedBy { it.name }
        }

        return countries.sortedBy { it.name }
    }

    override fun getAllCities(): List<City> {
        return cityMapper.map(stationsLocal.getAllCities(), -1).sortedBy { it.name }
    }

    override fun getAllStations(): List<RadioStationDB> = stationsLocal.getAllStations()

    override fun saveStations(stations: List<RadioStationDB>) = stationsLocal.insertStations(stations)

    override fun saveInitialInfo(info: InitialJsonObject) = stationsLocal.saveInitialInfo(info)

    override fun getAllGenres(): List<Genre> {
        val genresDB = stationsLocal.getAllGenres()
        val genres = genreMapper.map(genresDB)

        for (genre in genres) {
            val subgenreDB = stationsLocal.getSubgenres(genre.id)
            genre.subganre = subgenreMapper.map(subgenreDB, genre.id).sortedBy { it.name }
        }

        return genres.sortedBy { it.name }
    }

    override fun getLastPlayedStation(): RadioStationDB? {
        val id = sharedPreferencesStorage.getLastPlayedStationId()
        return stationsLocal.getStationById(id)
    }

    override fun saveLastPlayedStationId(id: Long) {
        sharedPreferencesStorage.saveLastPlayedStationId(id)
    }

    override fun updateStation(stationDB: RadioStationDB) {
        stationsLocal.updateStation(stationDB)
    }

    override fun getSavedStations(): List<RadioStationDB> {
        return stationsLocal.getSavedStations()
    }

    override fun sendSearchEvent(str: String) {
        launch {
            updateSearch.send(str)
        }
    }

}
