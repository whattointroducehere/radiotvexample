package net.vrgsoft.radiotape.data.pipeline.player_actions

import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.db.model.RadioStationDB


class PlayerActionHubImpl : PlayerActionHub {
    private val playPause = ConflatedBroadcastChannel<PlayPauseAction>()
    private val icyHeaderChange = ConflatedBroadcastChannel<IcyData>()
    private val nextPrevious = BroadcastChannel<NextPreviousAction>(1)
    private val qualityChange = BroadcastChannel<ChangeQuality>(1)
    private val favoriteChange = BroadcastChannel<RadioStationDB>(1)

    override fun subscribeToPlayPause() = playPause
    override fun subscribeToNextPrevious() = nextPrevious
    override fun subscribeToQualityChange() = qualityChange
    override fun subscribeToFavoriteChange() = favoriteChange
    override fun subscribeToIcyHeaderChange() = icyHeaderChange

    override suspend fun sendPlayPause(action: PlayPauseAction) {
        playPause.send(action)
    }

    override suspend fun sendNextPrevious(action: NextPreviousAction) {
        nextPrevious.send(action)
    }

    override suspend fun sendQualityChange(action: ChangeQuality) {
        qualityChange.send(action)
    }

    override suspend fun sendFavoriteChange(station: RadioStationDB) {
        favoriteChange.send(station)
    }

    override suspend fun sendIcyHeaderChange(icyData: IcyData) {
        icyHeaderChange.send(icyData)
    }
}