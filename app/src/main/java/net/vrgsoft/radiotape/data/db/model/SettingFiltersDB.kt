package net.vrgsoft.radiotape.data.db.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import net.vrgsoft.radiotape.data.model.*
import net.vrgsoft.radiotape.data.model.Filtrable.Companion.ID_ALL

@Entity
data class SettingFiltersDB(var countriesId: List<Long> = listOf(-1),
                            var cityIds: List<Long> = listOf(),
                            var genreIds: List<Long> = listOf(-1),
                            var subgenreIds: List<Long> = listOf()) {
    @PrimaryKey
    var id = DEFAULT_SETTINGS_ID

    companion object {
        const val DEFAULT_SETTINGS_ID = 666L

        fun create(countries: List<Country>, genres: List<Genre>): SettingFiltersDB {
            val countriesId: MutableList<Long> = mutableListOf()
            val cityIds: MutableList<Long> = mutableListOf()
            val genreIds: MutableList<Long> = mutableListOf()
            val subgenreIds: MutableList<Long> = mutableListOf()

            fillCheckedFilters(countries, countriesId, cityIds, Country(ID_ALL), City(ID_ALL))
            fillCheckedFilters(genres, genreIds, subgenreIds, Genre(ID_ALL), Subgenre(ID_ALL))

            return SettingFiltersDB(countriesId, cityIds, genreIds, subgenreIds)
        }

        private fun <T : Filtrable> fillCheckedFilters(mainItems: List<T>,
                                                       mainChecked: MutableList<Long>,
                                                       subChecked: MutableList<Long>,
                                                       mainAddAllItem: Filtrable,
                                                       subAddAllItem: Filtrable) {

            val mainAllIndex = mainItems.indexOf(mainAddAllItem)

            if (mainAllIndex >= 0 && mainItems[mainAllIndex].checked) {
                mainChecked.add(ID_ALL)
            } else {
                mainItems.forEach { mainItem ->

                    val childList = mainItem.getChild()

                    if (childList != null) {
                        val subAllIndex = childList.indexOf(subAddAllItem)

                        if (subAllIndex >= 0 && childList[subAllIndex].checked) {
                            mainChecked.add(mainItem.id)
                        } else {
                            var allSubChecked = childList.isEmpty().not()

                            childList.forEach {
                                allSubChecked = allSubChecked && it.checked

                                if (it.checked) {
                                    subChecked.add(it.id)
                                }
                            }

                            if (allSubChecked) {
                                mainChecked.add(mainItem.id)
                            }
                        }
                    }
                }
            }
        }

        fun applyFilters(mainFilter: List<Filtrable>,
                         mainIds: List<Long>,
                         subFilterIds: List<Long>,
                         dispList: MutableList<Filtrable>? = null) {

            if (mainIds.contains(Filtrable.ID_ALL)) {
                // check all main and sub
                mainFilter.forEach { country ->
                    country.checked = true
                    dispList?.add(country)
                    country.getChild()?.forEach { it.checked = true }
                }
            } else {
                var mainAllChecked = mainIds.isEmpty().not()

                mainFilter.forEach { mainItem ->
                    val containsInMain = mainIds.contains(mainItem.id)
                    mainAllChecked = mainAllChecked && (containsInMain || mainItem.id == ID_ALL)

                    if (containsInMain) { // check main + all sub
                        mainItem.getChild()?.forEach { it.checked = true }
                        mainItem.checked = true
                        dispList?.add(mainItem)
                    } else {
                        val tmpDispList = mutableListOf<Filtrable>()
                        var allSubChecked = mainItem.getChild()?.isEmpty()?.not() ?: false

                        mainItem.getChild()?.forEach {
                            val contains = subFilterIds.contains(it.id)
                            allSubChecked = allSubChecked && (contains || it.id == ID_ALL)

                            it.checked = contains // check sub
                            if (contains) {
                                tmpDispList.add(it)
                            }
                        }

                        if (allSubChecked) {
                            dispList?.add(mainItem)
                        } else {
                            dispList?.addAll(tmpDispList)
                        }
                    }
                }
            }
        }

    }
}
