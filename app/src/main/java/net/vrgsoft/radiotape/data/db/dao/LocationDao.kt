package net.vrgsoft.radiotape.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import net.vrgsoft.radiotape.data.db.model.CityDB
import net.vrgsoft.radiotape.data.db.model.CountryCityDB
import net.vrgsoft.radiotape.data.db.model.CountryDB
import net.vrgsoft.radiotape.data.db.model.SubgenreDB

@Dao
interface LocationDao {
    @Query("SELECT * FROM CityDB")
    fun getAllCities(): List<CityDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCities(cities: List<CityDB>)

    @Query("SELECT * FROM CountryDB")
    fun getAllCountries(): List<CountryDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCountries(cities: List<CountryDB>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCountryCity(countryCity: List<CountryCityDB>)

    @Query("SELECT * FROM CityDB WHERE CityDb.id IN (SELECT cityId FROM CountryCityDB WHERE countryId = :country_id)")
    fun getCitiesForCountry(country_id: Long): List<CityDB>

    @Query("SELECT * FROM SubgenreDB WHERE SubgenreDB.id IN (SELECT GenreSubgenreDB.subgenreId FROM GenreSubgenreDB WHERE GenreSubgenreDB.genreId = :id)")
    fun getSubgenres(id: Long): List<SubgenreDB>

}
