package net.vrgsoft.radiotape.data.model

import net.vrgsoft.radiotape.data.db.model.*

open class InitialJsonObject(
        var countries: List<CountryDB> = listOf(),
        var cities: List<CityDB> = listOf(),
        var genres: List<GenreDB> = listOf(),
        var subgenres: List<SubgenreDB> = listOf(),
        var stations: List<RadioStationDB> = listOf(),
        var countryCities: MutableList<CountryCityDB> = mutableListOf(),
        var genreSubgenre: MutableList<GenreSubgenreDB> = mutableListOf()
)