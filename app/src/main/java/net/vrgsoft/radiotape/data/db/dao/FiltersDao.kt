package net.vrgsoft.radiotape.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB.Companion.DEFAULT_SETTINGS_ID

@Dao
interface FiltersDao {
    @Query("SELECT * FROM SettingFiltersDB WHERE id = $DEFAULT_SETTINGS_ID")
    fun getSettings(): SettingFiltersDB?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun update(filter: SettingFiltersDB)
}
