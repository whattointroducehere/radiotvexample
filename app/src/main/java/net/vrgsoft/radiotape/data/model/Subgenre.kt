package net.vrgsoft.radiotape.data.model

open class Subgenre(override var id: Long,
                    override var name: String = "",
                    var genreId: Long = -1,
                    override var checked: Boolean = false) : Filtrable {

    override var expanded: Boolean = false

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Subgenre

        if (id != other.id) return false

        return true
    }

    override fun getParentId(): Long {
        return genreId
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getChild(): List<Filtrable>? = null
}
