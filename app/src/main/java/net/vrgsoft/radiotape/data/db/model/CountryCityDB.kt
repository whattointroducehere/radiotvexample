package net.vrgsoft.radiotape.data.db.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

@Entity(primaryKeys = ["countryId", "cityId"],
        foreignKeys = [
            ForeignKey(entity = CountryDB::class, parentColumns = ["id"], childColumns = ["countryId"]),
            ForeignKey(entity = CityDB::class, parentColumns = ["id"], childColumns = ["cityId"])
        ]
)
data class CountryCityDB(
        val countryId: Long,
        val cityId: Long)