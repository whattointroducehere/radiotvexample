package net.vrgsoft.radiotape.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import net.vrgsoft.radiotape.data.db.model.GenreDB
import net.vrgsoft.radiotape.data.db.model.GenreSubgenreDB
import net.vrgsoft.radiotape.data.db.model.SubgenreDB

@Dao
interface GenreDao {
    @Query("SELECT * FROM GenreDB")
    fun getAllGenres(): List<GenreDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllGenres(cities: List<GenreDB>)

    @Query("SELECT * FROM SubgenreDB")
    fun getAllSubGenres(): List<SubgenreDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllSubGenres(cities: List<SubgenreDB>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllGenreSubgenre(genreSubgenre: List<GenreSubgenreDB>)
}