package net.vrgsoft.radiotape.data.model

data class Band(
        val position: Int,
        val centerRange: Int,
        var level: Int
)
