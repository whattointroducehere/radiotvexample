package net.vrgsoft.radiotape.data.model.mapper

import net.vrgsoft.radiotape.data.db.model.GenreDB
import net.vrgsoft.radiotape.data.model.Genre

class GenreMapper {
    fun map(genre: GenreDB): Genre {
        return Genre(genre.id, genre.name, mutableListOf())
    }

    fun map(dbList: List<GenreDB>): List<Genre> {
        val list = mutableListOf<Genre>()
        for (genreDB in dbList) {
            list.add(map(genreDB))
        }

        return list
    }
}