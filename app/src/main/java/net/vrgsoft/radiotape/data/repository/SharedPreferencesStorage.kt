package net.vrgsoft.radiotape.data.repository

import android.content.SharedPreferences
import net.vrgsoft.radiotape.data.pipeline.player_actions.Quality

class SharedPreferencesStorage(
        private val preferences: SharedPreferences
) {
    fun saveQuality(quality: String) = preferences.edit().putString("quality", quality).apply()

    fun getQuality(defaultValue: Quality): Quality {
        val result = preferences.getString("quality", null)
        return if (result == null) defaultValue else Quality.byValue(result)
    }

    fun saveLastPlayedStationId(id: Long) {
        preferences.edit().putLong("lastPlayedStationId", id).apply()
    }

    fun getLastPlayedStationId(): Long {
        return preferences.getLong("lastPlayedStationId", -1)
    }

    fun getEqualizerProperties(): String? = preferences.getString(KEY_EQUALIZER_PROPERTIES, null)

    fun setEqualizerProperties(properties: String) {
        preferences.edit().putString(KEY_EQUALIZER_PROPERTIES, properties).apply()
    }

    fun isEqualizerEnabled(): Boolean = preferences.getBoolean(KEY_EQUALIZER_ENABLED, false)

    fun isAudioCompressionEnabled(): Boolean = preferences.getBoolean(KEY_AUDIO_COMPRESSION_ENABLED, false)

    fun setEqualizerEnabled(enabled: Boolean) {
        preferences.edit().putBoolean(KEY_EQUALIZER_ENABLED, enabled).apply()
    }

    fun setAudioCompressionEnabled(enabled: Boolean) {
        preferences.edit().putBoolean(KEY_AUDIO_COMPRESSION_ENABLED, enabled).apply()
    }

    fun getEqualizerPreset(): Int = preferences.getInt(KEY_USER_PRESET_POSITION, -1)

    fun setEqualizerPreset(position: Int) {
        preferences.edit().putInt(KEY_USER_PRESET_POSITION, position).apply()
    }

    fun getEqualizerPreamp(): Int = preferences.getInt(KEY_EQUALIZER_PREAMP, 0)

    fun setEqualizerPreamp(value: Int) {
        preferences.edit().putInt(KEY_EQUALIZER_PREAMP, value).apply()
    }

    fun isFirstLaunch(): Boolean {
        return preferences.getBoolean(KEY_IS_FIRST_LAUNCH, true)
    }

    fun disableFirstLaunchState() {
        preferences.edit().putBoolean(KEY_IS_FIRST_LAUNCH, false).apply()
    }

    fun setLastTimer(time: Long) {
        preferences.edit().putLong(KEY_LAST_TIMER, time).apply()
    }

    fun getLastTimerTime(): Long = preferences.getLong(KEY_LAST_TIMER, 0L)

    companion object {
        const val KEY_EQUALIZER_PROPERTIES = "key_equalizer_properties"
        const val KEY_EQUALIZER_ENABLED = "key_equalizer_enabled"
        const val KEY_AUDIO_COMPRESSION_ENABLED = "key_audio_compression_enabled"
        const val KEY_USER_PRESET_POSITION = "key_user_preset_position"
        const val KEY_EQUALIZER_PREAMP = "key_equalizer_preamp"
        const val KEY_IS_FIRST_LAUNCH = "KEY_IS_FIRST_LAUNCH"
        const val KEY_LAST_TIMER = "KEY_LAST_TIMER"
    }
}
