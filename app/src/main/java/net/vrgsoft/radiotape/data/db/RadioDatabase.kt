package net.vrgsoft.radiotape.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import net.vrgsoft.radiotape.DB_VERSION
import net.vrgsoft.radiotape.data.db.dao.FiltersDao
import net.vrgsoft.radiotape.data.db.dao.GenreDao
import net.vrgsoft.radiotape.data.db.dao.LocationDao
import net.vrgsoft.radiotape.data.db.dao.RadioStationDao
import net.vrgsoft.radiotape.data.db.model.*

@Database(entities = [
    CityDB::class,
    CountryDB::class,
    GenreDB::class,
    SubgenreDB::class,
    RadioStationDB::class,
    CountryCityDB::class,
    GenreSubgenreDB::class,
    SettingFiltersDB::class
], version = DB_VERSION)
@TypeConverters(value = [Converters::class])
abstract class RadioDatabase : RoomDatabase() {
    abstract fun radioStationDao(): RadioStationDao
    abstract fun locationDao(): LocationDao
    abstract fun genreDao(): GenreDao
    abstract fun filtersDao(): FiltersDao
}
