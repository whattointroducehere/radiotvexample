package net.vrgsoft.radiotape.data.repository.stations

import net.vrgsoft.radiotape.data.db.dao.GenreDao
import net.vrgsoft.radiotape.data.db.dao.LocationDao
import net.vrgsoft.radiotape.data.db.dao.RadioStationDao
import net.vrgsoft.radiotape.data.db.model.*
import net.vrgsoft.radiotape.data.model.InitialJsonObject

class StationsStorageLocal(
        private val stationDao: RadioStationDao,
        private val locationDao: LocationDao,
        private val genreDao: GenreDao
) {
    fun getAllStations(): List<RadioStationDB> = stationDao.getAll()

    fun insertStations(stations: List<RadioStationDB>) {
        stationDao.insertAll(stations)
    }

    fun saveInitialInfo(info: InitialJsonObject) {
        stationDao.insertAll(info.stations)
        locationDao.insertAllCities(info.cities)
        locationDao.insertAllCountries(info.countries)
        locationDao.insertAllCountryCity(info.countryCities)
        genreDao.insertAllGenres(info.genres)
        genreDao.insertAllSubGenres(info.subgenres)
        genreDao.insertAllGenreSubgenre(info.genreSubgenre)
    }

    fun getAllGenres(): List<GenreDB> = genreDao.getAllGenres()

    fun getAllCountries(): List<CountryDB> = locationDao.getAllCountries()

    fun getAllCities(): List<CityDB> = locationDao.getAllCities()

    fun getCitiesForCountry(id: Long): List<CityDB> = locationDao.getCitiesForCountry(id)

    fun getSubgenres(id: Long): List<SubgenreDB> = locationDao.getSubgenres(id)

    fun getStationById(id: Long): RadioStationDB? = stationDao.getStationById(id)
    fun updateStation(stationDB: RadioStationDB) {
        stationDao.updateStation(stationDB)
    }

    fun getSavedStations(): List<RadioStationDB> = stationDao.getSavedStations()
}