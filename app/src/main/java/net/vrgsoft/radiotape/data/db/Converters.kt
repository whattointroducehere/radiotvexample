package net.vrgsoft.radiotape.data.db

import android.arch.persistence.room.TypeConverter

class Converters {
    @TypeConverter
    fun fromLongListToString(values: List<Long>): String {
        val sb = StringBuilder()
        for (value in values) {
            sb.append(value).append(",")
        }
        if (!sb.isEmpty()) {
            sb.deleteCharAt(sb.length - 1)
        }
        return sb.toString()
    }

    @TypeConverter
    fun fromStringToLongList(value: String): List<Long> {
        if(value.isEmpty()) return emptyList()
        return value.split(",").toList().map { it.toLong() }
    }
}