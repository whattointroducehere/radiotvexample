package net.vrgsoft.radiotape.data.model

class City(override var id: Long,
           override var name: String = "",
           var countryId: Long = -1,
           override var checked: Boolean = false) : Filtrable {
    override var expanded: Boolean = false

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as City

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun getParentId(): Long {
        return countryId
    }

    override fun getChild(): List<Filtrable>? = null

}
