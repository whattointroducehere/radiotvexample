package net.vrgsoft.radiotape.data.repository.filters

import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB
import net.vrgsoft.radiotape.domain.filters.FiltersRepository

class FiltersRepositoryImpl(
        private val remote: FiltersStorageRemote,
        private val local: FiltersStorageLocal
) : FiltersRepository {

    override val onFiltersUpdated = ConflatedBroadcastChannel<SettingFiltersDB>()

    override fun saveFilters(settingFiltersDB: SettingFiltersDB) {
        local.saveFilters(settingFiltersDB)

        launch { onFiltersUpdated.send(settingFiltersDB) }
    }

    override fun loadFilters(): SettingFiltersDB = local.loadFilters()
}
