package net.vrgsoft.radiotape.data.model.mapper

import net.vrgsoft.radiotape.data.db.model.CityDB
import net.vrgsoft.radiotape.data.model.City

class CityMapper {
    fun map(cityDB: CityDB, countryId : Long): City {
        return City(cityDB.id, cityDB.name, countryId)
    }

    fun map(cityDBList: List<CityDB>, countryId : Long): List<City> {
        val list = mutableListOf<City>()
        for (cityDb in cityDBList) {
            list.add(map(cityDb, countryId))
        }

        return list
    }
}