package net.vrgsoft.radiotape.data.repository.filters

import net.vrgsoft.radiotape.data.db.dao.FiltersDao
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB

class FiltersStorageLocal(
        private val dao: FiltersDao
) {
    fun loadFilters(): SettingFiltersDB {
        return dao.getSettings() ?: SettingFiltersDB()
    }

    fun saveFilters(settingFiltersDB: SettingFiltersDB) {
        dao.update(settingFiltersDB)
    }
}
