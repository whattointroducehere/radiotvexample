package net.vrgsoft.radiotape.data.model

interface Filtrable {
    var id: Long
    var name: String
    var checked: Boolean
    var expanded: Boolean

    fun getChild(): List<Filtrable>?
    fun getParentId(): Long = ID_ALL

    companion object {
        const val ID_ALL = -1L
    }
}
