package net.vrgsoft.radiotape.utils

import android.content.Context
import android.media.audiofx.Equalizer
import android.text.TextUtils
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.model.Band
import net.vrgsoft.radiotape.data.model.Preset
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.presentation.service.PlayerService
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class AppEqualizer(androidContext: Context) : KodeinAware {
    private val parentKodein by closestKodein(androidContext)

    override val kodein: Kodein = Kodein.lazy {
        extend(parentKodein)
        bind() from provider { createOfflineEqualizer() }
        bind() from singleton { Job() }
    }

    private val settings: SettingsInteractor by kodein.instance()
    private val offlineEqualizer: Equalizer by instance()
    private val jobs: Job by instance()
    private var equalizer: Equalizer = offlineEqualizer

    init {
        launch(parent = jobs) {
            settings.equalizerChangeEvent.consumeEach {
                updateSettings(it)
            }
        }
    }

    fun attachEqualizer(audioSessionId: Int) {
        try {
            this.equalizer = Equalizer(PlayerService.EQUALIZER_PRIORITY, audioSessionId)

            launch(parent = jobs) {
                updateSettings(settings.getEqualizerProperties())
            }
        } catch (e: Exception) {
            logError("AppEqualizer", "attachEqualizer error", e)
        }
    }

    private fun updateSettings(properties: String?) {
        this.equalizer.enabled = settings.isEqualizerEnabled()
        if (!TextUtils.isEmpty(properties)) {
            equalizer.properties = Equalizer.Settings(properties)
        }
    }

    fun getPresets(): List<Preset> {
        val presets = mutableListOf<Preset>()

        val numberOfPresets = equalizer.numberOfPresets

        for (i in 0..numberOfPresets) {
            val presetName = equalizer.getPresetName(i.toShort())

            if (TextUtils.isEmpty(presetName)) continue

            val preset = Preset(i, presetName)
            presets.add(preset)
        }

        return presets
    }

    fun getBands(): List<Band> {
        val numberOfBands = equalizer.numberOfBands

        val list = ArrayList<Band>()

        for (i in 0..(numberOfBands - 1)) {
            val center = equalizer.getCenterFreq(i.toShort())
            val level = equalizer.getBandLevel(i.toShort())
            val band = Band(i, center, level.toInt())

            list.add(band)
        }

        return list
    }

    fun getLowerEqualizerBandLevel(): Short {
        return equalizer.bandLevelRange[0]
    }

    fun getUpperEqualizerBandLevel(): Short {
        return equalizer.bandLevelRange[1]
    }

    fun getNumberOfBands(): Short {
        return equalizer.numberOfBands
    }

    fun detachEqualizer() {
        jobs.cancel()
        equalizer = offlineEqualizer
    }

    fun setEqualizerEnabled(enabled: Boolean) {
        equalizer.enabled = enabled
    }

    fun getSettings() = equalizer.properties

    fun setPreset(position: Short) {
        equalizer.usePreset(position)
    }

    private fun createOfflineEqualizer() = Equalizer(PlayerService.EQUALIZER_PRIORITY, 97555)

    fun reset(): Equalizer.Settings {
        equalizer = createOfflineEqualizer()
        return equalizer.properties
    }
}
