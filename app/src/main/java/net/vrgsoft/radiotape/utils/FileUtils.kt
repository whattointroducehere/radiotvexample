package net.vrgsoft.radiotape.utils

import android.os.Environment
import java.io.File

fun getRecordsDirectory(): String {
    val destPath = Environment.getExternalStorageDirectory().toString() + File.separator + "RadioTapeRecords"
    val directory = File(destPath)
    try {
        if (!directory.exists()) {
            directory.mkdir()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return destPath + File.separator
}
