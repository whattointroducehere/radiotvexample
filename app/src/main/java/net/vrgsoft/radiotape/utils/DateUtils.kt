package net.vrgsoft.radiotape.utils

import java.text.SimpleDateFormat
import java.util.*


fun getCurrentFormattedDateForRecord(): String {
    val dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    return dateFormat.format(Date(System.currentTimeMillis()))
}