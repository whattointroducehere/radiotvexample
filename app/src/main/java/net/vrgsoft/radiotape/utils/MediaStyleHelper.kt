package net.vrgsoft.radiotape.utils

import android.app.PendingIntent
import android.content.Context
import android.content.Intent

import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaButtonReceiver
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import net.vrgsoft.radiotape.presentation.cabinet.MainActivity
import net.vrgsoft.radiotape.presentation.service.CHANNEL_ID


object MediaStyleHelper {
    fun from(
            context: Context, mediaSession: MediaSessionCompat?): NotificationCompat.Builder? {
        val controller = mediaSession?.controller
        val mediaMetadata = controller?.metadata
        val description = mediaMetadata?.description
        val subtitle = mediaMetadata?.getString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE)

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
        builder.setContentTitle(description?.title)
        builder.setContentIntent(controller?.sessionActivity)
        val resultIntent = Intent(context, MainActivity::class.java)
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT)

        builder
                .setContentTitle(description?.title)
                .setContentText(subtitle)
                .setSubText(description?.description)
                .setLargeIcon(description?.iconBitmap)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setShowWhen(false)
                .setContentIntent(resultPendingIntent)
                .setDeleteIntent(MediaButtonReceiver.buildMediaButtonPendingIntent(context, PlaybackStateCompat.ACTION_STOP))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        return builder
    }
}
