package net.vrgsoft.radiotape.utils

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.support.v7.widget.AppCompatImageView
import android.view.View
import net.vrgsoft.radiotape.R

object BindAdapters {
    @JvmStatic
    @BindingAdapter("android:visibility")
    fun setVisibility(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    @JvmStatic
    @BindingAdapter("app:srcCompat")
    fun setImage(imageView: AppCompatImageView, drawable: Drawable) {
        imageView.setImageDrawable(drawable)
    }

    @JvmStatic
    @BindingAdapter("app:favIcon")
    fun setImage(imageView: AppCompatImageView, flag: Boolean) {
        imageView.setImageResource(if (flag) R.drawable.ic_favorite_purple else R.drawable.ic_favorite_white)
    }
}
