package net.vrgsoft.radiotape.utils

import com.google.gson.Gson
import com.google.gson.JsonElement
import net.vrgsoft.radiotape.data.db.model.CountryCityDB
import net.vrgsoft.radiotape.data.db.model.GenreSubgenreDB
import net.vrgsoft.radiotape.data.model.InitialJsonObject

class InitJsonParser(private val gson: Gson) {

    fun parse(jsonObject: JsonElement): InitialJsonObject {
        val startTime: Long = System.currentTimeMillis()

        logDebug("InitJsonParser", "StartTime = $startTime ms.")

        val initialJsonObject = InitialJsonObject()

        val jsonObject = jsonObject.asJsonObject

        // countryCitiesId
        val countryCitiesId = jsonObject["countries_cities"].asJsonObject
        for (key in countryCitiesId.keySet()) {
            val longKey = key.replaceAllQuotes().toLong()

            for (element in countryCitiesId[key].asJsonArray) {
                val longValue = element.asString.replaceAllQuotes().toLong()
                val countryCity = CountryCityDB(longKey, longValue)
                initialJsonObject.countryCities.add(countryCity)
            }
        }

        // genresSubgenresId
        val genresSubgenresId = jsonObject["genres_subgenres"].asJsonObject
        for (strKey in genresSubgenresId.keySet()) {
            val longKey = strKey.replaceAllQuotes().toLong()

            for (element in genresSubgenresId[strKey].asJsonArray) {
                val longValue = element.asString.replaceAllQuotes().toLong()
                val genreSubgenre = GenreSubgenreDB(longKey, longValue)
                initialJsonObject.genreSubgenre.add(genreSubgenre)
            }
        }

        // countries
        val countries = jsonObject["countries"].asJsonArray
        initialJsonObject.countries = gson.fromJson(countries)

        // cities
        val cities = jsonObject["cities"].asJsonArray
        initialJsonObject.cities = gson.fromJson(cities)

        // genres
        val genres = jsonObject["genres"].asJsonArray
        initialJsonObject.genres = gson.fromJson(genres)

        // subgenres
        val subgenres = jsonObject["subgenres"].asJsonArray
        initialJsonObject.subgenres = gson.fromJson(subgenres)

        // stations
        val stations = jsonObject["stations"].asJsonArray
        initialJsonObject.stations = gson.fromJson(stations)

        val endTime = System.currentTimeMillis()

        logDebug("InitJsonParser", "ExecTime = ${endTime - startTime} ms.")

        return initialJsonObject
    }

    private fun String.replaceAllQuotes(): String {
        return replace("\"", "")
    }
}



