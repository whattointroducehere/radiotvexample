package net.vrgsoft.radiotape.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.annotation.Px
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ConflatedChannel
import kotlinx.coroutines.experimental.channels.ReceiveChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.di.GlideApp
import net.vrgsoft.radiotape.presentation.cabinet.ExpandCallbacks

/**RX*/
//inline fun <reified T> Observable<T>.applyIOSchedulers(): Observable<T> =
//        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//
//inline fun <reified T> Single<T>.applyIOSchedulers(): Single<T> =
//        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//
//inline fun <reified T> Flowable<T>.applyIOSchedulers(): Flowable<T> =
//        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//
//inline fun Completable.applyIOSchedulers(): Completable =
//        this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//
//inline fun <reified T> Single<T>.applySubscriber(subscriber: SingleSubscriber<T>) =
//        this.doOnSubscribe(subscriber.progressStart)
//                .doFinally(subscriber.progressEnd)
//                .subscribe(subscriber.onNext, subscriber.onError)
//
//inline fun Completable.applySubscriber(subscriber: CompletableSubscriber) =
//        this.doOnSubscribe(subscriber.progressStart)
//                .doFinally(subscriber.progressEnd)
//                .subscribe(subscriber.onComplete, subscriber.onError)
//
//inline fun <reified T> Observable<T>.applySubscriber(subscriber: ObservableSubscriber<T>) =
//        this.doOnSubscribe(subscriber.progressStart)
//                .doAfterNext({ subscriber.progressEnd })
//                .subscribe(subscriber.onNext, subscriber.onError)

/**fragment manager*/

fun FragmentManager.replaceWithBackStack(containerId: Int, fragment: Fragment) {
    beginTransaction()
//            .setCustomAnimations(R.anim.fragment_slide_enter, R.anim.fragment_slide_exit, R.anim.fragment_slide_pop_enter, R.anim.fragment_slide_pop_exit)
            .replace(containerId, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
}

@SuppressLint("CheckResult")
fun FragmentManager.replaceWithoutBackStack(containerId: Int, fragment: Fragment) {
    beginTransaction()
//            .setCustomAnimations(R.anim.fragment_fade_enter, R.anim.fragment_fade_exit)
            .replace(containerId, fragment, fragment::class.java.simpleName)
            .commit()
}
fun FragmentManager.replaceSliding(containerId: Int, fragment: Fragment) {
    beginTransaction()
           .setCustomAnimations(R.anim.fragment_bottom_top, R.anim.fragment_top_bottom, R.anim.fragment_pop_bottom_top, R.anim.fragment_pop_top_bottom)
            .replace(containerId, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
}
fun FragmentManager.addSliding(containerId: Int, fragment: Fragment) {
    beginTransaction()
            .setCustomAnimations(R.anim.fragment_bottom_top, R.anim.fragment_top_bottom, R.anim.fragment_pop_bottom_top, R.anim.fragment_pop_top_bottom)
            .add(containerId, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commitAllowingStateLoss()
}
//@SuppressLint("CheckResult")
//fun FragmentManager.replaceMenuFragment(containerId: Int, fragment: Fragment) {
//    Observable.timer(400, TimeUnit.MILLISECONDS).applyIOSchedulers().subscribe {
//        popBackStackToTop()
//        beginTransaction()
//                .setCustomAnimations(R.anim.fragment_fade_enter, R.anim.fragment_fade_exit)
//                .replace(containerId, fragment, fragment::class.java.simpleName)
//                .commitAllowingStateLoss()
//    }
//}

fun FragmentManager.popBackStackToTop(): FragmentManager {
    while (backStackEntryCount > 0) popBackStackImmediate()
    return this
}

/**view*/
fun View.show(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun ImageView.setImageUrl(url: String?) {
    GlideApp.with(context)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
}

fun View.expandFromRight(callbacks: ExpandCallbacks? = null) {
    val animation = TranslateAnimation(Animation.RELATIVE_TO_PARENT, +1f,
            Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, 0f)
    animation.duration = 500

    animation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {

        }

        override fun onAnimationEnd(p0: Animation?) {
            callbacks?.invoke()
        }

        override fun onAnimationStart(p0: Animation?) {
            visibility = View.VISIBLE
        }


    })
    startAnimation(animation)
}

fun View.collapseToLeft() {
    val animation = TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, -1f,
            Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, 0f)
    animation.duration = 500

    animation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {

        }

        override fun onAnimationEnd(p0: Animation?) {
            visibility = View.GONE
        }

        override fun onAnimationStart(p0: Animation?) {

        }


    })
    startAnimation(animation)

}

fun View.expandFromLeft() {
    val animation = TranslateAnimation(Animation.RELATIVE_TO_PARENT, -1f,
            Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, 0f)
    animation.duration = 500

    animation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {

        }

        override fun onAnimationEnd(p0: Animation?) {

        }

        override fun onAnimationStart(p0: Animation?) {
            visibility = View.VISIBLE
        }


    })
    startAnimation(animation)
}

fun View.collapseToRight() {
    val animation = TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, +1f,
            Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, 0f)
    animation.duration = 500

    animation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {

        }

        override fun onAnimationEnd(p0: Animation?) {
            visibility = View.GONE
        }

        override fun onAnimationStart(p0: Animation?) {

        }


    })
    startAnimation(animation)

}

fun View.isExpanded() = visibility == View.VISIBLE

//fun ImageView.setImageUrlBlurred(url: String?) {
//    GlideApp.with(this)
//            .load(url)
//            .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 5)))
//            .transition(DrawableTransitionOptions.withCrossFade())
//            .into(this)
//}

//fun CircleImageView.setImageUrlCircled(url: String?) {
//    GlideApp.with(context)
//            .load(url)
//            .into(object : DrawableImageViewTarget(this) {
//                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
//                    alpha = 0.0f
//                    setImageDrawable(resource)
//                    animate().setDuration(300).alpha(1.0f).start()
//                }
//            })
//}

/**fragments*/
fun Fragment.showKeyboard(view: View) {
    val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

    inputManager?.showSoftInput(view, 0)
}

fun Fragment.hideKeyboard() {
    val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

    if (view != null) inputManager?.hideSoftInputFromWindow(view?.windowToken, 0)
}

/**activity*/
fun AppCompatActivity.launchExternalIntent(intent: Intent) {
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(intent)
    } else {
        Toast.makeText(this, R.string.not_supported, LENGTH_SHORT).show()
    }
}

inline fun <reified T : Any> Gson.fromJson(json: JsonElement) = Gson().fromJson<T>(json, object : TypeToken<T>() {}.type)

suspend inline fun <reified T> Channel<T>.subscribe(subscriber: (T) -> Unit) = consumeEach(subscriber)

suspend inline fun <reified T> ConflatedChannel<T>.subscribe(subscriber: (T) -> Unit) = consumeEach(subscriber)

suspend inline fun <reified T> ReceiveChannel<T>.subscribe(subscriber: (T) -> Unit) = consumeEach(subscriber)

fun Drawable.toBitmap(
        @Px width: Int = intrinsicWidth,
        @Px height: Int = intrinsicHeight,
        config: Bitmap.Config? = null
): Bitmap {
    if (this is BitmapDrawable) {
        if (config == null || bitmap.config == config) {
            // Fast-path to return original. Bitmap.createScaledBitmap will do this check, but it
            // involves allocation and two jumps into native code so we perform the check ourselves.
            if (width == intrinsicWidth && height == intrinsicHeight) {
                return bitmap
            }
            return Bitmap.createScaledBitmap(bitmap, width, height, true)
        }
    }

    val oldBottom = bounds.bottom
    val oldTop = bounds.top
    val oldLeft = bounds.left
    val oldRight = bounds.right
    val bitmap = Bitmap.createBitmap(width, height, config ?: Bitmap.Config.ARGB_8888)
    setBounds(0, 0, width, height)
    draw(Canvas(bitmap))

    setBounds(oldLeft, oldTop, oldRight, oldBottom)
    return bitmap
}

infix fun <T> Iterable<T>.hasIntersect(other: Iterable<T>): Boolean {
    val set = this.toMutableSet()
    set.retainAll(other)
    return !set.isEmpty()
}
