package net.vrgsoft.radiotape.utils

import android.util.Log
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import net.vrgsoft.radiotape.BuildConfig

fun logDebug(tag: String, message: String) {
    if (BuildConfig.DEBUG) Log.d(tag, message)
}

fun logVerbose(tag: String, message: String) {
    if (BuildConfig.DEBUG) Log.v(tag, message)
}

fun logInfo(tag: String, message: String) {
    if (BuildConfig.DEBUG) Log.i(tag, message)
}

fun logError(tag: String, message: String?, ex: Throwable) {
    if (BuildConfig.DEBUG) Log.e(tag, message?:"", ex)
}

fun logWarn(tag: String, message: String) {
    if (BuildConfig.DEBUG) Log.w(tag, message)
}

fun logRequest(tag: String) =
        { next: (Request) -> Request ->
            { r: Request ->
                logDebug(tag, r.cUrlString())
                next(r)
            }
        }

fun logResponse(tag: String): ((Request, Response) -> Response) -> (Request, Response) -> Response =
        {
            { _, response: Response ->
                logDebug(tag, response.toString())
                response
            }
        }
