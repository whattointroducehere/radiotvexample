package net.vrgsoft.radiotape

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_MEDIA_BUTTON
import android.content.IntentFilter
import android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY
import android.support.v4.media.session.MediaButtonReceiver
import android.util.Log

class MyMediaButtonReceiver : MediaButtonReceiver(), IntentFilterDeclaringReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context, intent: Intent) {
        try {
            super.onReceive(context, intent)
        } catch (e: IllegalStateException) {
            Log.d(this.javaClass.name, e.message)
        }
    }


    override fun getIntentFilter(): IntentFilter {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_MEDIA_BUTTON)
        intentFilter.addAction(ACTION_AUDIO_BECOMING_NOISY)
        return intentFilter
    }

}
