package net.vrgsoft.radiotape.presentation.cabinet.filters.common

import android.view.LayoutInflater
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.model.Filtrable
import net.vrgsoft.radiotape.data.model.Genre

class GenreAdapter(layoutInflater: LayoutInflater, var saveFilterChannel: ConflatedBroadcastChannel<Boolean>
) : BaseFiltersAdapter<Genre>(layoutInflater = layoutInflater,channel = saveFilterChannel) {

    private val adaptersMap: MutableMap<Int, BaseFiltersAdapter<*>> = HashMap()

    override fun getItemViewType(position: Int): Int {
        return if (dataList[position].id > 0) return VIEW_TYPE_CATEGORY else VIEW_TYPE_FILTER
    }

    override fun createAdapterForCategory(position: Int): BaseFiltersAdapter<*> {
        return if (!adaptersMap.contains(position)) {
            val adapter = SubgenreAdapter(layoutInflater, this,saveFilterChannel)

            val genre = dataList[position]
            adapter.setData(genre.subganre)
            adaptersMap[position] = adapter
            adapter
        } else {
            adaptersMap[position]!!
        }
    }

    override fun checkItem(adapter: BaseFiltersAdapter<*>, anyChecked: Boolean) {
        adaptersMap.entries.filter {
            it.value === adapter
        }.forEach {
            dataList[it.key].checked = anyChecked
        }
    }

    override fun isAllChecked(): Boolean {
        var isAllChecked = true

        dataList.forEach { it ->
            val subganre = it.subganre
            subganre.forEach {
                isAllChecked = isAllChecked && (it.checked || (it.id == Filtrable.ID_ALL && subganre.size > 1))
            }
        }

        return isAllChecked
    }
}
