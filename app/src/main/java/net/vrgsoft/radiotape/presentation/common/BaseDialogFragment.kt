package net.vrgsoft.radiotape.presentation.common

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.experimental.Job
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.android.closestKodein
import org.kodein.di.simpleErasedName
import java.lang.reflect.ParameterizedType

abstract class BaseDialogFragment<B : ViewDataBinding> : DialogFragment(), KodeinAware {
    private val _parentKodein: Kodein by closestKodein()
    override val kodein: Kodein = Kodein.lazy {
        extend(_parentKodein)
        import(diModule())
    }
    override val kodeinTrigger = KodeinTrigger()

    protected lateinit var binding: B
    protected var jobs = Job()

    override fun onAttach(context: Context?) {
        kodeinTrigger.trigger()
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewCreated()
    }

    protected inline fun <reified VM : BaseViewModel> vm(factory: ViewModelProvider.Factory): VM {
        return ViewModelProviders.of(this, factory)[VM::class.java]
    }

    override fun onDestroy() {
        jobs.cancel()
        jobs = Job()
        super.onDestroy()
    }

    abstract fun diModule(): Kodein.Module

    fun getLayoutRes(): Int {
        val fragmentLayoutName = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]
                .simpleErasedName()
                .replace("Binding", "")
                .split("(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])".toRegex())
                .joinToString(separator = "_")
                .toLowerCase()

        val resourceName = "${context?.applicationContext?.packageName}:layout/$fragmentLayoutName"
        return resources.getIdentifier(resourceName, null, null)

    }

    abstract fun viewCreated()
}
