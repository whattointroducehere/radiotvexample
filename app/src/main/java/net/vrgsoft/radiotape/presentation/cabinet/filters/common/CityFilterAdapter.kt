package net.vrgsoft.radiotape.presentation.cabinet.filters.common

import android.view.LayoutInflater
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.model.City

class CityFilterAdapter(layoutInflater: LayoutInflater,
                        override val parentAdapter: CountryFilterAdapter,
                        saveFilterChannel: ConflatedBroadcastChannel<Boolean>
) : BaseFiltersAdapter<City>(layoutInflater = layoutInflater,channel = saveFilterChannel) {

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE_SUBFILTER
    }
}
