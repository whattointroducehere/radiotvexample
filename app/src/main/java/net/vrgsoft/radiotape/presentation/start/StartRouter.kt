package net.vrgsoft.radiotape.presentation.start

import net.vrgsoft.radiotape.presentation.cabinet.MainActivity
import net.vrgsoft.radiotape.presentation.cabinet.filters.FiltersContract

class StartRouter(private val activity: StartActivity)
    : StartContract.Router,
        FiltersContract.Router {
    override fun openMainScreen() {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finish()
    }

    override fun onFiltersFragmentClose() {
        //do nothing
    }

}
