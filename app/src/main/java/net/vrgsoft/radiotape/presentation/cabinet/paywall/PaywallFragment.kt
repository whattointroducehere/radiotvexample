package net.vrgsoft.radiotape.presentation.cabinet.paywall


import net.vrgsoft.radiotape.databinding.FragmentPaywallBinding
import net.vrgsoft.radiotape.presentation.cabinet.MainRouter
import net.vrgsoft.radiotape.presentation.common.BaseFragment
import net.vrgsoft.radiotape.presentation.common.PaywallFactory
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton


class PaywallFragment : BaseFragment<FragmentPaywallBinding>() {
    override fun diModule()= Kodein.Module("PaywallModule") {
        bind<PaywallFactory>() with singleton { PaywallFactory(instance(), instance(), instance()) }
        bind<PaywallContract.ViewModel>() with singleton { vm<PaywallViewModel>(instance<PaywallFactory>()) }
        bind<PaywallContract.Router>() with singleton { instance<MainRouter>() }
    }

    private val viewModel: PaywallContract.ViewModel by instance()

    override fun viewCreated() {
        binding.apply {
            viewModel = this@PaywallFragment.viewModel
        }
        viewModel.bound()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.unbound()
    }

    companion object {
        fun newInstance() = PaywallFragment()
    }
}
