package net.vrgsoft.radiotape.presentation.cabinet.radioplayer

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatImageView
import android.text.method.ScrollingMovementMethod
import android.view.MotionEvent
import android.view.animation.AccelerateInterpolator
import android.widget.SeekBar
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_player.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.pipeline.player_actions.Pause
import net.vrgsoft.radiotape.data.pipeline.player_actions.Play
import net.vrgsoft.radiotape.databinding.FragmentPlayerBinding
import net.vrgsoft.radiotape.di.GlideApp
import net.vrgsoft.radiotape.presentation.cabinet.MainRouter
import net.vrgsoft.radiotape.presentation.common.BaseFragment
import net.vrgsoft.radiotape.presentation.common.RadioPlayerFactory
import net.vrgsoft.radiotape.utils.subscribe
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class RadioPlayerFragment : BaseFragment<FragmentPlayerBinding>() {
    override fun diModule() = Kodein.Module("RadioPlayerFragment") {
        bind<RadioPlayerFactory>() with singleton { RadioPlayerFactory(instance(), instance(), instance()) }
        bind<RadioPlayerContract.ViewModel>() with singleton { vm<RadioPlayerViewModel>(instance<RadioPlayerFactory>()) }
        bind<RadioPlayerContract.Router>() with singleton { instance<MainRouter>() }
    }

    private val viewModel: RadioPlayerContract.ViewModel by instance()
    private val volumeKeyReceiver = VolumeKeyReceiver()
    private var currentStation: RadioStationDB? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun viewCreated() {
        launch(UI, parent = jobs) {
            viewModel.getPlayPause().subscribe {
                when (it) {
                    is Play -> {
                        binding.animationView.playAnimation()
                        currentStation = it.station

                        binding.tvStationTitle.text = currentStation?.name
                        GlideApp.with(this@RadioPlayerFragment)
                                .load(currentStation?.logo)
                                .into(binding.logo)
                    }
                    is Pause -> binding.animationView.pauseAnimation()
                }
            }
        }
        tvSongName.isSelected = true
        launch(UI, parent = jobs) { viewModel.getShowInfoDialog().subscribe { showInfoDialog() } }
        binding.apply {
            viewModel = this@RadioPlayerFragment.viewModel
            val audio = context?.getSystemService(Context.AUDIO_SERVICE) as AudioManager
            val maxLevel = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
            val level = audio.getStreamVolume(AudioManager.STREAM_MUSIC)
            sbVolume.max = maxLevel
            sbVolume.progress = level
            sbVolume.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    audio.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }
            })

            closeButtonTop.setOnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_UP -> {
                        if (root.translationY > root.height / 4) {
                            viewModel?.goBack()
                        } else {
                            val animator = ObjectAnimator.ofFloat(root, "translationY", root.translationY, 0f)
                            animator?.interpolator = AccelerateInterpolator()
                            animator?.start()
                        }
                    }
                    MotionEvent.ACTION_MOVE -> {
                        root.translationY = event.rawY
                    }
                }

                return@setOnTouchListener true
            }

        }
        viewModel.bound()
    }

    private fun showInfoDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_station_info, null, false)

        val dialog = AlertDialog.Builder(context!!)
                .setView(view)
                .create()

        val tvDescription = view.findViewById<TextView>(R.id.tvStationDescription)
        tvDescription.movementMethod = ScrollingMovementMethod()
        tvDescription.text = currentStation?.description
        view.findViewById<TextView>(R.id.tvStationTitle).text = currentStation?.name
        view.findViewById<AppCompatImageView>(R.id.ivClose).setOnClickListener { dialog.dismiss() }

        dialog.show()
    }

    override fun onStart() {
        val intentFilter = IntentFilter()
        intentFilter.addAction("android.media.VOLUME_CHANGED_ACTION")
        activity?.registerReceiver(volumeKeyReceiver, intentFilter)
        super.onStart()
    }

    override fun onStop() {
        try {
            activity?.unregisterReceiver(volumeKeyReceiver)
        } catch (e: Exception) {

        }
        super.onStop()
    }

    companion object {
        fun newInstance() = RadioPlayerFragment()
    }

    inner class VolumeKeyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val audio = context?.getSystemService(Context.AUDIO_SERVICE) as AudioManager
            val level = audio.getStreamVolume(AudioManager.STREAM_MUSIC)
            binding.sbVolume.progress = level
        }
    }
}
