package net.vrgsoft.radiotape.presentation.cabinet.radioplayer

import android.databinding.ObservableField
import kotlinx.coroutines.experimental.channels.Channel
import net.vrgsoft.radiotape.data.pipeline.player_actions.PlayPauseAction
import net.vrgsoft.radiotape.presentation.common.BaseVM

interface RadioPlayerContract {
    interface ViewModel : BaseVM {
        fun onInfoClick()

        fun getPlayPause(): Channel<PlayPauseAction>
        fun getShowInfoDialog(): Channel<Boolean>

        fun showEqualizer()
        fun goBack()

        val songNameText: ObservableField<String>
    }

    interface Router {
        fun goToEqualizerScreen()
        fun goBack()
    }
}
