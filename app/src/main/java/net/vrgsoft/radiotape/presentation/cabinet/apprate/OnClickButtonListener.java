package net.vrgsoft.radiotape.presentation.cabinet.apprate;

public interface OnClickButtonListener {

    void onClickButton(int which);

}