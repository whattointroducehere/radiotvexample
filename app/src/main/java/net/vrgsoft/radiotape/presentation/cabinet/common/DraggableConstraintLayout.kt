package net.vrgsoft.radiotape.presentation.cabinet.common

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.*
import android.view.animation.AccelerateInterpolator
import io.fabric.sdk.android.services.common.CommonUtils.hideKeyboard
import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import kotlin.math.absoluteValue


private enum class State { FILTER, EQ, FAVORITES, NONE }

class DraggableConstraintLayout : ConstraintLayout, KodeinAware {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override val kodein: Kodein by closestKodein(context.applicationContext)

    //filter views
    private val ivFilterBtn: View
    private val filterContainer: View
    private val clFilterContainerRoot: View
    private val llFilterContainer: View

    //equalizer views
    private val ivEqBtn: View
    private val equalizerContainer: View
    private val clEqContainerRoot: View
    private val llEqContainer: View

    //favorites views
    private val ivFavoritesBtn: View
    private val favoritesContainer: View
    private val clFavoritesContainerRoot: View
    private val llFavoritesContainer: View

    //coordinates variables
    private val initialTranslation: Float
    private var rightLimit: Float = 0f
    private var previousX = 0f
    private var downX = 0f

    //velocity variables
    private val slop: Int
    private val minFlingVelocity: Int
    private val maxFlingVelocity: Int
    private var velocityTracker: VelocityTracker? = null
    private var velocityX = 0f

    //other
    private var move = false
    private var animator: ValueAnimator? = null
    private var state = State.NONE

    private val startDragChannel: BroadcastChannel<Boolean> by instance("startDragEvent")

    init {
        clipChildren = false
        val root = LayoutInflater.from(context).inflate(R.layout.view_draggable_right, this, true)

        ivFilterBtn = root.findViewById(R.id.ivFilterBtn)
        filterContainer = root.findViewById(R.id.filterContainer)
        clFilterContainerRoot = root.findViewById(R.id.clFilterContainerRoot)
        llFilterContainer = root.findViewById(R.id.llFilterContainer)

        ivEqBtn = root.findViewById(R.id.ivEqBtn)
        equalizerContainer = root.findViewById(R.id.equalizerContainer)
        clEqContainerRoot = root.findViewById(R.id.clEqContainerRoot)
        llEqContainer = root.findViewById(R.id.llEqContainer)

        ivFavoritesBtn = root.findViewById(R.id.ivFavoritesBtn)
        favoritesContainer = root.findViewById(R.id.favoritesContainer)
        clFavoritesContainerRoot = root.findViewById(R.id.clFavoritesContainerRoot)
        llFavoritesContainer = root.findViewById(R.id.llFavoritesContainer)

        initialTranslation = resources.getDimension(R.dimen.initial_drag_translation)

        val config = ViewConfiguration.get(context)
        slop = config.scaledTouchSlop
        minFlingVelocity = config.scaledMinimumFlingVelocity * 2
        maxFlingVelocity = config.scaledMaximumFlingVelocity

        filterContainer.visibility = GONE
        equalizerContainer.visibility = GONE
        favoritesContainer.visibility = GONE

        initTouchListeners()
    }

//    override fun onInterceptTouchEvent(e: MotionEvent): Boolean {
//        when (e.action) {
//            MotionEvent.ACTION_DOWN -> {
//                initVelocityTracker(e)
//            }
//            MotionEvent.ACTION_MOVE -> {
//                calculateVelocity(e)
//            }
//            MotionEvent.ACTION_UP -> {
//                if(isOpened() && velocityX > 0 && velocityX.absoluteValue in minFlingVelocity..maxFlingVelocity){
//                    close()
//                    velocityTracker?.recycle()
//                    velocityTracker = null
//                    move = false
//                    return true
//                }
//
//            }
//        }
//        return super.onInterceptTouchEvent(e)
//    }

    @SuppressLint("Recycle")
    private fun initTouchListeners() {
        ivFilterBtn.setOnTouchListener { _, e ->
            when (e.action) {
                MotionEvent.ACTION_DOWN -> {
                    filterContainer.visibility = VISIBLE
                    launch {
                        startDragChannel.send(true)
                    }
                    onActionDown(e)
                }
                MotionEvent.ACTION_MOVE -> {
                    if (move) {
                        val distanceX = e.rawX - previousX
                        if (llFilterContainer.x + distanceX > rightLimit) {
                            positionViewsRight()
                        } else {
                            moveViews(llFilterContainer, llEqContainer, llFavoritesContainer, distanceX)
                        }
                        previousX = e.rawX
                    }
                    calculateVelocity(e)
                }
                MotionEvent.ACTION_UP -> {
                    onActionUp(filterContainer, llFilterContainer, llEqContainer, llFavoritesContainer, e)
                }
            }
            move
        }

        ivEqBtn.setOnTouchListener { _, e ->
            when (e.action) {
                MotionEvent.ACTION_DOWN -> {
                    equalizerContainer.visibility = VISIBLE
                    onActionDown(e)
                }
                MotionEvent.ACTION_MOVE -> {
                    if (move) {
                        val distanceX = e.rawX - previousX
                        if (llEqContainer.x + distanceX > rightLimit) {
                            positionViewsRight()
                        } else {
                            moveViews(llEqContainer, llFilterContainer, llFavoritesContainer, distanceX)
                        }
                        previousX = e.rawX
                    }
                    calculateVelocity(e)
                }
                MotionEvent.ACTION_UP -> {
                    onActionUp(equalizerContainer, llEqContainer, llFilterContainer, llFavoritesContainer, e)
                }
            }
            move
        }

        ivFavoritesBtn.setOnTouchListener { _, e ->
            when (e.action) {
                MotionEvent.ACTION_DOWN -> {
                    favoritesContainer.visibility = VISIBLE
                    onActionDown(e)
                }
                MotionEvent.ACTION_MOVE -> {
                    if (move) {
                        val distanceX = e.rawX - previousX
                        if (llFavoritesContainer.x + distanceX > rightLimit) {
                            positionViewsRight()
                        } else {
                            moveViews(llFavoritesContainer, llEqContainer, llFilterContainer, distanceX)
                        }
                        previousX = e.rawX
                    }
                    calculateVelocity(e)
                }
                MotionEvent.ACTION_UP -> {
                    onActionUp(favoritesContainer, llFavoritesContainer, llEqContainer, llFilterContainer, e)
                }
            }
            move
        }
    }

    private fun startViewsFinalAnimation(mainView: View, subView1: View, subView2: View) {
        if (animator?.isRunning == true) {
            animator?.cancel()
            animator = null
        }

        val mainViewX = mainView.x
        val subView1X = subView1.x
        val subView2X = subView2.x

        animator = ValueAnimator.ofFloat(0f, (-clFilterContainerRoot.width + resources.displayMetrics.density - mainView.x).absoluteValue)
        animator?.interpolator = AccelerateInterpolator()
        animator?.addUpdateListener {
            val value = it.animatedValue as Float
            mainView.x = mainViewX - value
            subView1.x = subView1X + value
            subView2.x = subView2X + value
        }
        animator?.start()
    }

    private fun startViewsRightAnimation(containerView: View, mainView: View, subView1: View, subView2: View) {
        if (animator?.isRunning == true) {
            animator?.cancel()
            animator = null
        }

        val mainViewX = mainView.x
        val subView1X = subView1.x
        val subView2X = subView2.x

        animator = ValueAnimator.ofFloat(0f, rightLimit - mainView.x)
        animator?.interpolator = AccelerateInterpolator()
        animator?.addUpdateListener {
            val value = it.animatedValue as Float
            mainView.x = mainViewX + value
            subView1.x = subView1X - value
            subView2.x = subView2X - value
        }
        animator?.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                containerView.visibility = GONE
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {

            }
        })
        animator?.start()
    }

    private fun onActionUp(containerView: View, mainView: View, subView1: View, subView2: View, e: MotionEvent) {
        val opened: Boolean
        if (isOpened()) {
            if (velocityX > 0 && velocityX.absoluteValue in minFlingVelocity..maxFlingVelocity) {
                startViewsRightAnimation(containerView, mainView, subView1, subView2)
                state = State.NONE
            }
        } else {
            if (velocityX < 0 && velocityX.absoluteValue in minFlingVelocity..maxFlingVelocity) {
                startViewsFinalAnimation(mainView, subView1, subView2)
                opened = true
            } else {
                if (e.rawX < Resources.getSystem().displayMetrics.widthPixels * 0.5f) {
                    opened = true
                    startViewsFinalAnimation(mainView, subView1, subView2)
                } else {
                    opened = false
                    startViewsRightAnimation(containerView, mainView, subView1, subView2)
                }
            }
            state = if (opened) {
                when (mainView) {
                    llFilterContainer -> State.FILTER
                    llEqContainer -> State.EQ
                    llFavoritesContainer -> State.FAVORITES
                    else -> State.NONE
                }
            } else {
                State.NONE
            }
        }
        velocityTracker?.recycle()
        velocityTracker = null
        move = false
    }

    private fun onActionDown(e: MotionEvent) {
        filterContainer.postInvalidate()
        move = true
        previousX = e.rawX
        downX = e.rawX
        initVelocityTracker(e)

        hideKeyboard(context, this)
    }

    private fun initVelocityTracker(e: MotionEvent) {
        velocityTracker?.clear()
        velocityTracker = velocityTracker ?: VelocityTracker.obtain()
        velocityTracker?.addMovement(e)
    }

    private fun calculateVelocity(e: MotionEvent) {
        velocityTracker?.apply {
            val pointerId: Int = e.getPointerId(e.actionIndex)
            addMovement(e)
            computeCurrentVelocity(1000)
            velocityX = xVelocity
        }
    }

    private fun moveViews(toMove: View, toHide1: View, toHide2: View, dx: Float) {
        toMove.x += dx
        toHide1.x -= dx
        toHide2.x -= dx
    }

    private fun positionViewsFinal(toMove: View, toHide1: View, toHide2: View) {
        toMove.x = 0f - clFilterContainerRoot.width + resources.displayMetrics.density
        toHide1.x = rightLimit + width
        toHide2.x = rightLimit + width
    }

    fun isOpened() = state != State.NONE

    fun close() {
        when (state) {
            State.FILTER -> startViewsRightAnimation(filterContainer, llFilterContainer, llEqContainer, llFavoritesContainer)
            State.EQ -> startViewsRightAnimation(equalizerContainer, llEqContainer, llFilterContainer, llFavoritesContainer)
            State.FAVORITES -> startViewsRightAnimation(favoritesContainer, llFavoritesContainer, llEqContainer, llFilterContainer)
            State.NONE -> {
            }
        }
        state = State.NONE
    }

    private fun positionViewsRight() {
        llFavoritesContainer.x = rightLimit
        llEqContainer.x = rightLimit
        llFilterContainer.x = rightLimit
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        rightLimit = w.toFloat() + initialTranslation

        llFilterContainer.x = rightLimit
        var layoutParams = filterContainer.layoutParams
        layoutParams.width = w
        filterContainer.layoutParams = layoutParams

        llEqContainer.x = rightLimit
        layoutParams = equalizerContainer.layoutParams
        layoutParams.width = w
        equalizerContainer.layoutParams = layoutParams

        llFavoritesContainer.x = rightLimit
        layoutParams = favoritesContainer.layoutParams
        layoutParams.width = w
        favoritesContainer.layoutParams = layoutParams
    }

    override fun onDetachedFromWindow() {
        velocityTracker?.recycle()
        velocityTracker = null
        animator?.cancel()
        animator = null
        super.onDetachedFromWindow()
    }
}
