package net.vrgsoft.radiotape.presentation.cabinet.radiolist.common

import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.graphics.drawable.PaintDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RectShape
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.databinding.ItemRadioStationBinding
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListContract
import net.vrgsoft.radiotape.presentation.common.BaseRecyclerAdapter
import net.vrgsoft.radiotape.presentation.common.BaseViewHolder
import net.vrgsoft.radiotape.utils.setImageUrl
import net.vrgsoft.radiotape.utils.show

class RadioListAdapter(
        private val viewModel: RadioListContract.ViewModel
) : BaseRecyclerAdapter<ItemRadioStationBinding>() {
    var stations: MutableList<RadioStationState> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    private val gradientDrawable by lazy {
        val gradient = PaintDrawable()
        gradient.shape = RectShape()
        gradient.shaderFactory = object : ShapeDrawable.ShaderFactory() {
            override fun resize(width: Int, height: Int): Shader {
                return LinearGradient(width * 0.5f, 0f, width * 0.5f, height.toFloat(),
                        intArrayOf(
                                Color.parseColor("#00000000"),
                                Color.parseColor("#5a000000"),
                                Color.parseColor("#ff030303"),
                                Color.parseColor("#ff0a0a0b"),
                                Color.parseColor("#ff1a202b")
                        ),
                        floatArrayOf(0f, 0.33f, 0.53f, 0.91f, 1f),
                        Shader.TileMode.CLAMP)
            }
        }
        gradient
    }

    private var playingStation: RadioStationState? = null

    override fun bindItem(holder: BaseViewHolder<ItemRadioStationBinding>, position: Int) {
        val station = stations[position]

        holder.binding.apply {
            viewModel = this@RadioListAdapter.viewModel
            this.position = position
            this.station = station
            vMiddleGradient.background = gradientDrawable
            ivStationIcon.setImageUrl(station.radioStation.logo)
            groupPlaying.show(station.isPlaying)
            if (station.isPlaying) equalizerMini.animateBars() else equalizerMini.stopBars()
        }
    }

    override fun getLayoutId(position: Int) = R.layout.item_radio_station
    override fun getItemCount() = stations.size

    fun updateFavorite(pair: Pair<RadioStationState, Int>) {
        if (pair.second >= 0) {
            notifyItemChanged(pair.second)
        } else {
            for ((index, value) in stations.withIndex()) {
                if (value.radioStation.id == pair.first.radioStation.id) {
                    value.radioStation.isFavorite = pair.first.radioStation.isFavorite
                    notifyItemChanged(index)
                    break
                }
            }
        }
    }

    fun removeFavorite(pair: Pair<RadioStationState, Int>) {

        for ((index, value) in stations.withIndex()) {
            if (value.radioStation.id == pair.first.radioStation.id) {
                value.radioStation.isFavorite = pair.first.radioStation.isFavorite
                stations.removeAt(index)
                notifyItemRemoved(index)
                break
            }
        }
    }

    fun updatePlayback(pair: Pair<RadioStationState, Int>) {
        if (playingStation != null) {
            val index = stations.indexOf(playingStation!!)
            if (index >= 0) {
                stations[index].isPlaying = false
                notifyItemChanged(index)
                playingStation = null
            }
        }

        playingStation = pair.first

        val index = stations.indexOf(playingStation!!)
        if (index >= 0) {
            stations[index].isPlaying = pair.first.isPlaying
            notifyItemChanged(index)
        }
    }

    fun add(second: RadioStationState) {
        stations.add(second)
        notifyDataSetChanged()
    }

}
