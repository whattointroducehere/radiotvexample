package net.vrgsoft.radiotape.presentation.cabinet.equalizer

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.model.Band
import net.vrgsoft.radiotape.data.model.Preset
import net.vrgsoft.radiotape.presentation.common.BaseVM

class EqualizerContract {
    interface ViewModel : BaseVM {

        // functions

        fun notifyVolumeChanged()

        fun forwardPreset()

        fun rewindPreset()

        fun openPresetList()

        fun selectPreset(which: Int, forceUpdate: Boolean, forceBandsReload: Boolean)

        fun getLowerBandLevel(): Int

        fun getUpperBandLevel(): Int

        fun onBandLevelChanged(band: Band)

        fun goBack()

        fun muteVolume()

        fun unmuteVolume()

        fun onResetClicked()

        fun resetSettings()

        // properties

        val presetText: ObservableField<String>

        val volumeValue: ObservableInt

        val volumeMaxValue: ObservableInt

        val equalizerEnabled: ObservableBoolean

        val audioCompressionEnabled: ObservableBoolean

        val forwardPresetButtonVisibility: ObservableBoolean

        val rewindPresetButtonVisibility: ObservableBoolean

        // channels

        val openPresetList: ConflatedBroadcastChannel<List<Preset>>

        val showBandController: ConflatedBroadcastChannel<List<Band>>

        val showResetSettingsDialog: ConflatedBroadcastChannel<Boolean>
    }

    interface Router {
        fun goBack()
    }
}
