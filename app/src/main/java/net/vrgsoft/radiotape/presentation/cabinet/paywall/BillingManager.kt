package net.vrgsoft.radiotape.presentation.cabinet.paywall


import android.app.Activity
import android.widget.Toast
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.SkuDetails
import com.anjlab.android.iab.v3.TransactionDetails
import io.reactivex.subjects.BehaviorSubject
import net.vrgsoft.radiotape.BuildConfig
import net.vrgsoft.radiotape.R
import java.util.ArrayList
import java.util.HashMap


class BillingManager(private val activity: Activity) : BillingProcessor.IBillingHandler {

    private val mAvailableProducts = ArrayList<String>()

    private val bp: BillingProcessor
    private var readyToPurchase: Boolean = false

    var skuDetails = BehaviorSubject.create<Map<String, SkuDetails>>()
    var onSubscriptionPurchased = BehaviorSubject.create<Boolean>()

    private val productMonth: String = activity.getString(R.string.sku_inapp_month)
    private val productYear: String = activity.getString(R.string.sku_inapp_year)

    val isProductPurchased: Boolean
        get() = (onSubscriptionPurchased.hasValue() && onSubscriptionPurchased.value!!
                || bp.isPurchased(productMonth) || bp.isPurchased(productYear)
                || bp.isSubscribed(productMonth) || bp.isSubscribed(productYear))

    init {

        bp = BillingProcessor.newBillingProcessor(activity, LICENSE_KEY, this)

        if (!BillingProcessor.isIabServiceAvailable(activity)) {
            Toast.makeText(activity, R.string.billing_unavailable_error_message, Toast.LENGTH_SHORT).show()
        } else {
            bp.initialize()
        }

        onSubscriptionPurchased.onNext(false)

        mAvailableProducts.add(productMonth)
        mAvailableProducts.add(productYear)
    }

    override fun onProductPurchased(s: String, transactionDetails: TransactionDetails) {
        onSubscriptionPurchased.onNext(true)
    }

    override fun onPurchaseHistoryRestored() {

    }

    override fun onBillingError(i: Int, throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            Toast.makeText(activity,
                    String.format(activity.getString(R.string.billing_error_format), Integer.toString(i)),
                    Toast.LENGTH_LONG).show()
        }
    }

    override fun onBillingInitialized() {
        readyToPurchase = true
         if (bp.loadOwnedPurchasesFromGoogle()) {
            val details = bp.getPurchaseListingDetails(mAvailableProducts)
            if (details != null) {
                val map = HashMap<String, SkuDetails>()
                for (details1 in details) {
                    map[details1.productId] = details1
                }

                skuDetails.onNext(map)
            } else {
                Toast.makeText(activity, R.string.billing_failed_to_load_prices, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun startSubscription(sku: String) {
        if (readyToPurchase && bp.isInitialized) {

            if (bp.isSubscriptionUpdateSupported) {
                bp.subscribe(activity, sku)
            } else {
                Toast.makeText(activity, R.string.billing_unavailable_error_message, Toast.LENGTH_LONG).show()
            }

        }
    }

    fun startMonthlySubscription() {
        if (readyToPurchase) {
            startSubscription(productMonth)
        }
    }

    fun startAnnualSubscription() {
        if (readyToPurchase) {
            startSubscription(productYear)
        }
    }

    companion object {
        private val LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsSIrBc+N24xHC0CUkY163+qUwnL0zLdmJ8VRsuNJhaDW6ZOigoR1LJobeKMchhFW+pCeS/+EFIyUCDoUHye60nMsKqHllTsROTqtdZ+dNOtiZiJ7PI7EJxwy5Hnh/Iz9sen0zRakwBQ84NQ6tgEwAk/oDLtzzARgKNz2lKdnzKzBmwW4zglUMbyk/rsKzf9lavqfYRiywdKXnIHPKGiB8FHw9Jfjs4d/vudF3Xqdy6t4Pvvha+alMy8Qb1bthBpGUGwIGAgU0Jw8C1ux5e8MBp6Cs++lnzGA9JfOpNh3qFGfLBMuFw/5jOsMym3ZJWadr1Q+sIHSeeWCLdg3YvGFPwIDAQAB"
    }
}
