package net.vrgsoft.radiotape.presentation.cabinet.radiolist

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import net.vrgsoft.radiotape.databinding.FragmentRadioListBinding
import net.vrgsoft.radiotape.presentation.cabinet.MainRouter
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.common.RadioListAdapter
import net.vrgsoft.radiotape.presentation.common.BaseFragment
import net.vrgsoft.radiotape.presentation.common.RadioListFactory
import net.vrgsoft.radiotape.utils.hideKeyboard
import net.vrgsoft.radiotape.utils.subscribe
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class RadioListFragment : BaseFragment<FragmentRadioListBinding>() {
    override fun diModule() = Kodein.Module("RadioListModule") {
        bind<RadioListFactory>() with singleton { RadioListFactory(instance(), instance(), instance(), instance(), instance("filtersChangeEvent")) }
        bind<RadioListContract.ViewModel>() with singleton { vm<RadioListViewModel>(instance<RadioListFactory>()) }
        bind<RadioListContract.Router>() with singleton { instance<MainRouter>() }
    }

    private val viewModel: RadioListContract.ViewModel by instance()
    private val adapter by lazy { RadioListAdapter(viewModel) }
    private val startDragChannel: BroadcastChannel<Boolean> by instance("startDragEvent")

    override fun viewCreated() {
        binding.apply {
            viewModel = this@RadioListFragment.viewModel
            rvRadioList.layoutManager = LinearLayoutManager(this@RadioListFragment.context)
            rvRadioList.itemAnimator = null
            rvRadioList.adapter = adapter

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    this@RadioListFragment.hideKeyboard()
                    searchView.clearFocus()
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    viewModel?.search(newText)
                    return true
                }
            })
        }

        launch(parent = jobs) {
            startDragChannel.consumeEach {
                withContext(UI) { hideSoftInput() }
            }
        }

        viewModel.bound()
        viewModel.getStations()
        launch(UI, parent = jobs) {
            viewModel.radioList.subscribe {
                adapter.stations = it.toMutableList()
                binding.rvRadioList.scrollToPosition(0)
            }
        }

        launch(UI, parent = jobs) {
            viewModel.updateStation().subscribe {
                when (it.first) {
                    RadioListContract.UpdateEvent.FAVORITE -> adapter.updateFavorite(it.second)
                    RadioListContract.UpdateEvent.PLAYBACK -> adapter.updatePlayback(it.second)
                }
            }
        }

        launch(UI, parent = jobs) {
            viewModel.hideKeyboard.consumeEach { hideSoftInput() }
        }
    }

    private fun hideSoftInput() {
        binding.searchView.clearFocus()
        hideKeyboard()
    }

    override fun onPause() {
        super.onPause()
        binding.searchView.clearFocus()
    }

    override fun onResume() {
        super.onResume()
        if (adapter.itemCount > 0) {
            viewModel.getStations()
        }
    }

    companion object {
        fun newInstance() = RadioListFragment()
    }
}
