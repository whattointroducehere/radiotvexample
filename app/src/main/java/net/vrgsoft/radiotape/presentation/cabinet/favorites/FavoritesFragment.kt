package net.vrgsoft.radiotape.presentation.cabinet.favorites


import android.support.v7.widget.LinearLayoutManager
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.databinding.FragmentFavoritesBinding
import net.vrgsoft.radiotape.presentation.cabinet.MainRouter
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListContract
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.common.RadioListAdapter
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.common.RadioStationState
import net.vrgsoft.radiotape.presentation.common.BaseFragment
import net.vrgsoft.radiotape.presentation.common.FavoritesFactory
import net.vrgsoft.radiotape.utils.hideKeyboard
import net.vrgsoft.radiotape.utils.subscribe
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton




class FavoritesFragment : BaseFragment<FragmentFavoritesBinding>() {
    override fun diModule() = Kodein.Module("FavoritesModule") {
        bind() from singleton { FavoritesFactory(instance(), instance(), instance(), instance(), instance("filtersChangeEvent")) }
        bind<FavoritesContract.ViewModel>() with singleton { vm<FavoritesViewModel>(instance<FavoritesFactory>()) }
        bind<FavoritesContract.Router>() with singleton { instance<MainRouter>() }
    }

    private val viewModel: FavoritesContract.ViewModel by instance()
    private val adapter by lazy { RadioListAdapter(viewModel) }

    override fun viewCreated() {
        binding.apply {
            viewModel = this@FavoritesFragment.viewModel
            rvRadioList.layoutManager = LinearLayoutManager(context)
            rvRadioList.itemAnimator = null
            rvRadioList.adapter = adapter

        }

        viewModel.bound()
        launch(UI, parent = jobs) {
            viewModel.radioList.subscribe { adapter.stations = it.toMutableList() }
        }

        launch(UI, parent = jobs) {
            viewModel.hideKeyboard.consumeEach {
                hideKeyboard()
            }
        }

        launch(UI, parent = jobs) {
            viewModel.updateStation().subscribe {
                when (it.first) {
                    FavoritesContract.UpdateEvent.FAVORITE -> {
                        adapter.removeFavorite(it.second)
                        viewModel.onItemsNotFoundEvent.set(adapter.stations.isEmpty())
                    }
                    RadioListContract.UpdateEvent.PLAYBACK -> adapter.updatePlayback(it.second)
                }
            }
        }
        launch(UI, parent = jobs) {
            viewModel.onStationUpdated().subscribe {
                if (it.isFavorite) {
                    adapter.add(RadioStationState(it, false))
                    viewModel.onItemsNotFoundEvent.set(adapter.stations.isEmpty())
                }
            }
        }
    }

    companion object {
        fun newInstance() = FavoritesFragment()
    }
}
