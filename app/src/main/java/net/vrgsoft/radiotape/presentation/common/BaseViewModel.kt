package net.vrgsoft.radiotape.presentation.common

import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.channels.BroadcastChannel
import net.vrgsoft.radiotape.presentation.common.model.ErrorMessage

abstract class BaseViewModel : ViewModel(), BaseVM {
    protected val jobs = Job()
    protected val errors = BroadcastChannel<ErrorMessage>(1)
    protected val progress = BroadcastChannel<Boolean>(1)

    override fun unbound() {
        jobs.cancel()
    }
}