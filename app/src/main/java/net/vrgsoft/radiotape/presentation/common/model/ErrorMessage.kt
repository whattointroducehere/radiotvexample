package net.vrgsoft.radiotape.presentation.common.model

import android.support.annotation.StringRes

class ErrorMessage (val message: String? = null, @StringRes val resId: Int = -1)