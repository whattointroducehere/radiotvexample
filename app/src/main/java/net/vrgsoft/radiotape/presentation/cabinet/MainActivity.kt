package net.vrgsoft.radiotape.presentation.cabinet

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v7.app.AlertDialog
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.EditText
import android.widget.TextView
import com.google.android.exoplayer2.util.Util
import com.marcinmoskala.arcseekbar.ArcSeekBar
import com.marcinmoskala.arcseekbar.ProgressListener
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.data.pipeline.player_actions.Pause
import net.vrgsoft.radiotape.data.pipeline.player_actions.Play
import net.vrgsoft.radiotape.data.pipeline.player_actions.Quality
import net.vrgsoft.radiotape.data.pipeline.record.RecordFinished
import net.vrgsoft.radiotape.data.pipeline.record.RecordStarted
import net.vrgsoft.radiotape.data.pipeline.record.RecordState
import net.vrgsoft.radiotape.data.repository.SharedPreferencesStorage
import net.vrgsoft.radiotape.databinding.ActivityMainBinding
import net.vrgsoft.radiotape.presentation.cabinet.apprate.AppRate
import net.vrgsoft.radiotape.presentation.cabinet.filters.FiltersContract
import net.vrgsoft.radiotape.presentation.cabinet.paywall.BillingManager
import net.vrgsoft.radiotape.presentation.common.BaseActivity
import net.vrgsoft.radiotape.presentation.common.MainFactory
import net.vrgsoft.radiotape.presentation.service.PlayerService
import net.vrgsoft.radiotape.presentation.service.RecorderService
import net.vrgsoft.radiotape.utils.getCurrentFormattedDateForRecord
import net.vrgsoft.radiotape.utils.getRecordsDirectory
import net.vrgsoft.radiotape.utils.show
import net.vrgsoft.radiotape.utils.subscribe
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import java.io.File
import java.util.*


class MainActivity : BaseActivity() {
    override fun diModule() = Kodein.Module("MainActivityModule") {
        bind<BillingManager>() with provider { BillingManager(this@MainActivity) }
        bind() from singleton { MainRouter(this@MainActivity) }
        bind() from singleton { MainFactory(instance(), instance(), instance(), instance(), instance(), instance(), instance("filtersChangeEvent"), instance()) }
        bind<MainContract.ViewModel>() with singleton { vm<MainViewModel>(instance<MainFactory>()) }
        bind<MainContract.Router>() with singleton { instance<MainRouter>() }
        bind<FiltersContract.Router>() with singleton { instance<MainRouter>() }
    }

    private val router: MainRouter by instance()
    private val viewModel: MainContract.ViewModel by instance()
    private lateinit var binding: ActivityMainBinding
    private var intervalJob: Job? = null

    //MediaPlayer

    private val STATE_PAUSED = 0
    private val STATE_PLAYING = 1

    private var currentState: Int = 0

    private var mediaBrowser: MediaBrowserCompat? = null
    private var mediaController: MediaControllerCompat? = null

    private var mediaBrowserConnectionCallback: MediaBrowserCompat.ConnectionCallback = object : MediaBrowserCompat.ConnectionCallback() {
        override fun onConnected() {
            super.onConnected()
            mediaController = MediaControllerCompat(applicationContext, mediaBrowser?.sessionToken!!)
            mediaController?.registerCallback(mediaControlCompat)
            MediaControllerCompat.setMediaController(this@MainActivity, mediaController)
        }
    }
    private var mediaControlCompat: MediaControllerCompat.Callback = object : MediaControllerCompat.Callback() {
        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
            super.onPlaybackStateChanged(state)
            if (state == null) {
                return
            }
            when (state.state) {
                PlaybackStateCompat.STATE_PLAYING -> currentState = STATE_PLAYING
                PlaybackStateCompat.STATE_PAUSED -> currentState = STATE_PAUSED
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel


        mediaBrowser = MediaBrowserCompat(this, ComponentName(this, PlayerService::class.java), mediaBrowserConnectionCallback, intent?.extras)
        startService(Intent(this, PlayerService::class.java))
        mediaBrowser?.connect()


        if (savedInstanceState == null) {
            router.goToRadioList()
        }

        manageCoroutines()
        setRateAppReminder()

    }

    private fun setRateAppReminder() {
        AppRate.with(this)
                .setLaunchTimes(0)
                .setInstallDays(3)
                .setRemindInterval(3)
                .monitor()
        AppRate.showRateDialogIfMeetsConditions(this)
    }

    private fun manageCoroutines() {
        launch(UI, parent = jobs) {
            viewModel.getPlayPause().subscribe {
                when (it) {
                    is Play -> {
                        binding.ivPlay.setImageResource(R.drawable.ic_pause_circle_filled)
                        binding.ivPlayTape.playAnimation()
                    }
                    is Pause -> {
                        binding.ivPlay.setImageResource(R.drawable.ic_play_circle_filled)
                        binding.ivPlayTape.stopAnimation()
                    }
                }
            }
        }

        launch(UI, parent = jobs) {
            viewModel.getQualityChange().subscribe {
                when (it) {
                    Quality.LOW -> binding.tvHQ.setText(R.string.sc_radio_list_lq)
                    Quality.MEDIUM -> binding.tvHQ.setText(R.string.sc_radio_list_mq)
                    Quality.HIGH -> binding.tvHQ.setText(R.string.sc_radio_list_hq)
                }
            }
        }

        launch(UI, parent = jobs) { viewModel.getQualityAnimation().subscribe { handleQualityAnimation(it) } }
        launch(UI, parent = jobs) { viewModel.getSnoozeDialog().subscribe { showSnoozeDialog() } }
        launch(UI, parent = jobs) {
            viewModel.getStationChange().subscribe {
                binding.station = it

            }
        }
        launch(UI, parent = jobs) { viewModel.getRecordState().subscribe { handleRecordStateChanged(it) } }
        launch(UI, parent = jobs) { viewModel.getRequestPermission().subscribe { handleRequestPermission(it) } }
    }

    @SuppressLint("CheckResult")
    private fun handleRequestPermission(permissions: Array<String>) {
        val rxPermissions = RxPermissions(this)
        if (rxPermissions.isGranted(permissions[0])) {
            viewModel.startRecording()
            return
        }

        val view = layoutInflater.inflate(R.layout.dialog_record_permission_needed, null, false)
        val dialog = AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(false)
                .create()
        view.findViewById<TextView>(R.id.tvOk).setOnClickListener { _ ->
            rxPermissions.request(*permissions).subscribe {
                if (it) viewModel.startRecording()
            }
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun handleRecordStateChanged(state: RecordState) {
        intervalJob?.cancel()
        intervalJob = null
        when (state) {
            is RecordStarted -> {
                intervalJob = launch(UI, parent = jobs) {
                    var count = 0
                    while (true) {
                        delay(500)
                        ++count
                        binding.vRecIcon.visibility = if (count % 2 == 0) VISIBLE else INVISIBLE
                    }
                }
            }
            is RecordFinished -> {
                binding.vRecIcon.show(true)
                showSaveRecordDialog(state)
            }
        }
    }

    private fun handleQualityAnimation(show: Boolean) {
        var animator = binding.llQualityRoot.animate()
                .translationY(if (show) 0f else binding.llQualityRoot.height.toFloat())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            val interpolator = if (show) DecelerateInterpolator() else AccelerateInterpolator()
            animator.interpolator = interpolator

        }
        animator.start()
    }

    private fun showSnoozeDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_snooze_alarm, null, false)

        val dialog = AlertDialog.Builder(this)
                .setView(view)
                .create()

        val tvMinutes = view.findViewById<TextView>(R.id.tvTimeToSleep)
        val sbTime = view.findViewById<ArcSeekBar>(R.id.asbTimeSeek)

        val lastTimerTime = viewModel.getLastTimerTime()
        val currentTimeMillis = System.currentTimeMillis()

        sbTime.onProgressChangedListener =
                ProgressListener { tvMinutes.text = resources.getQuantityString(R.plurals.sc_snooze_dialog_time_to_sleep_format, it, it) }

        if (lastTimerTime > currentTimeMillis) {
            sbTime.progress = ((lastTimerTime - currentTimeMillis) / (60L * 1000L)).toInt()
        } else {
            sbTime.progress = resources.getInteger(R.integer.timer_default_time)
        }

        view.findViewById<TextView>(R.id.tvCancelTimer).setOnClickListener {
            viewModel.setTimer(0)
            dialog.dismiss()
        }
        view.findViewById<TextView>(R.id.tvSetTimer).setOnClickListener {
            viewModel.setTimer(sbTime.progress)
            dialog.dismiss()
        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun showSaveRecordDialog(state: RecordFinished) {
        val view = layoutInflater.inflate(R.layout.dialog_save_record, null, false)

        val dialog = AlertDialog.Builder(this)
                .setView(view)
                .create()
        val sb = StringBuilder()
        val formatter = Formatter(sb, Locale.getDefault())
        val duration = Util.getStringForTime(sb, formatter, state.duration)

        val etFileName = view.findViewById<EditText>(R.id.etFileName)
        etFileName.setText("${getCurrentFormattedDateForRecord()}.mp3")

        view.findViewById<TextView>(R.id.tvRecordDuration).text = getString(R.string.sc_dialog_record_duration_format, duration)
        view.findViewById<TextView>(R.id.tvSave).setOnClickListener { _ ->
            if (etFileName.text.toString().isBlank()) {
                return@setOnClickListener
            }
            val fileName = etFileName.text.toString().let { if (!it.endsWith(".mp3", true)) it.plus(".mp3") else it }
            launch(UI, parent = jobs) {
                val moveFileJob = async {
                    var destFile = File(getRecordsDirectory(), fileName)
                    if (destFile.exists()) {
                        val newFileName = fileName.removeRange(fileName.length - 4, fileName.length)
                                .plus("_${UUID.randomUUID()}")
                                .plus(".mp3")
                        destFile = File(getRecordsDirectory(), newFileName)
                    }
                    File(state.filePath).copyTo(destFile, true)
                }
                moveFileJob.await()
                dialog.dismiss()
            }
        }
        view.findViewById<TextView>(R.id.tvCancel).setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        val controller = MediaControllerCompat.getMediaController(this)
        if (controller != null && controller.playbackState?.state == PlaybackStateCompat.STATE_PLAYING) {
            controller.transportControls?.pause()
        }


        viewModel.stopPlayback()
        mediaBrowser?.disconnect()
        RecorderService.newStopRecordingIntent(this)

        MediaControllerCompat.getMediaController(this)?.unregisterCallback(mediaControlCompat)
        stopService(Intent(this, PlayerService::class.java))
        NotificationManagerCompat.from(this).cancel(1)
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    override fun onBackPressed() {
        if (binding.draggableRightMenu.isOpened()) {
            binding.draggableRightMenu.close()
        } else {
            val fragmentManager = supportFragmentManager
            val isStateSaved = fragmentManager.isStateSaved
            if (isStateSaved && Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1) {
                return
            }
            if (fragmentManager.isStateSaved || !fragmentManager.popBackStackImmediate()) {
                val startMain = Intent(Intent.ACTION_MAIN)
                startMain.addCategory(Intent.CATEGORY_HOME)
                startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(startMain)
            }
        }
    }
}
