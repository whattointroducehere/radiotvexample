package net.vrgsoft.radiotape.presentation.cabinet.filters.common

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import net.cachapa.expandablelayout.ExpandableLayout
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.data.model.Filtrable
import net.vrgsoft.radiotape.databinding.ItemCategoryBinding
import net.vrgsoft.radiotape.databinding.ItemFilterBinding
import net.vrgsoft.radiotape.databinding.ItemSubfilterBinding

abstract class BaseFiltersAdapter<T : Filtrable>(
        protected var dataList: List<T> = mutableListOf(),
        protected val layoutInflater: LayoutInflater,
        protected open val parentAdapter: BaseFiltersAdapter<*>? = null,
        open val channel: ConflatedBroadcastChannel<Boolean>? = null
) : RecyclerView.Adapter<BaseFiltersAdapter.BaseFilterViewHolder<T>>() {

    val startExpand = BroadcastChannel<Int>(1)
    val endExpand = BroadcastChannel<Int>(1)

    protected var jobs = Job()
    /*
    * Устанавливает значение используемые адаптером
    * */
    fun setData(list: List<T>) {
        val oldList = this.dataList
        this.dataList = list

        DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition] === dataList[newItemPosition]
            }

            override fun getOldListSize(): Int = oldList.size

            override fun getNewListSize(): Int = dataList.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition] == dataList[newItemPosition]
            }
        }).dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseFilterViewHolder<T> {

        val view = when (viewType) {
            VIEW_TYPE_SUBFILTER -> layoutInflater.inflate(R.layout.item_subfilter, parent, false)
            VIEW_TYPE_CATEGORY -> layoutInflater.inflate(R.layout.item_category, parent, false)
            else -> layoutInflater.inflate(R.layout.item_filter, parent, false)
        }

        return generateBaseFilterViewHolder(view, viewType)
    }

    override fun onBindViewHolder(holder: BaseFilterViewHolder<T>, position: Int) {
        val filtrable = dataList[position]
        holder.bind(filtrable, channel)

        if (getItemViewType(position) == VIEW_TYPE_CATEGORY) {
            val adapter = createAdapterForCategory(position)

            if (adapter != null) {
                val categoryFilterViewHolder = holder as CategoryFilterViewHolder
                categoryFilterViewHolder.setAdapter(adapter)
                categoryFilterViewHolder.startExpandCallbacks = {
                    launch(parent = jobs) {
                        startExpand.send(position)
                    }
                }
                categoryFilterViewHolder.endExpandCallbacks = {
                    launch(parent = jobs) {
                        endExpand.send(position)
                    }
                }
            }
        }
    }

    open fun createAdapterForCategory(position: Int): BaseFiltersAdapter<*>? {
        return null
    }

    override fun getItemCount(): Int = dataList.size

    open fun cancelAllJobs() {
        jobs.cancel()
        jobs = Job()
    }

    private fun generateBaseFilterViewHolder(view: View, viewType: Int): BaseFilterViewHolder<T> {
        return when (viewType) {
            VIEW_TYPE_SUBFILTER -> SubFilterViewHolder(view, this::onClicked)
            VIEW_TYPE_CATEGORY -> CategoryFilterViewHolder(view, this::onClicked)
            else -> FilterViewHolder(view, this::onClicked)
        }
    }

    private fun checkAll(flag: Boolean) {
        dataList.forEach { it ->
            it.checked = flag

            it.getChild()?.forEach {
                it.checked = flag
            }
        }
    }

    private fun onClicked(data: T) {
        if (data.id == Filtrable.ID_ALL) {
            checkAll(data.checked)
            notifyDataSetChanged()
        }

        updateAllFiltersState()

        if (parentAdapter != null) {
            val anyChecked = isAnyChecked()
            parentAdapter?.checkItem(this, anyChecked)
        }
    }

    open fun checkItem(adapter: BaseFiltersAdapter<*>, anyChecked: Boolean) {}

    open fun isAllChecked(): Boolean {
        var isAllChecked = dataList.size != 1

        dataList.forEach {
            isAllChecked = isAllChecked && (it.checked || (it.id == Filtrable.ID_ALL))
        }

        return isAllChecked
    }

    private fun isAnyChecked(): Boolean {
        for (filterable in dataList) {
            if (filterable.checked) {
                return true
            }
        }

        return false
    }

    private fun updateAllFiltersState() {
        if (dataList.size > 1 || parentAdapter == null) {
            launch(UI, parent = jobs) {
                val index = withContext(CommonPool) {
                    val isAllChecked = isAllChecked()

                    var index = dataList.indexOfFirst { data -> data.id == Filtrable.ID_ALL }

                    if (index >= 0) {
                        val oldValue = dataList[index].checked
                        if (oldValue != isAllChecked) {
                            dataList[index].checked = isAllChecked
                        } else {
                            index = -1
                        }
                    }

                    return@withContext index
                }

                if (index >= 0) {
                    notifyItemChanged(index)
                }
            }
        }

        parentAdapter?.updateAllFiltersState()
    }

    abstract class BaseFilterViewHolder<T : Filtrable>(
            itemView: View?,
            protected val clickListener: (data: T) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {
        protected lateinit var filtrable: T
        protected var saveChannel: ConflatedBroadcastChannel<Boolean>? = null
        fun bind(filtrable: T, channel: ConflatedBroadcastChannel<Boolean>?) {
            saveChannel = channel
            this.filtrable = filtrable
            bindData(filtrable)

            itemView.setOnClickListener {
                onItemClicked(true)
                clickListener(filtrable)
            }
        }

        abstract fun onItemClicked(forceChecked: Boolean)

        abstract fun bindData(filtrable: T)
    }

    class FilterViewHolder<T : Filtrable>(
            itemView: View?,
            clickListener: (data: T) -> Unit
    ) : BaseFilterViewHolder<T>(itemView, clickListener) {

        private val binding: ItemFilterBinding = DataBindingUtil.bind(itemView!!)!!

        init {
            val color = itemView?.context?.resources?.getColor(R.color.white)!!
            @SuppressLint("RestrictedApi")
            binding.checkbox.supportButtonTintList = ColorStateList.valueOf(color)
        }

        override fun bindData(filtrable: T) {
            binding.title.text = filtrable.name
            binding.checkbox.isChecked = filtrable.checked

            binding.checkbox.setOnClickListener {
                onItemClicked(false)
                clickListener(filtrable)
            }
        }

        override fun onItemClicked(forceChecked: Boolean) {
            val checkbox = binding.checkbox

            if (forceChecked) {
                checkbox.isChecked = !checkbox.isChecked
            }

            filtrable.checked = checkbox.isChecked
            launch {
                saveChannel?.send(true)
            }
        }
    }

    class SubFilterViewHolder<T : Filtrable>(
            itemView: View?,
            clickListener: (data: T) -> Unit
    ) : BaseFilterViewHolder<T>(itemView, clickListener) {
        private val binding: ItemSubfilterBinding = DataBindingUtil.bind(itemView!!)!!
        init {
            val color = itemView?.context?.resources?.getColor(R.color.white)!!
            @SuppressLint("RestrictedApi")
            binding.checkbox.supportButtonTintList = ColorStateList.valueOf(color)
        }

        override fun bindData(filtrable: T) {
            binding.title.text = filtrable.name
            binding.checkbox.isChecked = filtrable.checked

            binding.checkbox.setOnClickListener {
                onItemClicked(false)
                clickListener(filtrable)
            }
        }

        override fun onItemClicked(forceChecked: Boolean) {

            val checkbox = binding.checkbox

            if (forceChecked) {
                checkbox.isChecked = !checkbox.isChecked
            }

            filtrable.checked = checkbox.isChecked
            launch {
                saveChannel?.send(true)
            }
        }
    }

    class CategoryFilterViewHolder<T : Filtrable>(
            itemView: View?,
            clickListener: (data: T) -> Unit
    ) : BaseFilterViewHolder<T>(itemView, clickListener), ExpandableLayout.OnExpansionUpdateListener {
        val binding: ItemCategoryBinding = DataBindingUtil.bind(itemView!!)!!

        var startExpandCallbacks: ((Int) -> Unit)? = null
        var endExpandCallbacks: ((Int) -> Unit)? = null

        override fun bindData(filtrable: T) {
            binding.title.text = filtrable.name
            binding.expandableLayout.setOnExpansionUpdateListener(this)

            itemView.post {
                if (filtrable.expanded) {
                    binding.expandableLayout.toggle(false)
                } else {
                    binding.expandableLayout.collapse(false)
                }
            }
        }

        override fun onItemClicked(forceChecked: Boolean) {
            launch {
                startExpandCallbacks?.invoke(adapterPosition)
            }
            val expandableLayout = binding.expandableLayout
            expandableLayout.toggle()

            filtrable.expanded = expandableLayout.isExpanded

            val arrow = if (filtrable.expanded) R.drawable.ic_arrow_up else R.drawable.ic_arrow_down
            binding.expandButton.setImageResource(arrow)

        }

        fun setAdapter(adapter: BaseFiltersAdapter<*>) {
            val recyclerView = binding.subRecyclerView
            recyclerView.layoutManager = LinearLayoutManager(itemView.context)
            recyclerView.adapter = adapter
        }

        override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
            endExpandCallbacks?.invoke(adapterPosition)
        }

    }

    companion object {
        const val VIEW_TYPE_FILTER = 1
        const val VIEW_TYPE_SUBFILTER = 2
        const val VIEW_TYPE_CATEGORY = 3
    }
}
