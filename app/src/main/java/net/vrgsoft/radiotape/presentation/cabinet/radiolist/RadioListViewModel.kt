package net.vrgsoft.radiotape.presentation.cabinet.radiolist

import android.databinding.ObservableBoolean
import android.text.TextUtils
import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB
import net.vrgsoft.radiotape.data.pipeline.player_actions.Pause
import net.vrgsoft.radiotape.data.pipeline.player_actions.Play
import net.vrgsoft.radiotape.domain.filters.FiltersInteractor
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.common.RadioStationState
import net.vrgsoft.radiotape.presentation.common.BaseViewModel
import net.vrgsoft.radiotape.utils.subscribe

open class RadioListViewModel(
        private val stationsInteractor: StationsInteractor,
        val actionsInteractor: PlayerActionsInteractor,
        private val filtersInteractor: FiltersInteractor,
        private val router: RadioListContract.Router,
        private val filtersChangeEvent: ConflatedBroadcastChannel<Boolean>
) : BaseViewModel(), RadioListContract.ViewModel {
    override val radioList = ConflatedChannel<List<RadioStationState>>()
    private val updateStation = Channel<Pair<RadioListContract.UpdateEvent, Pair<RadioStationState, Int>>>()
    protected var currentStation: RadioStationState? = null
    override val onItemsNotFoundEvent = ObservableBoolean()
    override val hideKeyboard = ConflatedChannel<Boolean>()

    private var allStations: List<RadioStationDB>? = null
    private var allFilters: SettingFiltersDB? = null
    private var searchQuery: String? = null

    override fun updateStation() = updateStation

    override fun bound() {
        getStations()

        launch(parent = jobs) {
            val channel = actionsInteractor.subscribeToFavoriteChange().openSubscription()
            channel.subscribe {
                updateStation.send(RadioListContract.UpdateEvent.FAVORITE to (RadioStationState(it, false) to -1))
            }
        }

        observePlayPause()

        launch(parent = jobs) {
            filtersChangeEvent.consumeEach { getStations() }
        }
    }

    override fun unbound() {

    }

    override fun search(newText: String?) {
        searchQuery = newText

        stationsInteractor.sendSearchEvent(newText ?: "")

        launch(parent = jobs) {
            proccessData()
        }
    }

    protected fun observePlayPause() {
        launch(parent = jobs) {
            actionsInteractor.subscribeToPlayPause().consumeEach {
                when (it) {
                    is Play -> {
                        if (it.station != null) {
                            currentStation = RadioStationState(it.station, true)
                            updateStation.send(RadioListContract.UpdateEvent.PLAYBACK to (RadioStationState(it.station, true) to -1))
                        }
                    }
                    is Pause -> {
                        if (it.station != null) {
                            currentStation = RadioStationState(it.station, false)
                            updateStation.send(RadioListContract.UpdateEvent.PLAYBACK to (RadioStationState(it.station, false) to -1))
                        }
                    }
                }
            }
        }
    }

    override fun getStations() {
        launch(parent = jobs) {
            allStations = withContext(DefaultDispatcher) { stationsInteractor.getAllStations() }
            allFilters = withContext(DefaultDispatcher) { filtersInteractor.loadFilters() }
            proccessData()
        }
    }

    private suspend fun proccessData() {
        if (allStations == null || allFilters == null) {
            return
        }

        // search
        val useSearch = !TextUtils.isEmpty(searchQuery)
        var data = if (useSearch) {
            allStations!!.filter { it.name.toLowerCase().contains(searchQuery!!.toLowerCase()) }
        } else {
            allStations!!
        }

        // filters
        data = if (!useSearch) {
            data.filter { it.filter(allFilters!!) }
        } else {
            data
        }

        onItemsNotFoundEvent.set(data.isEmpty())

        val stations = data.map {
            RadioStationState(it, currentStation?.radioStation?.id == it.id && currentStation?.isPlaying ?: false)
        }
        radioList.send(stations)
    }


    override fun onAddFavoriteClick(station: RadioStationState, position: Int) {
        station.radioStation.isFavorite = !station.radioStation.isFavorite
        launch(parent = jobs) {
            updateStation.send(RadioListContract.UpdateEvent.FAVORITE to (station to position))
            stationsInteractor.updateStation(station.radioStation)
            actionsInteractor.sendFavoriteChange(station.radioStation)
        }
    }

    override fun playStation(station: RadioStationState, position: Int) {
        station.isPlaying = true
        launch(parent = jobs) {
            updateStation.send(RadioListContract.UpdateEvent.PLAYBACK to (station to position))
        }
        actionsInteractor.sendPlayPause(Play(station.radioStation))
    }

    override fun pauseStation(station: RadioStationState, position: Int) {
        station.isPlaying = false
        launch(parent = jobs) {
            updateStation.send(RadioListContract.UpdateEvent.PLAYBACK to (station to position))
        }
        actionsInteractor.sendPlayPause(Pause(station.radioStation))
    }

    override fun openStation(station: RadioStationState, position: Int) {
        station.isPlaying = true
        launch(parent = jobs) {
            hideKeyboard.send(true)
            updateStation.send(RadioListContract.UpdateEvent.PLAYBACK to (station to position))
        }
        actionsInteractor.sendPlayPause(Play(station.radioStation))

        router.navigateToRadioPlayer()
    }
}
