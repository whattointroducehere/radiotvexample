package net.vrgsoft.radiotape.presentation.splash

import android.os.Bundle
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.data.model.InitialJsonObject
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.presentation.cabinet.MainActivity
import net.vrgsoft.radiotape.presentation.common.BaseActivity
import net.vrgsoft.radiotape.presentation.start.StartActivity
import net.vrgsoft.radiotape.utils.InitJsonParser
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import java.io.InputStreamReader

class SplashActivity : BaseActivity() {
    override fun diModule() = Kodein.Module("SplashActivity") {
        bind() from provider { Gson() }
        bind() from singleton { InitJsonParser(instance()) }
    }

    private val stationsInteractor: StationsInteractor by instance()
    private val initJsonParser: InitJsonParser by instance()
    private val settingsInteractor: SettingsInteractor by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        launch(UI, parent = jobs) {
            val job = async {
                if (stationsInteractor.getAllStations().isNotEmpty()) return@async

                val assetsReader = InputStreamReader(assets.open("radio_stations.json"))

                val initialJsonObject = Gson().fromJson(assetsReader, InitialJsonObject::class.java)
                stationsInteractor.saveInitialInfo(initialJsonObject)

            }
            job.await()

            val intent = if (settingsInteractor.isFirstLaunch) {
                StartActivity.newIntent(this@SplashActivity)
            } else {
                MainActivity.newIntent(this@SplashActivity)
            }
            startActivity(intent)
            finish()
        }
    }
}
