package net.vrgsoft.radiotape.presentation.cabinet.equalizer

import android.content.res.Resources
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.data.model.Band
import net.vrgsoft.radiotape.databinding.ItemBandBinding

class EqualizerAdapter(
        val layoutInflater: LayoutInflater
) : RecyclerView.Adapter<EqualizerAdapter.EqualizerViewHolder>() {

    val onBandUpdated = ConflatedBroadcastChannel<Band>()

    private var data = listOf<Band>()

    private var lowerBandLevel: Int? = null
    private var maxLevel: Int? = null

    fun setData(data: List<Band>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EqualizerViewHolder {
        val view = layoutInflater.inflate(R.layout.item_band, parent, false)
        return EqualizerViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: EqualizerViewHolder, position: Int) {
        val band = data[position]
        holder.bind(band)
    }

    fun setBandLevels(lowerBandLevel: Int, upperBandLevel: Int) {
        this.lowerBandLevel = lowerBandLevel
        this.maxLevel = getFakeBandLevel(upperBandLevel)
    }

    fun getRealBandLevel(value: Int) = value + lowerBandLevel!!

    fun getFakeBandLevel(value: Int) = value.minus(this.lowerBandLevel!!)

    inner class EqualizerViewHolder(
            itemView: View?
    ) : RecyclerView.ViewHolder(itemView), OnRangeChangedListener {

        private lateinit var band: Band
        private val binding: ItemBandBinding

        init {
            binding = DataBindingUtil.bind(itemView!!)!!
        }

        fun bind(band: Band) {
            this.band = band
            val context = itemView.context
            val resources = context.resources
            binding.label.text = getRangeText(band, resources)

            binding.seekbar.setRange(0f, maxLevel!!.toFloat())
            binding.seekbar.setValue(getFakeBandLevel(band.level).toFloat())
            binding.seekbar.setOnRangeChangedListener(this)
        }

        private fun getRangeText(band: Band, resources: Resources): String? {
            if (band.centerRange < 0) {
                return itemView.context.getString(R.string.preamp)
            }

            var centerRange = band.centerRange / 1_000
            return if (centerRange > 1000) {
                centerRange /= 1000
                resources.getString(R.string.khz_label_format, centerRange)
            } else {
                resources.getString(R.string.hz_label_format, centerRange)
            }
        }

        override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

        }

        override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
            if (isFromUser) {
                band.level = getRealBandLevel(leftValue.toInt())

                launch {
                    onBandUpdated.send(band)
                }
            }
        }

        override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

        }

    }

    companion object {
        const val VIEW_TYPE_PREAMP = 12
    }
}
