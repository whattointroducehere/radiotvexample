package net.vrgsoft.radiotape.presentation.cabinet.filters.common

import android.view.LayoutInflater
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.model.Country
import net.vrgsoft.radiotape.data.model.Filtrable.Companion.ID_ALL

class CountryFilterAdapter(layoutInflater: LayoutInflater,var  saveFilterChannel: ConflatedBroadcastChannel<Boolean>
) : BaseFiltersAdapter<Country>(layoutInflater = layoutInflater,channel = saveFilterChannel) {

    private val adaptersMap: MutableMap<Int, BaseFiltersAdapter<*>> = HashMap()

    override fun getItemViewType(position: Int): Int {
        return if (dataList[position].id > 0) return VIEW_TYPE_CATEGORY else VIEW_TYPE_FILTER
    }

    override fun createAdapterForCategory(position: Int): BaseFiltersAdapter<*> {
        return if (!adaptersMap.contains(position)) {
            val adapter = CityFilterAdapter(layoutInflater, this,saveFilterChannel)

            val country = dataList[position]
            adapter.setData(country.cityList)

            adaptersMap[position] = adapter
            adapter
        } else {
            adaptersMap[position]!!
        }
    }

    override fun checkItem(adapter: BaseFiltersAdapter<*>, anyChecked: Boolean) {
        adaptersMap.entries.filter {
            it.value === adapter
        }.forEach {
            dataList[it.key].checked = anyChecked
        }
    }

    override fun isAllChecked(): Boolean {
        var isAllChecked = true

        dataList.forEach { country ->
            val cityList = country.cityList
            cityList.forEach {
                val size = cityList.size
                isAllChecked = isAllChecked && (it.checked || (it.id == ID_ALL && size > 1))
            }
        }

        return isAllChecked
    }

    override fun cancelAllJobs() {
        super.cancelAllJobs()
        adaptersMap.forEach { it.value.cancelAllJobs() }
    }
}
