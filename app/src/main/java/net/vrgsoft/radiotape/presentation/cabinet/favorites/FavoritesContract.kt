package net.vrgsoft.radiotape.presentation.cabinet.favorites

import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListContract

interface FavoritesContract {
    interface ViewModel : RadioListContract.ViewModel {
        fun goBack()
        fun onStationUpdated(): Channel<RadioStationDB>
    }

    interface Router : RadioListContract.Router {
        fun goBack()
    }

    enum class UpdateEvent { FAVORITE }
}
