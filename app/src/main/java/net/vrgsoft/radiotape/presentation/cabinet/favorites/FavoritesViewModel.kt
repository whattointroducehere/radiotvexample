package net.vrgsoft.radiotape.presentation.cabinet.favorites

import android.os.Handler
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.domain.filters.FiltersInteractor
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListViewModel
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.common.RadioStationState

class FavoritesViewModel(
        private val stationsInteractor: StationsInteractor,
        actionsInteractor: PlayerActionsInteractor,
        filtersInteractor: FiltersInteractor,
        private val router: FavoritesContract.Router,
        filtersChangeEvent: ConflatedBroadcastChannel<Boolean>
) : RadioListViewModel(stationsInteractor, actionsInteractor, filtersInteractor, router, filtersChangeEvent), FavoritesContract.ViewModel {
    private val updateStation = Channel<RadioStationDB>()

    private var updateJob: Job? = null

    override fun onStationUpdated() = updateStation
    override fun bound() {
        observePlayPause()
        launch(parent = jobs) {
            updateStations()
        }
        launch(parent = jobs) {
            actionsInteractor.subscribeToFavoriteChange().consumeEach {
                updateStations()
            }
        }
    }

    private fun updateStations() {
        updateJob?.cancel()

        updateJob = launch(parent = jobs) {
            val savedStations = stationsInteractor.getSavedStations()
            onItemsNotFoundEvent.set(savedStations.isEmpty())

            val state = savedStations.map {
                RadioStationState(it, currentStation?.radioStation?.id == it.id && currentStation?.isPlaying ?: false)
            }
            radioList.send(state)
        }
    }

    override fun unbound() {

    }

    override fun goBack() {
        router.goBack()
    }

    override fun openStation(station: RadioStationState, position: Int) {
        goBack()
        Handler().postDelayed({ super.openStation(station, position) }, 600)

    }

}
