package net.vrgsoft.radiotape.presentation.cabinet.added_filters

import android.databinding.ObservableBoolean
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB
import net.vrgsoft.radiotape.data.model.*
import net.vrgsoft.radiotape.data.model.Filtrable.Companion.ID_ALL
import net.vrgsoft.radiotape.domain.filters.FiltersInteractor
import net.vrgsoft.radiotape.domain.genre.GenreInteractor
import net.vrgsoft.radiotape.domain.location.LocationInteractor
import net.vrgsoft.radiotape.presentation.common.BaseViewModel

class AddedFiltersViewModel(private val locationInteractor: LocationInteractor,
                            private val genreInteractor: GenreInteractor,
                            private val filtersInteractor: FiltersInteractor
) : BaseViewModel(), AddedFiltersContract.ViewModel {

    override val countryChannel = ConflatedBroadcastChannel<List<Filtrable>>()
    override val genresChannel = ConflatedBroadcastChannel<List<Filtrable>>()
    override val showFiltersDialog = ConflatedBroadcastChannel<Boolean>()
    override val goBackChannel = ConflatedBroadcastChannel<Boolean>()

    private lateinit var countries: List<Country>
    private lateinit var genres: List<Genre>

    override val progressVisibility = ObservableBoolean()

    private var saveFiltersJob: Job? = null
    private var loadFiltersJob: Job? = null

    // list for displaying data
    private lateinit var countryList: MutableList<Filtrable>
    private lateinit var genreList: MutableList<Filtrable>

    override fun bound() {
        observeFiltersChanges()
    }

    private fun observeFiltersChanges() {
        launch(parent = jobs) {
            filtersInteractor.observeFiltersChanges().consumeEach {
                if (it) {
                    delay(50)
                    loadData()
                }
            }
        }
    }

    override fun loadData() {
        loadFiltersJob?.cancel()
        loadFiltersJob = launch(parent = jobs) {
            progressVisibility.set(true)

            countryList = mutableListOf()
            genreList = mutableListOf()

            val countriesJob = async(parent = jobs) { locationInteractor.getAllCountries() }
            val genresJob = async(parent = jobs) { genreInteractor.getAllGenres() }

            countries = countriesJob.await()
            genres = genresJob.await()

            val filtersDB = withContext(DefaultDispatcher) { filtersInteractor.loadFilters() }
            applyFilters(filtersDB, countries, genres)

            countryChannel.send(countryList)
            genresChannel.send(genreList)

            progressVisibility.set(false)
        }
    }

    private fun applyFilters(filters: SettingFiltersDB, countries: List<Country>, genres: List<Genre>) {
        SettingFiltersDB.applyFilters(countries, filters.countriesId, filters.cityIds, countryList)
        SettingFiltersDB.applyFilters(genres, filters.genreIds, filters.subgenreIds, genreList)
    }

    override fun onItemDeleted(it: Filtrable) {
        launch(parent = jobs) {
            when (it) {
                is Country -> countryList.remove(it)
                is City -> countryList.remove(it)
                is Genre -> genreList.remove(it)
                is Subgenre -> genreList.remove(it)
            }

            countryChannel.send(countryList)
            genresChannel.send(genreList)

            when (it) {
                is Country -> removeFiltrable(countries, it) { Country(ID_ALL) }
                is City -> removeFiltrable(countries, it) { Country(ID_ALL) }
                is Genre -> removeFiltrable(genres, it) { Genre(ID_ALL) }
                is Subgenre -> removeFiltrable(genres, it) { Genre(ID_ALL) }
            }

            saveFilters()
        }
    }

    private fun saveFilters() {
        if (!::countries.isInitialized || !::genres.isInitialized) {
            return
        }

        saveFiltersJob?.cancel()
        saveFiltersJob = launch(parent = jobs) {
            filtersInteractor.saveFilters(countries, genres, false)
        }
    }

    private fun <T : Filtrable> removeFiltrable(mainList: List<T>, it: T, entityCreator: () -> T) {
        val indexOf = mainList.indexOf(it)

        if (indexOf >= 0) { //contains in main list
            mainList[indexOf].checked = false
            mainList[indexOf].getChild()?.forEach {
                it.checked = false
            }
        } else {
            val parentId = it.getParentId()

            if (parentId >= 0) {
                val element = entityCreator()
                element.id = parentId

                val indexOfMainElement = mainList.indexOf(element)

                if (indexOfMainElement >= 0) { //find parent
                    val child = mainList[indexOfMainElement].getChild()

                    if (child != null) {
                        val subItemIndex = child.indexOf(it)

                        if (subItemIndex >= 0) {
                            child[subItemIndex].checked = false
                        }
                    }
                }
            }
        }
    }

    override fun hasCheckedItems(): Boolean {
        return if (::countries.isInitialized && ::genres.isInitialized) {
            containsCheckedItems(countries) && containsCheckedItems(genres)
        } else {
            false
        }
    }

    private fun containsCheckedItems(filtrableList: List<Filtrable>): Boolean {
        filtrableList.forEach { item ->
            if (item.checked) return true

            item.getChild()?.forEach {
                if (it.checked) return true
            }
        }

        return false
    }

    override fun goBack() {
        launch(parent = jobs) {
            goBackChannel.send(true)
        }
    }

    override fun onAddButtonClicked() {
        launch(parent = jobs) {
            showFiltersDialog.send(true)
        }
    }

}
