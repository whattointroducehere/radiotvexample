package net.vrgsoft.radiotape.presentation.start

import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.presentation.common.BaseViewModel

class StartViewModel(
        private val router: StartContract.Router,
        private val settingsInteractor: SettingsInteractor
) : BaseViewModel(), StartContract.ViewModel {

    override fun bound() {

    }

    override fun onFiltersSaved() {

    }


    override fun onNextClicked() {
        launch(parent = jobs) {
            settingsInteractor.disableFirstLaunchState()
            router.openMainScreen()
        }
    }

}
