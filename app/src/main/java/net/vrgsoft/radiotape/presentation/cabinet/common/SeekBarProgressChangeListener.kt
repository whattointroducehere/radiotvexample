package net.vrgsoft.radiotape.presentation.cabinet.common

import android.widget.SeekBar

fun onProgressChangedDelegate(delegate: (progress: Int) -> Unit) = object : SeekBar.OnSeekBarChangeListener {
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        delegate(progress)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }
}
