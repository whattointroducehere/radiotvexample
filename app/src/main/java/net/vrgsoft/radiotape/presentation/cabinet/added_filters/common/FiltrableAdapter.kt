package net.vrgsoft.radiotape.presentation.cabinet.added_filters.common

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.data.model.Filtrable
import net.vrgsoft.radiotape.databinding.ItemFilterChipBinding

class FiltrableAdapter(
        private val layoutInflater: LayoutInflater
) : RecyclerView.Adapter<FiltrableAdapter.FiltrableViewHolder>() {
    private var data: List<Filtrable> = listOf()
    val onItemDeletedEvents = ConflatedBroadcastChannel<Filtrable>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FiltrableViewHolder {
        val view = layoutInflater.inflate(R.layout.item_filter_chip, parent, false)
        return FiltrableViewHolder(view)
    }

    fun setData(list: List<Filtrable>) {
        val oldList = this.data
        this.data = ArrayList(list)

        DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition] === data[newItemPosition]
            }

            override fun getOldListSize(): Int = oldList.size

            override fun getNewListSize(): Int = data.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition] == data[newItemPosition]
            }
        }).dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: FiltrableViewHolder, position: Int) {
        val filtrable = data[position]
        holder.bind(filtrable)
    }

    inner class FiltrableViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = DataBindingUtil.bind<ItemFilterChipBinding>(itemView)!!

        fun bind(filtrable: Filtrable) {
            binding.title.text = filtrable.name

            binding.closeButton.setOnClickListener {
                launch {
                    onItemDeletedEvents.send(filtrable)
                }
            }
        }

    }
}
