package net.vrgsoft.radiotape.presentation.cabinet.equalizer

import android.content.IntentFilter
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.widget.ArrayAdapter
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.databinding.FragmentEqualizerBinding
import net.vrgsoft.radiotape.presentation.cabinet.MainRouter
import net.vrgsoft.radiotape.presentation.common.BaseFragment
import net.vrgsoft.radiotape.presentation.common.EqualizerFactory
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class EqualizerFragment : BaseFragment<FragmentEqualizerBinding>() {
    override fun diModule(): Kodein.Module = Kodein.Module("EqualizerFragment") {
        bind<EqualizerContract.Router>() with singleton { instance<MainRouter>() }
        bind<EqualizerFactory>() with singleton {
            EqualizerFactory(instance(), instance(), instance(), instance(), instance())
        }
        bind<EqualizerContract.ViewModel>() with singleton {
            vm<EqualizerViewModel>(instance<EqualizerFactory>())
        }
        bind<VolumeChanged>() with singleton { VolumeChanged() }
        bind<EqualizerAdapter>() with provider { EqualizerAdapter(instance()) }
    }

    private val viewModel: EqualizerContract.ViewModel by instance()
    private val volumeChangedReceiver: VolumeChanged by instance()
    private val adapter: EqualizerAdapter by instance()

    private var alertDialog: AlertDialog? = null

    override fun viewCreated() {
        binding.apply {
            viewModel = this@EqualizerFragment.viewModel

            bandRecyclerView.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
            bandRecyclerView.adapter = adapter
        }

        viewModel.bound()
        addObservables()
    }

    private fun addObservables() {
        startObserveVolumeChanges()
        observeOpenPresetListDialog()
        observeShowBandController()
        observeBandLevelChanges()
        observeShowResetSettingsDialog()
    }

    private fun observeShowResetSettingsDialog() {
        launch(parent = jobs) {
            viewModel.showResetSettingsDialog.consumeEach {
                launch(UI) {
                    alertDialog?.hide()
                    alertDialog = AlertDialog.Builder(context!!, R.style.AppDialog)
                            .setTitle(R.string.equalizer)
                            .setMessage(R.string.equalizer_reset_msg)
                            .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                            .setPositiveButton(R.string.confirm) { dialog, _ ->
                                viewModel.resetSettings()
                                dialog.dismiss()
                            }
                            .show()
                }
            }
        }
    }

    private fun observeBandLevelChanges() {
        launch(parent = jobs) {
            adapter.onBandUpdated.consumeEach {
                viewModel.onBandLevelChanged(it)
            }
        }
    }

    private fun observeShowBandController() {
        launch(parent = jobs) {
            viewModel.showBandController.consumeEach {
                launch(UI) {
                    val lowerBandLevel = viewModel.getLowerBandLevel()
                    val upperBandLevel = viewModel.getUpperBandLevel()

                    adapter.setBandLevels(lowerBandLevel, upperBandLevel)
                    adapter.setData(it)

                }
            }
        }
    }

    private fun observeOpenPresetListDialog() {
        launch(parent = jobs) {
            viewModel.openPresetList.consumeEach {
                val simpleAdapter = ArrayAdapter(context!!, R.layout.item_preset, R.id.preset_title, it)

                launch(UI) {
                    alertDialog?.hide()
                    alertDialog = AlertDialog.Builder(context!!, R.style.AppDialog)
                            .setAdapter(simpleAdapter) { _, which -> viewModel.selectPreset(which, true, true) }
                            .show()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.unbound()
        context?.unregisterReceiver(volumeChangedReceiver)
    }

    private fun startObserveVolumeChanges() {
        val intentFilter = IntentFilter("android.media.VOLUME_CHANGED_ACTION")
        context?.registerReceiver(volumeChangedReceiver, intentFilter)

        launch(parent = jobs) {
            volumeChangedReceiver.onVolumeChanged.consumeEach {
                viewModel.notifyVolumeChanged()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): EqualizerFragment {
            return EqualizerFragment()
        }
    }
}
