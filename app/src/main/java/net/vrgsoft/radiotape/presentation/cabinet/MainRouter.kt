package net.vrgsoft.radiotape.presentation.cabinet

import android.content.Intent
import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.widget.FrameLayout
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.presentation.cabinet.added_filters.AddedFiltersFragment
import net.vrgsoft.radiotape.presentation.cabinet.equalizer.EqualizerContract
import net.vrgsoft.radiotape.presentation.cabinet.equalizer.EqualizerFragment
import net.vrgsoft.radiotape.presentation.cabinet.favorites.FavoritesContract
import net.vrgsoft.radiotape.presentation.cabinet.favorites.FavoritesFragment
import net.vrgsoft.radiotape.presentation.cabinet.filters.FiltersContract
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListContract
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListFragment
import net.vrgsoft.radiotape.presentation.cabinet.radioplayer.RadioPlayerContract
import net.vrgsoft.radiotape.presentation.cabinet.radioplayer.RadioPlayerFragment
import net.vrgsoft.radiotape.presentation.cabinet.paywall.PaywallContract
import net.vrgsoft.radiotape.presentation.cabinet.paywall.PaywallFragment
import net.vrgsoft.radiotape.presentation.service.RecorderService
import net.vrgsoft.radiotape.utils.*

class MainRouter(private val activity: MainActivity) :
        PaywallContract.Router,
        RadioListContract.Router,
        RadioPlayerContract.Router,
        FiltersContract.Router,
        MainContract.Router,
        FavoritesContract.Router,
        EqualizerContract.Router {

    override fun navigateToPaywall() {
        val paywallFragment = PaywallFragment.newInstance()
        activity.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit,
                        R.anim.fragment_enter, R.anim.fragment_exit)
                .add(R.id.container, paywallFragment, paywallFragment.javaClass.simpleName)
                .addToBackStack(paywallFragment.javaClass.simpleName)
                .commit()
    }

    override fun closePayWallFragment() {
        activity.supportFragmentManager.popBackStackImmediate()
    }

    //    val favoritesFragment = FavoritesFragment.newInstance()
//    val equalizerFragment = EqualizerFragment.newInstance()
//    val filtersFragment = AddedFiltersFragment.newInstance(true)
    override fun navigateToRadioPlayer() {
        if (activity.supportFragmentManager.findFragmentByTag(RadioPlayerFragment::class.java.simpleName) == null) {
            activity.supportFragmentManager.addSliding(R.id.mainContainer, RadioPlayerFragment.newInstance())
        }
    }

    fun goToRadioList() {
        activity.supportFragmentManager.beginTransaction()
                .add(R.id.filterContainer, AddedFiltersFragment.newInstance(true))
                .add(R.id.equalizerContainer, EqualizerFragment.newInstance())
                .add(R.id.favoritesContainer, FavoritesFragment.newInstance())
                .commit()
        activity.supportFragmentManager.replaceWithoutBackStack(R.id.mainContainer, RadioListFragment.newInstance())
    }

    override fun goToFavorites() {
//        activity.supportFragmentManager.beginTransaction()
//                .show(favoritesFragment)
//                .hide(filtersFragment)
//                .hide(equalizerFragment)
//                .commit()
//        collapseView()
//        expandView()
    }

    override fun stopRecording() {
        activity.startService(RecorderService.newStopRecordingIntent(activity))
    }

    override fun goBack() {
        activity.onBackPressed()
    }

    override fun onFiltersFragmentClose() {
        goBack()
    }

    override fun startRecording(stream: String?) {
        activity.startService(RecorderService.newStartRecordingIntent(activity, stream))
    }

    fun openPhoneDial(phoneNumber: String) {
        activity.launchExternalIntent(Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber")))
    }

    fun openEmail(email: String) {
        activity.launchExternalIntent(Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$email")))
    }

    fun openWebSite(url: String) {
        val schemeUrl = if (!url.startsWith("http")) "http://$url" else url
        activity.launchExternalIntent(Intent(Intent.ACTION_VIEW, Uri.parse(schemeUrl)))
    }

    override fun goToEqualizerScreen() {
//        activity.supportFragmentManager.beginTransaction()
//                .hide(favoritesFragment)
//                .hide(filtersFragment)
//                .show(equalizerFragment)
//                .commit()
//        collapseView()
//        expandView()
    }

    private fun expandView(callbacks: ExpandCallbacks? = null) {
        activity.findViewById<FrameLayout>(R.id.flSwipeContent).expandFromRight(callbacks)
    }

    private fun collapseView() {
        activity.findViewById<ConstraintLayout>(R.id.clMainLayout).collapseToLeft()
    }

    override fun goToFiltersScreen() {
//        activity.supportFragmentManager.beginTransaction()
//                .hide(favoritesFragment)
//                .show(filtersFragment)
//                .hide(equalizerFragment)
//                .commit()
//        collapseView()
//        expandView()

    }
}

typealias ExpandCallbacks = () -> Unit
