package net.vrgsoft.radiotape.presentation.cabinet.paywall

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import net.vrgsoft.radiotape.presentation.common.BaseVM

interface PaywallContract {
    interface ViewModel: BaseVM {
        val isSubscriptionAvailable: ObservableBoolean
        val monthlySubscription: ObservableField<String>
        val annualSubscription: ObservableField<String>

        fun startMonthlySubscription()
        fun startAnnualSubscription()
        fun onWallClicked()
    }

    interface Router {
        fun closePayWallFragment()
    }
}
