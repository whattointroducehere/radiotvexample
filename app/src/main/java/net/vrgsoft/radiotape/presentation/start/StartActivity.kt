package net.vrgsoft.radiotape.presentation.start

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.databinding.ActivityStartBinding
import net.vrgsoft.radiotape.presentation.cabinet.added_filters.AddedFiltersFragment
import net.vrgsoft.radiotape.presentation.cabinet.filters.FiltersContract
import net.vrgsoft.radiotape.presentation.common.BaseActivity
import net.vrgsoft.radiotape.presentation.common.StartFactory
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class StartActivity : BaseActivity() {
    override fun diModule() = Kodein.Module("StartActivity") {
        bind<StartRouter>() with singleton { StartRouter(this@StartActivity) }
        bind<StartContract.Router>() with singleton { instance<StartRouter>() }
        bind<StartFactory>() with singleton { StartFactory(instance(), instance()) }
        bind<StartContract.ViewModel>() with singleton { vm<StartViewModel>(instance<StartFactory>()) }
        bind<FiltersContract.Router>() with singleton { instance<StartRouter>() }
    }

    private val viewModel: StartContract.ViewModel by instance()
    private lateinit var filtersFragment: AddedFiltersFragment
    private lateinit var binding: ActivityStartBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_start)

        filtersFragment = AddedFiltersFragment.newInstance()
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, filtersFragment)
                .commit()

        binding.viewModel = this@StartActivity.viewModel

        viewModel.bound()
        addObservables()
    }

    private fun addObservables() {
        launch(parent = jobs) {
            while (isActive) {
                delay(500)
                withContext(UI) {
                    binding.nextButton.isEnabled = filtersFragment.hasCheckedItems()
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context) = Intent(context, StartActivity::class.java)
    }
}
