package net.vrgsoft.radiotape.presentation.common

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.res.Resources
import android.media.AudioManager
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.repository.resources.ResourcesStorage
import net.vrgsoft.radiotape.domain.filters.FiltersInteractor
import net.vrgsoft.radiotape.domain.genre.GenreInteractor
import net.vrgsoft.radiotape.domain.location.LocationInteractor
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.domain.record.RecordInteractor
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.presentation.cabinet.MainContract
import net.vrgsoft.radiotape.presentation.cabinet.MainViewModel
import net.vrgsoft.radiotape.presentation.cabinet.added_filters.AddedFiltersViewModel
import net.vrgsoft.radiotape.presentation.cabinet.equalizer.EqualizerContract
import net.vrgsoft.radiotape.presentation.cabinet.equalizer.EqualizerViewModel
import net.vrgsoft.radiotape.presentation.cabinet.favorites.FavoritesContract
import net.vrgsoft.radiotape.presentation.cabinet.favorites.FavoritesViewModel
import net.vrgsoft.radiotape.presentation.cabinet.filters.FiltersContract
import net.vrgsoft.radiotape.presentation.cabinet.filters.FiltersViewModel
import net.vrgsoft.radiotape.presentation.cabinet.paywall.BillingManager
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListContract
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListViewModel
import net.vrgsoft.radiotape.presentation.cabinet.radioplayer.RadioPlayerContract
import net.vrgsoft.radiotape.presentation.cabinet.radioplayer.RadioPlayerViewModel
import net.vrgsoft.radiotape.presentation.cabinet.paywall.PaywallContract
import net.vrgsoft.radiotape.presentation.cabinet.paywall.PaywallViewModel
import net.vrgsoft.radiotape.presentation.start.StartContract
import net.vrgsoft.radiotape.presentation.start.StartViewModel
import net.vrgsoft.radiotape.utils.AppEqualizer

class PaywallFactory(
        private val router: PaywallContract.Router,
        private val resources: Resources,
        private val billingManager: BillingManager): ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PaywallViewModel(router, resources, billingManager) as T
    }
}

class RadioListFactory(private val stationsInteractor: StationsInteractor,
                       private val playerActionsInteractor: PlayerActionsInteractor,
                       private val filtersInteractor: FiltersInteractor,
                       private val router: RadioListContract.Router,
                       private val filtersChangeEvent: ConflatedBroadcastChannel<Boolean>) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return RadioListViewModel(stationsInteractor, playerActionsInteractor, filtersInteractor, router, filtersChangeEvent) as T
    }
}

class FavoritesFactory(private val stationsInteractor: StationsInteractor,
                       private val playerActionsInteractor: PlayerActionsInteractor,
                       private val filtersInteractor: FiltersInteractor,
                       private val router: FavoritesContract.Router,
                       private val filtersChangeEvent: ConflatedBroadcastChannel<Boolean>) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FavoritesViewModel(stationsInteractor, playerActionsInteractor, filtersInteractor, router, filtersChangeEvent) as T
    }
}

class RadioPlayerFactory(private val router: RadioPlayerContract.Router,
                         private val playerActionsInteractor: PlayerActionsInteractor,
                         private val actionsInteractor: PlayerActionsInteractor) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return RadioPlayerViewModel(router, playerActionsInteractor, actionsInteractor) as T
    }
}

class MainFactory(private val router: MainContract.Router,
                  private val billingManager: BillingManager,
                  private val playerActionsInteractor: PlayerActionsInteractor,
                  private val stationsInteractor: StationsInteractor,
                  private val settingsInteractor: SettingsInteractor,
                  private val recordInteractor: RecordInteractor,
                  private val filtersChangeEvent: ConflatedBroadcastChannel<Boolean>,
                  private val filtersInteractor: FiltersInteractor) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(router, billingManager, playerActionsInteractor, stationsInteractor, settingsInteractor, recordInteractor, filtersChangeEvent, filtersInteractor) as T
    }
}

class FiltersFactory(private val locationInteractor: LocationInteractor,
                     private val genreInteractor: GenreInteractor,
                     private val filtersInteractor: FiltersInteractor,
                     private val router: FiltersContract.Router,
                     private val resourcesStorage: ResourcesStorage,
                     private val saveFilterChannel: ConflatedBroadcastChannel<Boolean>) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FiltersViewModel(locationInteractor, genreInteractor, filtersInteractor, router, resourcesStorage, saveFilterChannel) as T
    }
}

class AddedFiltersFactory(private val locationInteractor: LocationInteractor,
                          private val genreInteractor: GenreInteractor,
                          private val filtersInteractor: FiltersInteractor
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AddedFiltersViewModel(locationInteractor, genreInteractor, filtersInteractor) as T
    }
}

class EqualizerFactory(
        private val audioManager: AudioManager,
        private val router: EqualizerContract.Router,
        private val settingsInteractor: SettingsInteractor,
        private val appEqualizer: AppEqualizer,
        private val resources: ResourcesStorage
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return EqualizerViewModel(audioManager, router, appEqualizer, settingsInteractor, resources) as T
    }
}

class StartFactory(
        private val router: StartContract.Router,
        private val settingsInteractor: SettingsInteractor
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StartViewModel(router, settingsInteractor) as T
    }

}
