package net.vrgsoft.radiotape.presentation.cabinet.equalizer

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.launch

class VolumeChanged : BroadcastReceiver() {

    val onVolumeChanged = ConflatedBroadcastChannel<Boolean>()

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals("android.media.VOLUME_CHANGED_ACTION")) {
            launch {
                onVolumeChanged.send(true)
            }
        }
    }
}
