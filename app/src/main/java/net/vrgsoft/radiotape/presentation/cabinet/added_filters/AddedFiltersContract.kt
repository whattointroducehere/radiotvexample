package net.vrgsoft.radiotape.presentation.cabinet.added_filters

import android.databinding.ObservableBoolean
import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.model.Filtrable
import net.vrgsoft.radiotape.presentation.common.BaseVM

class AddedFiltersContract {
    interface ViewModel : BaseVM {
        fun hasCheckedItems(): Boolean


        fun onAddButtonClicked()

        fun onItemDeleted(it: Filtrable)
        fun goBack()
        fun loadData()

        val countryChannel: BroadcastChannel<List<Filtrable>>
        val genresChannel: BroadcastChannel<List<Filtrable>>
        val showFiltersDialog: ConflatedBroadcastChannel<Boolean>
        val goBackChannel : ConflatedBroadcastChannel<Boolean>


        val progressVisibility: ObservableBoolean
    }
}
