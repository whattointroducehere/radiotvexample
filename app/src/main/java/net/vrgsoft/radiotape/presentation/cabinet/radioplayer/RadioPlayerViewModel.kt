package net.vrgsoft.radiotape.presentation.cabinet.radioplayer

import android.databinding.ObservableField
import android.text.TextUtils
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.pipeline.player_actions.PlayPauseAction
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.presentation.common.BaseViewModel
import net.vrgsoft.radiotape.utils.subscribe

class RadioPlayerViewModel(
        private val router: RadioPlayerContract.Router,
        private val playerActionsInteractor: PlayerActionsInteractor,
        private val actionsInteractor: PlayerActionsInteractor
) : BaseViewModel(), RadioPlayerContract.ViewModel {
    private val playPauseActions = Channel<PlayPauseAction>(Channel.CONFLATED)
    private val showInfoDialog = Channel<Boolean>()

    override fun getPlayPause() = playPauseActions
    override fun getShowInfoDialog() = showInfoDialog

    override val songNameText = ObservableField<String>()

    init {
        launch(parent = jobs) {
            val channel = playerActionsInteractor.subscribeToPlayPause().openSubscription()
            channel.subscribe { playPauseActions.send(it) }
        }
    }

    override fun bound() {
        launch(parent = jobs) {
            actionsInteractor.subscribeToIcyHeaderChange().consumeEach {
                val streamTitle = it.streamTitle
                if (!TextUtils.isEmpty(streamTitle)) {
                    songNameText.set(streamTitle!!)
                }
            }
        }
    }

    override fun onInfoClick() {
        launch(parent = jobs) {
            showInfoDialog.send(true)
        }
    }

    override fun showEqualizer() {
        router.goToEqualizerScreen()
    }

    override fun goBack() {
        router.goBack()
    }

}
