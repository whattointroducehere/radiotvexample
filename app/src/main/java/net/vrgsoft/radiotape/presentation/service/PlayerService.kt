package net.vrgsoft.radiotape.presentation.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.app.NotificationCompat.MediaStyle
import android.support.v4.media.session.MediaButtonReceiver
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.view.KeyEvent
import android.widget.RemoteViews
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.analytics.DefaultAnalyticsListener
import com.google.android.exoplayer2.source.ExtractorMediaSource
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.pipeline.player_actions.Next
import net.vrgsoft.radiotape.data.pipeline.player_actions.Pause
import net.vrgsoft.radiotape.data.pipeline.player_actions.Play
import net.vrgsoft.radiotape.data.pipeline.player_actions.Previous
import net.vrgsoft.radiotape.data.pipeline.player_actions.Quality
import net.vrgsoft.radiotape.di.GlideApp
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.presentation.common.BaseMediaService
import net.vrgsoft.radiotape.utils.AppEqualizer
import net.vrgsoft.radiotape.utils.MediaStyleHelper
import net.vrgsoft.radiotape.utils.subscribe
import org.jetbrains.anko.runOnUiThread
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

const val CHANNEL_ID = "RadioTapePlayer"
private const val BUNDLE_ACTION = "net.vrgsoft.radiotape.BUNDLE_ACTION"
private const val ACTION_PLAY = 200
private const val ACTION_PAUSE = 201
private const val ACTION_NEXT = 202
private const val ACTION_PREVIOUS = 203
private const val ACTION_CLOSE = 204
private const val NOTIFICATION_ID = 1

class PlayerService : BaseMediaService(), AudioManager.OnAudioFocusChangeListener {


    override fun diModule() = Kodein.Module("PlayerServiceModule") {

    }

    private val actionsInteractor: PlayerActionsInteractor by instance()
    private val settingsInteractor: SettingsInteractor by instance()
    private val stationsInteractor: StationsInteractor by instance()
    private val mAppEqualizer: AppEqualizer by instance()
    private val mediaSourceFactory: ExtractorMediaSource.Factory by instance()
    private val player: SimpleExoPlayer by instance()

    private var currentRadioStation: RadioStationDB? = null

    private lateinit var currentQuality: Quality
    private var notificationView: RemoteViews? = null
    private var notification: Notification? = null
    private var notificationClosed = false
    private var mediaSession: MediaSessionCompat? = null
    private var isWasPlaying = false
    private var currentSongName = ""
    private var isPlaying = false

    private val lock = Any()

    private var mNoisyReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (player != null) {
                actionsInteractor.sendPlayPause(Pause(currentRadioStation))
            }
        }
    }
    private val mediaSessionCallback = object : MediaSessionCompat.Callback() {
        override fun onPlay() {
            super.onPlay()

            mediaSession?.isActive = true
            setMediaPlaybackState(PlaybackStateCompat.STATE_PLAYING)
            processAction(ACTION_PLAY)
            isPlaying = true
            showPlayingNotification()
        }

        override fun onPause() {
            super.onPause()
            isPlaying = false
            mediaSession?.isActive = false
            processAction(ACTION_PAUSE)
            setMediaPlaybackState(PlaybackStateCompat.STATE_PAUSED)
            showPausedNotification(false)
        }

        override fun onMediaButtonEvent(mediaButtonEvent: Intent?): Boolean {
            val keyEvent = mediaButtonEvent?.extras!!["android.intent.extra.KEY_EVENT"] as KeyEvent
            when (keyEvent.keyCode) {
                KeyEvent.KEYCODE_MEDIA_PLAY -> onPlay()
                KeyEvent.KEYCODE_MEDIA_PAUSE -> onPause()
                KeyEvent.KEYCODE_MEDIA_NEXT -> processAction(ACTION_NEXT)
                KeyEvent.KEYCODE_MEDIA_PREVIOUS -> processAction(ACTION_PREVIOUS)
            }
            return super.onMediaButtonEvent(mediaButtonEvent)

        }
    }
    private val phoneStateListener = object : PhoneStateListener() {
        override fun onCallStateChanged(state: Int, incomingNumber: String?) {
            when (state) {
                TelephonyManager.CALL_STATE_RINGING -> {
                    if (isPlaying) {
                        isWasPlaying = true
                        processAction(ACTION_PAUSE)
                    }
                }
                TelephonyManager.CALL_STATE_IDLE -> {
                    if (isWasPlaying) {
                        isWasPlaying = false
                        processAction(ACTION_PLAY)
                    }
                }
                TelephonyManager.CALL_STATE_OFFHOOK -> {
                    if (isPlaying) {
                        isWasPlaying = true
                        processAction(ACTION_PAUSE)
                    }
                }
            }
            super.onCallStateChanged(state, incomingNumber)
        }
    }

    override fun onCreate() {
        super.onCreate()
        isServiceRunning = true

        currentQuality = settingsInteractor.getQuality()
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                    CHANNEL_ID,
                    getString(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT)
            channel.importance = NotificationManager.IMPORTANCE_MIN
            notificationManager?.createNotificationChannel(channel)
        }

        //createNotification()

        player.addAnalyticsListener(object : DefaultAnalyticsListener() {
            override fun onAudioSessionId(eventTime: AnalyticsListener.EventTime?, audioSessionId: Int) {
                super.onAudioSessionId(eventTime, audioSessionId)
                mAppEqualizer.attachEqualizer(audioSessionId)
            }
        })
        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)
        initMediaSession()
        initNoisyReceiver()
        launch(parent = jobs) {
            val channel = actionsInteractor.subscribeToPlayPause().openSubscription()
            channel.subscribe {
                when (it) {
                    is Play -> {
                        isPlaying = true
                        notificationClosed = false
                        if (successfullyRetrievedAudioFocus().not()) {

                        }
                        if (currentRadioStation == null) {
                            showPlayingNotificationWithDelay()
                        }
                        if (currentRadioStation == null || currentRadioStation?.id != it.station?.id) {
                            currentRadioStation = it.station
                            prepareSource(currentRadioStation?.stream)
                            player.playWhenReady = true
                            showPlayingNotificationWithDelay()
                            stationsInteractor.saveLastPlayedStationId(it.station?.id ?: 0)
                        } else {
                            player.playWhenReady = true
                            showPlayingNotificationWithDelay()
                        }
                    }
                    is Pause -> {
                        isPlaying=false
                        player.playWhenReady = false
                        if (!notificationClosed) {
                            stopForeground(false)

                            launch {
                                delay(50)
                                if(isServiceRunning && !isPlaying){
                                    showPausedNotification(false)
                                }
                            }
                        }
                    }
                }
            }
        }

        launch {
            val channel = actionsInteractor.subscribeToNextPrevious().openSubscription()
            channel.subscribe {
                when (it) {
                    is Next -> {
                    }
                    is Previous -> {
                    }
                }
            }
        }

        launch {
            val channel = actionsInteractor.subscribeToQualityChange().openSubscription()
            channel.subscribe {
                currentQuality = it.quality
                settingsInteractor.saveQuality(currentQuality)

                if (currentRadioStation != null) {
                    val mediaSource = mediaSourceFactory.createMediaSource(Uri.parse(
                            "${currentRadioStation?.stream}${currentQuality.postfix}"
                    ))
                    player.prepare(mediaSource)
                }
            }
        }
        launch {
            val channel = actionsInteractor.subscribeToIcyHeaderChange().openSubscription()
            channel.subscribe {
                if (it.streamTitle != null) {
                    currentSongName = it.streamTitle
                    showPlayingNotificationWithDelay()
                }
            }
        }
    }

    private fun showPlayingNotificationWithDelay() {
        launch {
            delay(50)
            if (isServiceRunning && isPlaying) {
                showPlayingNotification()
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            val action = intent.getIntExtra(BUNDLE_ACTION, -1)

            if (action > 0) {
                processAction(action)
            }
        }
        MediaButtonReceiver.handleIntent(mediaSession, intent)
        return super.onStartCommand(intent, flags, startId)
    }

    private fun processAction(action: Int) {
        isWasPlaying = false
        when (action) {
            ACTION_PLAY -> actionsInteractor.sendPlayPause(Play(currentRadioStation))
            ACTION_PAUSE -> actionsInteractor.sendPlayPause(Pause(currentRadioStation))
            ACTION_NEXT -> actionsInteractor.sendNextPrevious(Next())
            ACTION_PREVIOUS -> actionsInteractor.sendNextPrevious(Previous())
            ACTION_CLOSE -> {
                stopForeground(true)
                notificationClosed = true
                actionsInteractor.sendPlayPause(Pause(currentRadioStation))
                val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
                notificationManager?.cancel(NOTIFICATION_ID)
            }
        }
    }

    private fun prepareSource(url: String?) {
        val mediaSource = mediaSourceFactory.createMediaSource(Uri.parse("$url${currentQuality.postfix}"))
        player.prepare(mediaSource)
    }

    override fun onDestroy() {
        super.onDestroy()
        mAppEqualizer.detachEqualizer()
        isServiceRunning = false
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioManager.abandonAudioFocus(this)
        processAction(ACTION_CLOSE)
        actionsInteractor.dispose()
        settingsInteractor.dispose()
        stationsInteractor.dispose()
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager?.deleteNotificationChannel(CHANNEL_ID)
        }
        //  player.release()
        mediaSession?.isActive = false
        unregisterReceiver(mNoisyReceiver)
        mediaSession?.setCallback(null)
        mediaSession?.release()
        mediaSession = null
        stopForeground(true)
        notificationManager?.cancel(NOTIFICATION_ID)
        NotificationManagerCompat.from(this@PlayerService).cancel(NOTIFICATION_ID)


    }

    private fun showPlayingNotification() {
        synchronized(lock){
            initMediaSessionMetadata()
        }
        val builder: NotificationCompat.Builder? = MediaStyleHelper.from(this, mediaSession)
                ?: return
        builder?.addAction(NotificationCompat.Action(R.drawable.ic_previous_black, "Previous", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)))
        builder!!.addAction(NotificationCompat.Action(R.drawable.ic_pause_black, "Pause", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PAUSE)))
        builder.addAction(NotificationCompat.Action(R.drawable.ic_next_black, "Next", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_NEXT)))
        builder.setStyle(MediaStyle().setShowActionsInCompactView(0, 1, 2).setMediaSession(mediaSession?.sessionToken))

        runOnUiThread {
            GlideApp.with(this@PlayerService)
                    .asBitmap()
                    .load(currentRadioStation?.logo)
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            builder.setLargeIcon(resource)
                            builder.setSmallIcon(R.mipmap.ic_launcher)
                            startForeground(NOTIFICATION_ID, builder.build())

                        }
                    })

        }
    }

    private fun showPausedNotification(allowMediaSession: Boolean) {
        val builder: NotificationCompat.Builder? = MediaStyleHelper.from(this, mediaSession)
                ?: return
        builder?.addAction(NotificationCompat.Action(R.drawable.ic_previous_black, "Previous", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)))
        builder!!.addAction(NotificationCompat.Action(R.drawable.ic_play_black, "Play", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY)))
        builder.addAction(NotificationCompat.Action(R.drawable.ic_next_black, "Next", MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_NEXT)))
        builder.setStyle(MediaStyle().setShowActionsInCompactView(0, 1, 2).setMediaSession(mediaSession?.sessionToken))
        runOnUiThread {
            GlideApp.with(this@PlayerService)
                    .asBitmap()
                    .load(currentRadioStation?.logo)
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            builder.setLargeIcon(resource)
                            builder.setSmallIcon(R.mipmap.ic_launcher)
                            val notification = builder.build()
                            stopForeground(true)
                            if (allowMediaSession) {
                                startForeground(NOTIFICATION_ID, notification)
                            } else {
                                NotificationManagerCompat.from(this@PlayerService).notify(NOTIFICATION_ID, notification)
                            }
                        }
                    })

        }

    }

    override fun onLoadChildren(parentId: String, result: Result<MutableList<MediaBrowserCompat.MediaItem>>) {
        result.sendResult(ArrayList())
    }

    override fun onGetRoot(clientPackageName: String, clientUid: Int, rootHints: Bundle?): BrowserRoot? {
        return BrowserRoot(getString(R.string.app_name), null)
    }

    private fun initMediaSession() {
        val mediaButtonReceiver = ComponentName(applicationContext, MediaButtonReceiver::class.java)
        mediaSession = MediaSessionCompat(applicationContext, "Tag", mediaButtonReceiver, null)
        mediaSession?.setCallback(mediaSessionCallback)
        mediaSession?.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)
        val mediaButtonIntent = Intent(Intent.ACTION_MEDIA_BUTTON)
        mediaButtonIntent.setClass(this, MediaButtonReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0)
        mediaSession?.setMediaButtonReceiver(pendingIntent)
        sessionToken = mediaSession?.sessionToken
    }

    private fun setMediaPlaybackState(state: Int) {
        val playbackStateBuilder = PlaybackStateCompat.Builder()
        if (state == PlaybackStateCompat.STATE_PLAYING) {
            playbackStateBuilder.setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE or PlaybackStateCompat.ACTION_PAUSE)
        } else {
            playbackStateBuilder.setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE or PlaybackStateCompat.ACTION_PLAY)
        }
        playbackStateBuilder.setState(state, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 0f)
        mediaSession?.setPlaybackState(playbackStateBuilder.build())
    }

    private fun initMediaSessionMetadata() {
        val metadataBuilder = MediaMetadataCompat.Builder()

        try {
            val bitmap = GlideApp.with(this@PlayerService)
                    .asBitmap()
                    .load(currentRadioStation?.logo)
                    .submit()
                    .get()

            metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, bitmap)
        } catch (e : Exception) {
            //do nothing...
        }

        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, currentRadioStation?.name)
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, currentSongName)
        mediaSession?.setMetadata(metadataBuilder.build())
    }

    private fun initNoisyReceiver() {
        var filter = IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)
        registerReceiver(mNoisyReceiver, filter)
    }

    private fun successfullyRetrievedAudioFocus(): Boolean {
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        var result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
        return result == AudioManager.AUDIOFOCUS_GAIN

    }

    override fun onAudioFocusChange(p0: Int) {
        when (p0) {
            AudioManager.AUDIOFOCUS_LOSS -> {
                if (isPlaying) {
                    actionsInteractor.sendPlayPause(Pause(currentRadioStation))
                    isWasPlaying = true
                }
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                processAction(ACTION_PAUSE)
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                player.volume = 0.3f
            }
            AudioManager.AUDIOFOCUS_GAIN -> {
                if (isWasPlaying) {
                    isWasPlaying = false
                    processAction(ACTION_PLAY)
                }
                player.volume = 1f
            }
        }
    }

    companion object {
        var isServiceRunning = false
        fun newIntent(context: Context) = Intent(context, PlayerService::class.java)
        fun newCloseIntent(context: Context) = Intent(context, PlayerService::class.java).putExtra(BUNDLE_ACTION, ACTION_CLOSE)

        const val EQUALIZER_PRIORITY = 1000
    }
}
