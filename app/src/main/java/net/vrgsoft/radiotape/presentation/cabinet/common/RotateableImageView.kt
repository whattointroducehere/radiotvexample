package net.vrgsoft.radiotape.presentation.cabinet.common

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.animation.LinearInterpolator


class RotateableImageView : AppCompatImageView{
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var mObjectAnimator: ObjectAnimator? = null
    private var mAnimationTime: Long = 0

    fun stopAnimation() {
        if (mObjectAnimator != null) {
            mAnimationTime = mObjectAnimator?.currentPlayTime ?: 0
            mObjectAnimator?.cancel()
        }
    }

    fun playAnimation() {
        if(mObjectAnimator == null){
            mObjectAnimator = ObjectAnimator.ofFloat(this, "rotation", 360f)
            mObjectAnimator?.repeatCount = ValueAnimator.INFINITE
            mObjectAnimator?.duration = 2000
            mObjectAnimator?.interpolator = LinearInterpolator()
        }
        mObjectAnimator?.start()
        mObjectAnimator?.currentPlayTime = mAnimationTime
    }


}