package net.vrgsoft.radiotape.presentation.cabinet.added_filters

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Gravity
import android.view.View
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.databinding.FragmentAddedFiltersBinding
import net.vrgsoft.radiotape.presentation.cabinet.added_filters.common.FiltrableAdapter
import net.vrgsoft.radiotape.presentation.cabinet.filters.FiltersFragment
import net.vrgsoft.radiotape.presentation.common.AddedFiltersFactory
import net.vrgsoft.radiotape.presentation.common.BaseFragment
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class AddedFiltersFragment : BaseFragment<FragmentAddedFiltersBinding>() {
    override fun diModule() = Kodein.Module("AddedFiltersFragmentModule") {
        bind<AddedFiltersFactory>() with singleton { AddedFiltersFactory(instance(), instance(), instance()) }
        bind<AddedFiltersContract.ViewModel>() with singleton { vm<AddedFiltersViewModel>(instance<AddedFiltersFactory>()) }
        bind<FiltrableAdapter>() with provider { FiltrableAdapter(layoutInflater) }
    }

    private val vm by instance<AddedFiltersContract.ViewModel>()
    private val countryAdapter by instance<FiltrableAdapter>()
    private val genreAdapter by instance<FiltrableAdapter>()

    private var filtersFragment: FiltersFragment? = null

    @SuppressLint("RestrictedApi")
    override fun viewCreated() {
        val toolbarEnabled = isToolbarEnabled()
        binding.apply {
            vm = this@AddedFiltersFragment.vm

            checkedCountriesRecycler.layoutManager = ChipsLayoutManager.newBuilder(context)
                    .setChildGravity(Gravity.LEFT)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build()

            checkedGenresRecycler.layoutManager = ChipsLayoutManager.newBuilder(context)
                    .setChildGravity(Gravity.LEFT)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build()
            checkedCountriesRecycler.adapter = countryAdapter
            checkedGenresRecycler.adapter = genreAdapter
            checkedGenresRecycler.addItemDecoration(SpacingItemDecoration(resources.getDimensionPixelOffset(R.dimen.chip_spacing),
                    resources.getDimensionPixelOffset(R.dimen.chip_spacing)))
            checkedCountriesRecycler.addItemDecoration(SpacingItemDecoration(resources.getDimensionPixelOffset(R.dimen.chip_spacing),
                    resources.getDimensionPixelOffset(R.dimen.chip_spacing)))

            toolbar.visibility = if (toolbarEnabled == true) View.VISIBLE else View.GONE
        }
        vm.bound()
        launch(UI, parent = jobs) { updateCountryList() }
        launch(UI, parent = jobs) { updateGenreList() }
        launch(UI, parent = jobs) { observeToShowFiltersDialogEvents() }
        launch(UI, parent = jobs) { observeToDeleteCountryEvents() }
        launch(UI, parent = jobs) { observeToDeleteGenreEvents() }
        launch(UI, parent = jobs) { observeCloseEvent() }
    }

    override fun onResume() {
        super.onResume()
        vm.loadData()
    }

    private fun isToolbarEnabled() = arguments?.getBoolean(ARG_TOOLBAR_ENABLED, false)!!
    private suspend fun observeToDeleteGenreEvents() {
        countryAdapter.onItemDeletedEvents.consumeEach { vm.onItemDeleted(it) }
    }

    private suspend fun observeToDeleteCountryEvents() {
        genreAdapter.onItemDeletedEvents.consumeEach { vm.onItemDeleted(it) }
    }

    private suspend fun observeToShowFiltersDialogEvents() {
        vm.showFiltersDialog.consumeEach {
            filtersFragment?.dismiss()

            val fragment = FiltersFragment.newInstance()
            fragment.show(childFragmentManager, "FiltersFragment")
        }
    }

    private suspend fun updateCountryList() {
        vm.countryChannel.consumeEach { countryAdapter.setData(it) }
    }

    private suspend fun updateGenreList() {
        vm.genresChannel.consumeEach { genreAdapter.setData(it) }
    }

    private suspend fun observeCloseEvent() {
        vm.goBackChannel.consumeEach { activity?.onBackPressed() }
    }

    fun hasCheckedItems(): Boolean {
        return vm.hasCheckedItems()
    }

    companion object {
        @JvmStatic
        fun newInstance(toolbarEnabled: Boolean = false): AddedFiltersFragment {
            val equalizerFragment = AddedFiltersFragment()
            val bundle = Bundle()
            bundle.putBoolean(ARG_TOOLBAR_ENABLED, toolbarEnabled)
            equalizerFragment.arguments = bundle
            return equalizerFragment
        }

        const val ARG_TOOLBAR_ENABLED = "ARG_TOOLBAR_ENABLED"
    }
}
