package net.vrgsoft.radiotape.presentation.start

import net.vrgsoft.radiotape.presentation.common.BaseVM

class StartContract {
    interface ViewModel : BaseVM {

        fun onNextClicked()

        fun onFiltersSaved()
    }

    interface Router {
        fun openMainScreen()

    }
}
