package net.vrgsoft.radiotape.presentation.common.view

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import net.vrgsoft.radiotape.R


class BubbleButton : View {
    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initAttrs(attrs)
    }

    private val paint = Paint()
    private val path = Path()
    private lateinit var points: FloatArray
    private var defaultX: Float = 0f
    private var defaultY: Float = 0f
    private var touchMode = false
    private var startColor: Int = 0
    private var endColor: Int = 0
    private var defaultCenterX = 0f
    private var defaultCenterY = 0f
    private var circleCenterX = 0f
    private var circleCenterY = 0f
    private var circleRadius = 0f
    private var iconHalfLength = 0f
    private val iconRect = RectF()
    private val stretchDecelerationRatio = 0.2f
    private val swipeThreshold = 100 * Resources.getSystem().displayMetrics.density
    private val screenAspectRatio = Resources.getSystem().displayMetrics.widthPixels * 1.0f / Resources.getSystem().displayMetrics.heightPixels

    private var swipe: (() -> Unit)? = null

    private val shader by lazy {
        LinearGradient(
                width.toFloat() / 2,
                0f,
                width.toFloat() / 2,
                height.toFloat(),
                startColor,
                endColor,
                Shader.TileMode.CLAMP
        )
    }
    private var icon: Bitmap? = null

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        icon?.recycle()
    }

    private fun initAttrs(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.BubbleButton)
        startColor = a.getColor(R.styleable.BubbleButton_startColor, startColor)
        endColor = a.getColor(R.styleable.BubbleButton_endColor, endColor)
        val drawable = a.getDrawable(R.styleable.BubbleButton_btImage)
        icon = getBitmapFromVectorDrawable(drawable)

        a.recycle()
    }

    override fun onSizeChanged(width: Int, height: Int, oldw: Int, oldh: Int) {
        val density = Resources.getSystem().displayMetrics.density

        points = floatArrayOf(
                width.toFloat(), 0f,
                width - 8 * density, 44 * density,
                width - 37 * density, 57 * density,
                width - 37 * density, 77 * density,
                width - 37 * density, 98 * density,
                width - 15 * density, 109 * density,
                width.toFloat(), 165 * density
        )

        defaultX = width - 37 * density
        defaultY = 77 * density

        defaultCenterX = width - 17 * density
        defaultCenterY = defaultY
        circleCenterX = defaultCenterX
        circleCenterY = defaultCenterY
        circleRadius = 17 * density

        iconHalfLength = 12 * density
        setIconPosition()
    }

    override fun onDraw(canvas: Canvas) {
        path.reset()
        path.moveTo(points[0], points[1])
        path.cubicTo(
                points[2],
                points[3],
                points[4],
                points[5],
                points[6],
                points[7]
        )
        path.cubicTo(
                points[8],
                points[9],
                points[10],
                points[11],
                points[12],
                points[13]
        )
        path.close()

        paint.style = Paint.Style.FILL
        paint.shader = shader
        canvas.drawPath(path, paint)
        canvas.drawCircle(circleCenterX, circleCenterY, circleRadius, paint)
        canvas.drawBitmap(icon, null, iconRect, null)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (swipe == null) {
            return super.onTouchEvent(event)
        } else {
            return when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    val rect = RectF(defaultX, 0f, width.toFloat(), height.toFloat())
                    if (rect.contains(event.x, event.y)) {
                        touchMode = true
                    }
                    touchMode
                }
                MotionEvent.ACTION_MOVE -> {
                    if (touchMode) {
                        val halfHeight = height / 2
                        val decelerationAmountX = width * (stretchDecelerationRatio - (event.x / width) * stretchDecelerationRatio)
                        val decelerationAmountY = height * ((event.y - halfHeight) / halfHeight) * stretchDecelerationRatio * screenAspectRatio
                        points[6] = if (event.x + decelerationAmountX > defaultX) defaultX else event.x + decelerationAmountX
                        points[7] = event.y - decelerationAmountY
                        circleCenterX = if (event.x > defaultCenterX) defaultCenterX else event.x
                        circleCenterY = event.y
                        setIconPosition()
                    }
                    invalidate()
                    touchMode
                }
                MotionEvent.ACTION_UP -> {
                    touchMode = false
                    points[6] = defaultX
                    points[7] = defaultY
                    circleCenterX = defaultCenterX
                    circleCenterY = defaultCenterY
                    if (defaultX - event.x > swipeThreshold) {
                        swipe?.invoke()
                    }
                    setIconPosition()
                    invalidate()
                    true
                }
                else -> super.onTouchEvent(event)
            }
        }
    }

    private fun setIconPosition() {
        iconRect.left = circleCenterX - iconHalfLength
        iconRect.top = circleCenterY - iconHalfLength
        iconRect.right = circleCenterX + iconHalfLength
        iconRect.bottom = circleCenterY + iconHalfLength
    }

    private fun getBitmapFromVectorDrawable(drawable: Drawable): Bitmap {
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth,
                drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    fun setSwipeListener(swipe: (() -> Unit)?) {
        this.swipe = swipe
    }
}
