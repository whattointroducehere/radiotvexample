package net.vrgsoft.radiotape.presentation.cabinet

import kotlinx.coroutines.experimental.channels.Channel
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.pipeline.player_actions.PlayPauseAction
import net.vrgsoft.radiotape.data.pipeline.player_actions.Quality
import net.vrgsoft.radiotape.data.pipeline.record.RecordState

interface MainContract {
    interface ViewModel {
        fun onRecordClick()
        fun onQualityClick()
        fun onNextClick()
        fun onPlayClick()
        fun onPreviousClick()
        fun onAddToFavoriteClick()
        fun onSnoozeClick()
        fun onQualityChosen(quality: Quality)
        fun onFavoritesClick()
        fun onEqualizerClick()

        fun onFiltersClicked()
        fun stopPlayback()

        fun getPlayPause(): Channel<PlayPauseAction>
        fun getQualityAnimation(): Channel<Boolean>
        fun getStationChange(): Channel<RadioStationDB>
        fun getSnoozeDialog(): Channel<Boolean>
        fun getQualityChange(): Channel<Quality>
        fun getRecordState(): Channel<RecordState>

        fun setTimer(progress: Int)
        fun getRequestPermission(): Channel<Array<String>>
        fun startRecording()
        fun getLastTimerTime(): Long
    }

    interface Router {
        fun stopRecording()
        fun startRecording(stream: String?)
        fun goToFavorites()
        fun goToEqualizerScreen()
        fun goToFiltersScreen()
        fun navigateToPaywall()
    }
}
