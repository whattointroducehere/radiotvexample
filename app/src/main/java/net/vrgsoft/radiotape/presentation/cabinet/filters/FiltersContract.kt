package net.vrgsoft.radiotape.presentation.cabinet.filters

import android.databinding.ObservableBoolean
import kotlinx.coroutines.experimental.channels.BroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedChannel
import net.vrgsoft.radiotape.data.model.Country
import net.vrgsoft.radiotape.data.model.Genre
import net.vrgsoft.radiotape.presentation.common.BaseVM

interface FiltersContract {

    interface ViewModel : BaseVM {
        val countryChannel: ConflatedChannel<List<Country>>

        val genresChannel: ConflatedChannel<List<Genre>>
        val expandGenreList: BroadcastChannel<Boolean>

        val expandCountryList: BroadcastChannel<Boolean>

        var closeDialogChannel: ConflatedBroadcastChannel<Any?>

        fun expandGenreList(isExpanded: Boolean)

        fun expandCountryList(isExpanded: Boolean)

        val progressVisibility: ObservableBoolean

        fun onFiltersFragmentClose()

        fun saveFilters()

        fun save()

        fun hasCheckedItems(): Boolean
        fun onCloseDialog()
    }

    interface Router {
        fun onFiltersFragmentClose()
    }
}
