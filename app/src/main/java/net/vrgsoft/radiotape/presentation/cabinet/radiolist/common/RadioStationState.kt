package net.vrgsoft.radiotape.presentation.cabinet.radiolist.common

import net.vrgsoft.radiotape.data.db.model.RadioStationDB

class RadioStationState(var radioStation: RadioStationDB, var isPlaying: Boolean) {
    private val id = radioStation.id

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RadioStationState

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
