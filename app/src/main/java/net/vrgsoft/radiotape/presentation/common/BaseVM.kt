package net.vrgsoft.radiotape.presentation.common

interface BaseVM {
    fun bound()

    fun unbound()
}