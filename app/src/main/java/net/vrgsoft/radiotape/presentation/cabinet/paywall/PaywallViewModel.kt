package net.vrgsoft.radiotape.presentation.cabinet.paywall

import android.content.res.Resources
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.anjlab.android.iab.v3.SkuDetails
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.presentation.common.BaseViewModel

class PaywallViewModel(
        private val router: PaywallContract.Router,
        private val resources: Resources,
        private val billingManager: BillingManager) : BaseViewModel(), PaywallContract.ViewModel {

    override val isSubscriptionAvailable: ObservableBoolean
        get() = subscriptionAvailable
    override val monthlySubscription: ObservableField<String>
        get() = monthly
    override val annualSubscription: ObservableField<String>
        get() = annual

    private val subscriptionAvailable = ObservableBoolean(false)
    private val monthly = ObservableField<String>()
    private val annual = ObservableField<String>()

    private val compositeDisposable = CompositeDisposable()

    override fun bound() {
        compositeDisposable.addAll(
                billingManager.onSubscriptionPurchased
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            if (it) {
                                router.closePayWallFragment()
                            }
                        },

                billingManager.skuDetails
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { stringSkuDetailsMap ->
                            var details: SkuDetails? = stringSkuDetailsMap[resources.getString(R.string.sku_inapp_month)]
                            if (details != null) {
                                monthlySubscription.set(
                                        resources.getString(R.string.save_space_month, details.priceValue, details.currency))
                            }
                            details = stringSkuDetailsMap[resources.getString(R.string.sku_inapp_year)]
                            if (details != null) {
                                annualSubscription.set(
                                        resources.getString(R.string.save_space_year, details.priceValue, details.currency))
                            }
                            subscriptionAvailable.set(true)
                        }
        )
    }

    override fun unbound() {
        super.unbound()
        compositeDisposable.dispose()
    }

    override fun onWallClicked() {
        router.closePayWallFragment()
    }

    override fun startMonthlySubscription() {
        billingManager.startMonthlySubscription()
    }

    override fun startAnnualSubscription() {
        billingManager.startAnnualSubscription()
    }
}
