package net.vrgsoft.radiotape.presentation.cabinet

import android.text.TextUtils
import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import net.vrgsoft.radiotape.data.db.model.RadioStationDB
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB
import net.vrgsoft.radiotape.data.job.TimerJob
import net.vrgsoft.radiotape.data.pipeline.player_actions.*
import net.vrgsoft.radiotape.data.pipeline.record.RecordFinished
import net.vrgsoft.radiotape.data.pipeline.record.RecordStarted
import net.vrgsoft.radiotape.data.pipeline.record.RecordState
import net.vrgsoft.radiotape.domain.filters.FiltersInteractor
import net.vrgsoft.radiotape.domain.player_action.PlayerActionsInteractor
import net.vrgsoft.radiotape.domain.record.RecordInteractor
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.domain.stations.StationsInteractor
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.RadioListContract
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.common.RadioStationState
import net.vrgsoft.radiotape.presentation.common.BaseViewModel
import net.vrgsoft.radiotape.presentation.cabinet.paywall.BillingManager
import net.vrgsoft.radiotape.utils.logDebug
import net.vrgsoft.radiotape.utils.subscribe

class MainViewModel(
        private val router: MainContract.Router,
        private val billingManager: BillingManager,
        private val playerActionsInteractor: PlayerActionsInteractor,
        private val stationsInteractor: StationsInteractor,
        private val settingsInteractor: SettingsInteractor,
        private val recordInteractor: RecordInteractor,
        private val filtersChangeEvent: ConflatedBroadcastChannel<Boolean>,
        private val filtersInteractor: FiltersInteractor
) : BaseViewModel(), MainContract.ViewModel {

    private val playPauseActions = Channel<PlayPauseAction>(Channel.CONFLATED)
    private val qualityAnimation = Channel<Boolean>(Channel.CONFLATED)
    private val recordState = Channel<RecordState>(Channel.CONFLATED)
    private val stationChange = Channel<RadioStationDB>()
    private val qualityChange = Channel<Quality>()
    private val snoozeDialog = Channel<Boolean>()
    private val requestPermission = Channel<Array<String>>()

    override fun getPlayPause() = playPauseActions
    override fun getQualityAnimation() = qualityAnimation
    override fun getStationChange() = stationChange
    override fun getQualityChange() = qualityChange
    override fun getSnoozeDialog() = snoozeDialog
    override fun getRecordState() = recordState
    override fun getRequestPermission() = requestPermission

    private var listDb: List<RadioStationDB>? = null
    private var filtersDB: SettingFiltersDB? = null

    private var isPlaying = false
    private var isRecording = false
    private var isQualityShown = false
    private var currentStation: RadioStationDB? = null
    private var stationsList: List<RadioStationDB>? = null
    private val updateStation = Channel<Pair<RadioListContract.UpdateEvent, Pair<RadioStationState, Int>>>()

    init {
        launch(parent = jobs) {
            currentStation = stationsInteractor.getLastPlayedStation()
            getStations()
            if (currentStation != null) {
                stationChange.send(currentStation!!)
            }
            var quality = settingsInteractor.getQuality()
            if(quality == Quality.HIGH && !billingManager.isProductPurchased){//for old preferences
                quality = Quality.MEDIUM
                chooseQuality(quality)
            }
            val channel = playerActionsInteractor.subscribeToPlayPause().openSubscription()
            channel.subscribe {
                when (it) {
                    is Play -> {
                        currentStation = it.station
                        if (currentStation != null) {
                            stationChange.send(currentStation!!)
                        }
                        isPlaying = true
                    }
                    is Pause -> {
                        isPlaying = false
                    }
                }
                playPauseActions.send(it)
            }
        }

        launch(parent = jobs) {
            stationsInteractor.observeSearchChanges {
                stationsList = proccessData(it)
            }
        }

        launch(parent = jobs) {
            filtersChangeEvent.consumeEach { getStations() }
        }
        launch(parent = jobs) {
            val channel = playerActionsInteractor.subscribeToFavoriteChange().openSubscription()
            channel.subscribe {
                val indexOfElement = stationsList?.indexOf(it) ?: -1
                if (indexOfElement >= 0) {
                    stationsList!![indexOfElement].isFavorite = it.isFavorite
                }

                if (currentStation?.id == it.id) {
                    currentStation = it
                    stationChange.send(it)
                }
            }
        }

        launch(parent = jobs) {
            recordInteractor.subscribeToRecordState().consumeEach {
                logDebug("RecorderService", it.toString())
                when (it) {
                    is RecordStarted -> isRecording = true
                    is RecordFinished -> isRecording = false
                }
                recordState.send(it)
            }
        }
        launch(parent = jobs)
        {
            val channel = playerActionsInteractor.subscribeToNextPrevious().openSubscription()
            channel.subscribe {
                if (it is Next) {
                    onNextClick()
                } else {
                    onPreviousClick()
                }
            }
        }
    }

    private fun getStations() {
        launch(parent = jobs) {
            currentStation = stationsInteractor.getLastPlayedStation()
            stationsList = async {
                listDb = withContext(DefaultDispatcher) { stationsInteractor.getAllStations() }
                filtersDB = withContext(DefaultDispatcher) { filtersInteractor.loadFilters() }

                return@async proccessData()
            }.await()
        }
    }

    private fun proccessData(searchQuery: String? = null): List<RadioStationDB> {
        if (listDb == null || filtersDB == null) {
            return listOf()
        }

        // search
        val useSearch = !TextUtils.isEmpty(searchQuery)
        val data = if (useSearch) {
            listDb!!.filter { it.name.toLowerCase().contains(searchQuery!!.toLowerCase()) }
        } else {
            listDb!!
        }

        // filters
        return if (!useSearch) {
            data.filter { it.filter(filtersDB!!) }
        } else {
            data
        }
    }

    override fun bound() {
    }

    override fun unbound() {
    }

    override fun onRecordClick() {
        launch(parent = jobs) {
            requestPermission.send(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE))
        }
    }

    override fun onQualityClick() {
        launch(parent = jobs) {
            isQualityShown = !isQualityShown
            qualityAnimation.send(isQualityShown)
        }
    }

    override fun onNextClick() {
        launch(parent = jobs) {
            if (currentStation != null && stationsList != null) {
                val nextIndex = stationsList!!.indexOf(currentStation!!) + 1
                if (nextIndex < stationsList!!.size) {
                    currentStation = stationsList!![nextIndex]
                    stationChange.send(currentStation!!)
                    playStation(RadioStationState(currentStation!!, false), nextIndex)
                }
            }
        }
    }

    override fun onPreviousClick() {
        launch(parent = jobs) {
            if (currentStation != null && stationsList != null) {
                val previousIndex = stationsList!!.indexOf(currentStation!!) - 1
                if (previousIndex >= 0) {
                    currentStation = stationsList!![previousIndex]
                    stationChange.send(currentStation!!)
                    playStation(RadioStationState(currentStation!!, false), previousIndex)
                }

            }
        }
    }

    override fun onFavoritesClick() {
        router.goToFavorites()
    }

    override fun onPlayClick() {
        if (currentStation == null) return
        isPlaying = !isPlaying
        playerActionsInteractor.sendPlayPause(if (isPlaying) Play(currentStation) else Pause(currentStation))
    }

    override fun onAddToFavoriteClick() {
        launch(parent = jobs) {
            if (currentStation != null && stationsList != null) {
                currentStation!!.isFavorite = !currentStation!!.isFavorite
                val indexOfItem = stationsList!!.indexOf(currentStation!!)

                if (indexOfItem >= 0) {
                    stationsList!![indexOfItem].isFavorite = currentStation!!.isFavorite
                }

                stationsInteractor.updateStation(currentStation!!)
                playerActionsInteractor.sendFavoriteChange(currentStation!!)
            }
        }
    }

    override fun onSnoozeClick() {
        launch(parent = jobs) { snoozeDialog.send(true) }
    }

    override fun onQualityChosen(quality: Quality) {
        onQualityClick()
        if (quality == Quality.HIGH && !billingManager.isProductPurchased) {
            router.navigateToPaywall()
        } else {
            chooseQuality(quality)
        }
    }

    private fun chooseQuality(quality: Quality){
        launch(parent = jobs) {
            qualityChange.send(quality)
            playerActionsInteractor.sendQualityChange(ChangeQuality(quality))
        }
    }

    override fun onFiltersClicked() {
        router.goToFiltersScreen()
    }

    override fun setTimer(progress: Int) {
        val time = progress * 60L * 1000L + System.currentTimeMillis()

        if (progress > 0) {
            TimerJob.scheduleTimer(progress)
        } else {
            TimerJob.cancelTimer()
        }

        settingsInteractor.setLastTimer(time)
    }

    override fun getLastTimerTime(): Long = settingsInteractor.getLastTimerTime()

    override fun startRecording() {
        if (!isRecording) router.startRecording(currentStation?.stream)
        else router.stopRecording()
    }

    override fun onEqualizerClick() {
        router.goToEqualizerScreen()
    }

    override fun stopPlayback() {
        if (currentStation == null) return
        playerActionsInteractor.sendPlayPause(Pause(currentStation))
    }

    private fun playStation(station: RadioStationState, position: Int) {
        station.isPlaying = true
        launch(parent = jobs) {
            updateStation.send(RadioListContract.UpdateEvent.PLAYBACK to (station to position))
        }
        playerActionsInteractor.sendPlayPause(Play(station.radioStation))
    }

}
