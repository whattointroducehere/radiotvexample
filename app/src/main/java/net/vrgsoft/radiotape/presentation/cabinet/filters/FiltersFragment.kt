package net.vrgsoft.radiotape.presentation.cabinet.filters

import android.annotation.SuppressLint
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.launch
import net.cachapa.expandablelayout.ExpandableLayout
import net.vrgsoft.radiotape.R
import net.vrgsoft.radiotape.databinding.FragmentFiltersBinding
import net.vrgsoft.radiotape.presentation.cabinet.filters.common.CountryFilterAdapter
import net.vrgsoft.radiotape.presentation.cabinet.filters.common.GenreAdapter
import net.vrgsoft.radiotape.presentation.common.BaseDialogFragment
import net.vrgsoft.radiotape.presentation.common.FiltersFactory
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class FiltersFragment : BaseDialogFragment<FragmentFiltersBinding>() {
    override fun diModule() = Kodein.Module("FiltersFragmentModule") {
        var saveFilterChannel = ConflatedBroadcastChannel<Boolean>()
        bind<FiltersFactory>() with singleton { FiltersFactory(instance(), instance(), instance(), instance(), instance(), saveFilterChannel) }
        bind<FiltersContract.ViewModel>() with singleton { vm<FiltersViewModel>(instance<FiltersFactory>()) }
        bind<CountryFilterAdapter>() with provider { CountryFilterAdapter(instance(), saveFilterChannel) }
        bind<GenreAdapter>() with provider { GenreAdapter(instance(), saveFilterChannel) }
    }

    private val vm: FiltersContract.ViewModel by instance()
    private val mCountriesAdapter: CountryFilterAdapter by instance()
    private val genresAdapter: GenreAdapter by instance()
    private var scrollY = -1

    @SuppressLint("RestrictedApi")
    override fun viewCreated() {
        binding.vm = vm

        binding.apply {
            genreFilterList.layoutManager = LinearLayoutManager(context)
            genreFilterList.isNestedScrollingEnabled = false
            genreFilterList.adapter = genresAdapter

            countryFilterList.layoutManager = LinearLayoutManager(context)
            countryFilterList.isNestedScrollingEnabled = false
            countryFilterList.adapter = mCountriesAdapter
        }

        vm.bound()

        launch(UI, parent = jobs) { updateCountryList() }
        launch(UI, parent = jobs) { updateGenreList() }
        launch(UI, parent = jobs) { observeToggleCountryList() }
        launch(UI, parent = jobs) { observeToggleGenreList() }
        launch(UI, parent = jobs) { observeCloseDialog() }
        launch(UI, parent = jobs) { observeStartExpandEvents() }
        launch(CommonPool, parent = jobs) { observeEndExpandEvents() }
    }

    private suspend fun observeStartExpandEvents() {
//        mCountriesAdapter.startExpand.consumeEach {
//            scrollY = binding.contentView.scrollY
//
//            delay(50)
//            withContext(UI) {
//                binding.contentView.scrollTo(0, scrollY)
//            }
//        }
    }

    private suspend fun observeEndExpandEvents() {
//        mCountriesAdapter.endExpand.consumeEach {
//            if (scrollY >= 0 && binding.contentView.scrollY != scrollY) {
//                withContext(UI) {
//                    binding.contentView.scrollTo(0, scrollY)
//                }
//            }
//        }
    }

    override fun onResume() {
        super.onResume()
        val params = dialog?.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = params as android.view.WindowManager.LayoutParams
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }

    private suspend fun updateCountryList() {
        val list = vm.countryChannel.receive()
        binding.countryFilters.visibility = View.VISIBLE
        mCountriesAdapter.setData(list)
    }

    private suspend fun updateGenreList() {
        val list = vm.genresChannel.receive()
        binding.genreFilters.visibility = View.VISIBLE
        genresAdapter.setData(list)
    }

    private suspend fun observeToggleGenreList() {
        vm.expandGenreList.consumeEach {
            expand(it, binding.expandableGenresLayout, binding.genreArrow)
        }
    }

    private suspend fun observeCloseDialog() {
        vm.closeDialogChannel.consumeEach {
            dismiss()
        }
    }

    private suspend fun observeToggleCountryList() {
        vm.expandCountryList.consumeEach {
            expand(it, binding.expandableCountriesLayout, binding.countryArrow)
        }
    }

    private fun expand(isExpanded: Boolean, view: ExpandableLayout, arrow: ImageView) {
        if (isExpanded) {
            arrow.setImageResource(R.drawable.ic_arrow_up)
        } else {
            arrow.setImageResource(R.drawable.ic_arrow_down)
        }

        view.toggle()
    }

    override fun onDestroyView() {
        vm.unbound()
        mCountriesAdapter.cancelAllJobs()
        genresAdapter.cancelAllJobs()
        super.onDestroyView()
    }

    companion object {
        @JvmStatic
        fun newInstance(): FiltersFragment {
            return FiltersFragment()
        }
    }
}
