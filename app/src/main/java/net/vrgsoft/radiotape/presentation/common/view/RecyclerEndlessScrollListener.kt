package net.vrgsoft.radiotape.presentation.common.view

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import net.vrgsoft.radiotape.DEFAULT_PAGE_SIZE

abstract class RecyclerEndlessScrollListener(
        private val layoutManager: LinearLayoutManager
) : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= DEFAULT_PAGE_SIZE) {
            loadMoreItems()
        }
    }

    abstract fun loadMoreItems()
}