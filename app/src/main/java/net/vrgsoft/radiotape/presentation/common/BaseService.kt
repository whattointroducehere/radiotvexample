package net.vrgsoft.radiotape.presentation.common

import android.app.Service
import kotlinx.coroutines.experimental.Job
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.android.closestKodein

abstract class BaseService : Service(), KodeinAware {
    private val _parentKodein by closestKodein()

    override val kodein: Kodein = Kodein.lazy {
        extend(_parentKodein)
        import(diModule())
    }

    override val kodeinTrigger = KodeinTrigger()
    protected val jobs =  Job()

    override fun onCreate() {
        kodeinTrigger.trigger()
        super.onCreate()
    }

    abstract fun diModule(): Kodein.Module

    override fun onDestroy() {
        jobs.cancel()
        super.onDestroy()
    }
}