package net.vrgsoft.radiotape.presentation.cabinet.radiolist

import android.databinding.ObservableBoolean
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ConflatedChannel
import net.vrgsoft.radiotape.presentation.cabinet.radiolist.common.RadioStationState
import net.vrgsoft.radiotape.presentation.common.BaseVM

interface RadioListContract {
    interface ViewModel:BaseVM{
        fun onAddFavoriteClick(station: RadioStationState, position: Int)
        fun playStation(station: RadioStationState, position: Int)
        fun openStation(station: RadioStationState, position: Int)
        fun updateStation(): Channel<Pair<UpdateEvent, Pair<RadioStationState, Int>>>
        fun pauseStation(station: RadioStationState, position: Int)
        fun getStations()

        fun search(newText: String?)

        val radioList: ConflatedChannel<List<RadioStationState>>
        val hideKeyboard: ConflatedChannel<Boolean>

        val onItemsNotFoundEvent: ObservableBoolean
    }

    interface Router {
        fun navigateToRadioPlayer()
    }

    enum class UpdateEvent{ FAVORITE, PLAYBACK }
}
