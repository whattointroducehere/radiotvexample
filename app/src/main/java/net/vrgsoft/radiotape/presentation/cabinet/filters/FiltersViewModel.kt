package net.vrgsoft.radiotape.presentation.cabinet.filters

import android.databinding.ObservableBoolean
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.channels.ConflatedChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import net.vrgsoft.radiotape.data.db.model.SettingFiltersDB
import net.vrgsoft.radiotape.data.model.*
import net.vrgsoft.radiotape.data.repository.resources.ResourcesStorage
import net.vrgsoft.radiotape.domain.filters.FiltersInteractor
import net.vrgsoft.radiotape.domain.genre.GenreInteractor
import net.vrgsoft.radiotape.domain.location.LocationInteractor
import net.vrgsoft.radiotape.presentation.common.BaseViewModel

class FiltersViewModel(private val locationInteractor: LocationInteractor,
                       private val genreInteractor: GenreInteractor,
                       private val filtersInteractor: FiltersInteractor,
                       private val router: FiltersContract.Router,
                       private val resourcesStorage: ResourcesStorage,
                       private val saveFilterChannel: ConflatedBroadcastChannel<Boolean>
) : BaseViewModel(), FiltersContract.ViewModel {


    override val countryChannel = ConflatedChannel<List<Country>>()
    override val genresChannel = ConflatedChannel<List<Genre>>()


    override val expandGenreList = ConflatedBroadcastChannel<Boolean>()
    override val expandCountryList = ConflatedBroadcastChannel<Boolean>()
    override var closeDialogChannel = ConflatedBroadcastChannel<Any?>()

    override val progressVisibility = ObservableBoolean()

    private lateinit var countries: List<Country>
    private lateinit var genres: List<Genre>
    private var saveFilterJob: Job? = null
    override fun bound() {
        launch(parent = jobs) {
            progressVisibility.set(true)
            val countriesJob = async(parent = jobs) { locationInteractor.getAllCountries() }
            val genresJob = async(parent = jobs) { genreInteractor.getAllGenres() }

            countries = addHeaders(countriesJob.await())
            genres = addHeadersToGenres(genresJob.await())

            val filtersDB = withContext(DefaultDispatcher) { filtersInteractor.loadFilters() }
            applyFilters(filtersDB, countries, genres)

            countryChannel.send(countries)
            genresChannel.send(genres)

            progressVisibility.set(false)
        }
        launch(parent = jobs) {
            saveFilterChannel.consumeEach {
                delay(100)
                saveFilters()
            }
        }
    }

    private fun applyFilters(filters: SettingFiltersDB, countries: List<Country>, genres: List<Genre>) {
        SettingFiltersDB.applyFilters(countries, filters.countriesId, filters.cityIds)
        SettingFiltersDB.applyFilters(genres, filters.genreIds, filters.subgenreIds)
    }

    override fun expandGenreList(isExpanded: Boolean) {
        launch(parent = jobs) {
            expandGenreList.send(!isExpanded)
        }
    }

    override fun expandCountryList(isExpanded: Boolean) {
        launch(parent = jobs) {
            expandCountryList.send(!isExpanded)
        }
    }
    override fun onCloseDialog() {
        launch(parent = jobs) {
            closeDialogChannel.send(null)
        }
    }
    private fun addHeadersToGenres(genres: List<Genre>): List<Genre> {
        return genres.toMutableList().apply {
            add(0, createGenreHeader())

            forEach {
                it.subganre = it.subganre.toMutableList().apply {
                    add(0, createSubgenreHeader(it.id))
                }
            }
        }
    }

    private fun addHeaders(countries: List<Country>): List<Country> {
        return countries.toMutableList().apply {
            add(0, createCountryHeader())

            forEach {
                it.cityList = it.cityList.toMutableList().apply {
                    add(0, createCityHeader(it.id))
                }
            }
        }
    }

    private fun createGenreHeader(): Genre {
        return Genre(Filtrable.ID_ALL, resourcesStorage.getAllGenresHeader())
    }

    private fun createSubgenreHeader(genreId: Long): Subgenre {
        return Subgenre(Filtrable.ID_ALL, resourcesStorage.getAllSubgenresHeader(), genreId)
    }

    private fun createCountryHeader(): Country {
        return Country(Filtrable.ID_ALL, resourcesStorage.getAllCountriesLabel())
    }

    private fun createCityHeader(countryId: Long): City {
        return City(Filtrable.ID_ALL, resourcesStorage.getAllCitiesLabel(), countryId)
    }

    override fun hasCheckedItems(): Boolean {
        return containsCheckedItems(countries) && containsCheckedItems(genres)
    }

    private fun containsCheckedItems(filtrableList: List<Filtrable>): Boolean {
        filtrableList.forEach { item ->
            if (item.checked) return true

            item.getChild()?.forEach {
                if (it.checked) return true
            }
        }

        return false
    }

    override fun onFiltersFragmentClose() {
        save()
        router.onFiltersFragmentClose()
    }

    override fun save() {
        launch(parent = jobs) {
            saveFilters()
        }
    }

    override fun saveFilters() {
        saveFilterJob?.cancel()
        saveFilterJob = launch(parent = jobs) {
            if (::countries.isInitialized && ::genres.isInitialized) {
                filtersInteractor.saveFilters(countries, genres, true)
            }
        }

    }
}
