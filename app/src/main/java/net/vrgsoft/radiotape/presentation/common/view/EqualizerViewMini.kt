package net.vrgsoft.radiotape.presentation.common.view

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.LinearInterpolator
import android.widget.LinearLayout
import net.vrgsoft.radiotape.R


class EqualizerViewMini : LinearLayout {

    private lateinit var musicBar1: View
    private lateinit var musicBar2: View
    private lateinit var musicBar3: View

    private var playingSet: AnimatorSet? = null
    private var stopSet: AnimatorSet? = null
    private var isAnimating: Boolean? = false

    private var foregroundColor: Int = 0
    private var duration: Int = 0


    constructor(context: Context) : super(context) {
        initViews()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setAttrs(context, attrs)
        initViews()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setAttrs(context, attrs)
        initViews()
    }

    private fun setAttrs(context: Context, attrs: AttributeSet) {
        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.EqualizerViewMini,
                0, 0)

        foregroundColor = a.getColor(R.styleable.EqualizerViewMini_foregroundColor, Color.BLACK)
        duration = a.getInt(R.styleable.EqualizerViewMini_animDuration, 3000)
        a.recycle()
    }

    private fun initViews() {
        LayoutInflater.from(context).inflate(R.layout.view_equalizer_mini, this, true)
        musicBar1 = findViewById(R.id.music_bar1)
        musicBar2 = findViewById(R.id.music_bar2)
        musicBar3 = findViewById(R.id.music_bar3)
        musicBar1.setBackgroundColor(foregroundColor)
        musicBar2.setBackgroundColor(foregroundColor)
        musicBar3.setBackgroundColor(foregroundColor)
        setPivots()
    }


    private fun setPivots() {
        musicBar1.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (musicBar1.height > 0) {
                    musicBar1.pivotY = musicBar1.height.toFloat()
                    if (Build.VERSION.SDK_INT >= 16) {
                        musicBar1.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                }

            }
        })
        musicBar2.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (musicBar2.height > 0) {
                    musicBar2.pivotY = musicBar2.height.toFloat()
                    if (Build.VERSION.SDK_INT >= 16) {
                        musicBar2.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                }

            }
        })
        musicBar3.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (musicBar3.height > 0) {
                    musicBar3.pivotY = musicBar3.height.toFloat()
                    if (Build.VERSION.SDK_INT >= 16) {
                        musicBar3.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                }

            }
        })
    }

    fun animateBars() {
        isAnimating = true
        if (playingSet == null) {
            val scaleYbar1 = ObjectAnimator.ofFloat(musicBar1, "scaleY", 0.2f, 0.8f, 0.1f, 0.1f, 0.3f, 0.1f, 0.2f, 0.8f, 0.7f, 0.2f, 0.4f, 0.9f, 0.7f, 0.6f, 0.1f, 0.3f, 0.1f, 0.4f, 0.1f, 0.8f, 0.7f, 0.9f, 0.5f, 0.6f, 0.3f, 0.1f)
            scaleYbar1.repeatCount = ValueAnimator.INFINITE
            val scaleYbar2 = ObjectAnimator.ofFloat(musicBar2, "scaleY", 0.2f, 0.5f, 1.0f, 0.5f, 0.3f, 0.1f, 0.2f, 0.3f, 0.5f, 0.1f, 0.6f, 0.5f, 0.3f, 0.7f, 0.8f, 0.9f, 0.3f, 0.1f, 0.5f, 0.3f, 0.6f, 1.0f, 0.6f, 0.7f, 0.4f, 0.1f)
            scaleYbar2.repeatCount = ValueAnimator.INFINITE
            val scaleYbar3 = ObjectAnimator.ofFloat(musicBar3, "scaleY", 0.6f, 0.5f, 1.0f, 0.6f, 0.5f, 1.0f, 0.6f, 0.5f, 1.0f, 0.5f, 0.6f, 0.7f, 0.2f, 0.3f, 0.1f, 0.5f, 0.4f, 0.6f, 0.7f, 0.1f, 0.4f, 0.3f, 0.1f, 0.4f, 0.3f, 0.7f)
            scaleYbar3.repeatCount = ValueAnimator.INFINITE

            playingSet = AnimatorSet()
            playingSet!!.playTogether(scaleYbar2, scaleYbar3, scaleYbar1)
            playingSet!!.duration = duration.toLong()
            playingSet!!.interpolator = LinearInterpolator()
            playingSet!!.start()

        } else if (Build.VERSION.SDK_INT < 19) {
            if (!playingSet!!.isStarted) {
                playingSet!!.start()
            }
        } else {
            if (playingSet!!.isPaused) {
                playingSet!!.resume()
            }
        }

    }

    fun stopBars() {
        isAnimating = false
        if (playingSet != null && playingSet!!.isRunning && playingSet!!.isStarted) {
            if (Build.VERSION.SDK_INT < 19) {
                playingSet!!.end()
            } else {
                playingSet!!.pause()
            }
        }

        if (stopSet == null) {
            // Animate stopping bars
            val scaleY1 = ObjectAnimator.ofFloat(musicBar1, "scaleY", 0.1f)
            val scaleY2 = ObjectAnimator.ofFloat(musicBar2, "scaleY", 0.1f)
            val scaleY3 = ObjectAnimator.ofFloat(musicBar3, "scaleY", 0.1f)
            stopSet = AnimatorSet()
            stopSet!!.playTogether(scaleY3, scaleY2, scaleY1)
            stopSet!!.duration = 200
            stopSet!!.start()
        } else if (!stopSet!!.isStarted) {
            stopSet!!.start()
        }
    }


}