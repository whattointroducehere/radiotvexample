package net.vrgsoft.radiotape.presentation.cabinet.equalizer

import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.media.AudioManager
import android.media.audiofx.Equalizer
import android.text.TextUtils
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.data.model.Band
import net.vrgsoft.radiotape.data.model.Preset
import net.vrgsoft.radiotape.data.repository.resources.ResourcesStorage
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.presentation.common.BaseViewModel
import net.vrgsoft.radiotape.utils.AppEqualizer
import net.vrgsoft.radiotape.utils.logError

class EqualizerViewModel(
        private val audioManager: AudioManager,
        private val router: EqualizerContract.Router,
        private val appEqualizer: AppEqualizer,
        private val settingsInteractor: SettingsInteractor,
        private val resources: ResourcesStorage
) : BaseViewModel(), EqualizerContract.ViewModel {

    override val openPresetList = ConflatedBroadcastChannel<List<Preset>>()
    override val showBandController = ConflatedBroadcastChannel<List<Band>>()
    override val showResetSettingsDialog = ConflatedBroadcastChannel<Boolean>()

    override val presetText = ObservableField<String>()
    override val volumeValue = ObservableInt()
    override val volumeMaxValue = ObservableInt()
    override val equalizerEnabled = ObservableBoolean()
    override val audioCompressionEnabled = ObservableBoolean()

    override val forwardPresetButtonVisibility = ObservableBoolean()
    override val rewindPresetButtonVisibility = ObservableBoolean()

    private lateinit var presets: List<Preset>
    private lateinit var settings: Equalizer.Settings
    private var preamp: Int

    private var savedVolume: Int? = null
    private var isTouchedByUser = false

    init {
        preamp = settingsInteractor.getEqualizerPreamp()
    }

    override fun bound() {
        notifyVolumeChanged()
        loadSettings()
        loadEqualizerProperties()
        addPropertyChangedCallbacks()
    }

    override fun selectPreset(which: Int, forceUpdate: Boolean, forceBandsReload: Boolean) {
        if (which >= 0) { //if user use standard preset
            if (presets.size > which) {
                updateCurrentPreset(presets[which], forceUpdate, forceBandsReload)
            }
        } else {
            updateCurrentPreset(getManualPreset(), forceUpdate, forceBandsReload)
        }
    }

    override fun openPresetList() {
        launch(parent = jobs) {
            openPresetList.send(presets)
        }
    }

    override fun forwardPreset() {
        val curPreset = settings.curPreset
        if (curPreset < presets.size) {
            selectPreset(curPreset + 1, true, true)
        }
    }

    override fun rewindPreset() {
        val curPreset = settings.curPreset
        if (curPreset >= 0) {
            selectPreset(curPreset - 1, true, true)
        }
    }

    override fun notifyVolumeChanged() {
        if (!isTouchedByUser) {
            launch(parent = jobs) {
                volumeValue.set(getDeviceVolume())
                volumeMaxValue.set(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC))
            }
        }
    }

    override fun onBandLevelChanged(band: Band) {
        if (band.position == PREAMP_BAND_ID) {
            updatePreampLevel(band.level)
            return
        }

        settings.bandLevels[band.position] = band.level.toShort()

        if (settings.curPreset.toInt() != MANUAL_PRESET_POSITION) {
            selectPreset(MANUAL_PRESET_POSITION, false, false)
        }

        saveSettings()
    }

    private fun updatePreampLevel(level: Int) {
        preamp = level
        settingsInteractor.setEqualizerPreamp(preamp)

    }

    override fun getLowerBandLevel() = appEqualizer.getLowerEqualizerBandLevel().toInt()

    override fun getUpperBandLevel() = appEqualizer.getUpperEqualizerBandLevel().toInt()

    override fun goBack() {
        router.goBack()
    }

    override fun muteVolume() {
        if (getDeviceVolume() == 0 && savedVolume != null) {
            unmuteVolume()
        } else {
            savedVolume = getDeviceVolume()
            setDeviceVolume(0)
        }
    }

    override fun unmuteVolume() {
        if (savedVolume != null) {
            setDeviceVolume(savedVolume!!)
            savedVolume = null
        }
    }

    override fun onResetClicked() {
        launch(parent = jobs) {
            showResetSettingsDialog.send(true)
        }
    }

    override fun resetSettings() {
        selectPreset(0, true, true)
    }

    private fun getDeviceVolume() = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)

    private fun loadSettings() {
        launch(parent = jobs) {
            val isEqualizerEnabled: Boolean = settingsInteractor.isEqualizerEnabled()
            val isAudioCompressionEnabled: Boolean = settingsInteractor.isAudioCompressionEnabled()

            equalizerEnabled.set(isEqualizerEnabled)
            audioCompressionEnabled.set(isAudioCompressionEnabled)
        }
    }

    private fun getManualPreset() = Preset(MANUAL_PRESET_POSITION, resources.getManualPresetString())

    private fun updateCurrentPreset(preset: Preset, forceUpdate: Boolean, forceBandsReload: Boolean) {
        settings.curPreset = preset.position.toShort()

        presetText.set(preset.title)

        val curPreset = settings.curPreset
        rewindPresetButtonVisibility.set(curPreset > 0)
        forwardPresetButtonVisibility.set(curPreset < presets.size - 1)

        if (forceUpdate) {
            appEqualizer.setPreset(preset.position.toShort())
            settings = appEqualizer.getSettings()
            saveSettings()
        }

        if (forceBandsReload) {
            launch(parent = jobs) {
                loadBands()
            }
        }
    }

    private fun addPropertyChangedCallbacks() {
        volumeValue.addOnPropertyChangedCallback(onPropertyChanged)
        equalizerEnabled.addOnPropertyChangedCallback(onPropertyChanged)
        audioCompressionEnabled.addOnPropertyChangedCallback(onPropertyChanged)
    }

    private fun loadEqualizerProperties() {
        launch(parent = jobs) {
            val equalizerProperties = settingsInteractor.getEqualizerProperties()
            settings = if (!TextUtils.isEmpty(equalizerProperties)) {
                Equalizer.Settings(equalizerProperties)
            } else {
                appEqualizer.getSettings()
            }

            val presetPosition: Int = settings.curPreset.toInt()
            this@EqualizerViewModel.presets = appEqualizer.getPresets()

            selectPreset(presetPosition, false, true)
        }
    }

    private val onPropertyChanged = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            onPropertyChanged(sender)
        }
    }

    private suspend fun loadBands() {
        try {
            val bands = appEqualizer.getBands().toMutableList()
            bands.add(0, createPreampBand())
            showBandController.send(bands)
        } catch (e: Exception) {
            logError("", "", e)
        }
    }

    private fun onPropertyChanged(sender: Observable?) {
        launch(parent = jobs) {
            when (sender) {
                volumeValue -> {
                    isTouchedByUser = true
                    setDeviceVolume(volumeValue.get())
                    delay(300L)
                    isTouchedByUser = false
                }
                equalizerEnabled -> {
                    val enabled = equalizerEnabled.get()
                    appEqualizer.setEqualizerEnabled(enabled)
                    settingsInteractor.setEqualizerEnabled(enabled)
                }
                audioCompressionEnabled -> {
                    settingsInteractor.setAudioCompressionEnabled(audioCompressionEnabled.get())
                }
            }
        }
    }

    private fun createPreampBand() = Band(PREAMP_BAND_ID, PREAMP_BAND_ID, preamp)

    private fun setDeviceVolume(value: Int) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, value, AudioManager.FLAG_PLAY_SOUND)
    }

    private fun saveSettings() {
        settingsInteractor.setEqualizerProperties(settings.toString())
    }

    companion object {
        const val MANUAL_PRESET_POSITION = -1
        const val PREAMP_BAND_ID = -1
    }
}
