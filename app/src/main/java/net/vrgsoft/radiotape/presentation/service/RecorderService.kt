package net.vrgsoft.radiotape.presentation.service

import android.content.Context
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.util.Log
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import net.vrgsoft.radiotape.TEMP_FILE_NAME
import net.vrgsoft.radiotape.data.pipeline.record.RecordFinished
import net.vrgsoft.radiotape.data.pipeline.record.RecordStarted
import net.vrgsoft.radiotape.domain.record.RecordInteractor
import net.vrgsoft.radiotape.domain.settings.SettingsInteractor
import net.vrgsoft.radiotape.presentation.common.BaseService
import net.vrgsoft.radiotape.utils.logDebug
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.URL

class RecorderService : BaseService() {
    override fun diModule() = Kodein.Module("RecorderService") {

    }

    private val settingsInteractor: SettingsInteractor by instance()
    private val recordInteractor: RecordInteractor by instance()

    private var isRecording = false
    private var fileOutputStream: FileOutputStream? = null
    private var job: Deferred<Unit>? = null
    private var startTime = 0L

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.getStringExtra(EXTRA_ACTION)) {
            RECORDING_START -> {
                startTime = System.currentTimeMillis()
                if (isRecording) return START_NOT_STICKY
                isRecording = true
                recordInteractor.sendRecordState(RecordStarted())
                job = async {
                    startRecording(intent.getStringExtra(EXTRA_RADIO_URL))
                }
            }
            RECORDING_STOP -> {
                val recordTime = System.currentTimeMillis() - startTime
                logDebug("RecorderService", recordTime.toString())
                if (!isRecording) return START_NOT_STICKY
                isRecording = false
                launch(parent = jobs) {
                    stopRecording()
                    trimToRealDuration(recordTime)
                    stopSelf()
                }
            }
        }
        return START_NOT_STICKY
    }

    private fun trimToRealDuration(recordTime: Long) {
        val file = File(filesDir, TEMP_FILE_NAME)
        val recordTimeSec = recordTime / 1000f
        val retriever = MediaMetadataRetriever()

        try {
            retriever.setDataSource(this, Uri.fromFile(file))

            val bitrate = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE).toLong()
            retriever.release()

            val destSize = (recordTimeSec * bitrate / 8).toLong()
            val tempFile = File(filesDir, TEMP_FILE_NAME + "1")
            val fileInputStream = FileInputStream(file)
            fileOutputStream = FileOutputStream(File(filesDir, TEMP_FILE_NAME + "1"))

            val buffer = ByteArray(1024)
            var bytesCopied = 0L
            var c: Int = fileInputStream.read(buffer)
            while (c != -1 && bytesCopied < destSize){
                bytesCopied += c
                if(bytesCopied - destSize > 0){
                    c = (bytesCopied - destSize).toInt()
                }
                fileOutputStream?.write(buffer, 0, c)
                c = fileInputStream.read(buffer)
            }

//        val array = ByteArray(destSize.toInt())
//        val c: Int = fileInputStream.read(array)
//        fileOutputStream?.write(array)

            fileInputStream.close()

            fileOutputStream?.flush()
            fileOutputStream?.close()
            fileOutputStream = null

            file.delete()
            tempFile.renameTo(file)

            recordInteractor.sendRecordState(RecordFinished(recordTime, file.absolutePath))
        } catch (e: Exception) {
            return
        }
    }

    private fun startRecording(radioUrl: String) {
        val url = URL(radioUrl + settingsInteractor.getQuality().postfix)
        val inputStream = url.openStream()

        fileOutputStream = FileOutputStream(File(filesDir, TEMP_FILE_NAME))

        var c: Int = inputStream.read()

        while (c != -1) {
            fileOutputStream?.write(c)
            c = inputStream.read()
        }
        fileOutputStream?.flush()
        fileOutputStream?.close()
        fileOutputStream = null
    }

    private fun stopRecording() {
        try {
            job?.cancel()
            job = null
            fileOutputStream?.flush()
            fileOutputStream?.close()
            fileOutputStream = null
        } catch (e: Exception) {

        }
    }

    override fun onDestroy() {
        Log.d("RecorderService", "onDestroy: " + System.currentTimeMillis().toString())
        stopRecording()
        super.onDestroy()
    }

    override fun onBind(intent: Intent?) = null

    companion object {
        private const val RECORDING_START = "recordingStart"
        private const val RECORDING_STOP = "recordingStop"
        private const val EXTRA_RADIO_URL = "radioUrl"
        private const val EXTRA_ACTION = "action"

        fun newStartRecordingIntent(context: Context, radioUrl: String?) = Intent(context, RecorderService::class.java)
                .putExtra(EXTRA_ACTION, RECORDING_START)
                .putExtra(EXTRA_RADIO_URL, radioUrl)

        fun newStopRecordingIntent(context: Context) = Intent(context, RecorderService::class.java).putExtra(EXTRA_ACTION, RECORDING_STOP)
    }
}
