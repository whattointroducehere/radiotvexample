package net.vrgsoft.radiotape.presentation.cabinet.filters.common

import android.view.LayoutInflater
import kotlinx.coroutines.experimental.channels.ConflatedBroadcastChannel
import net.vrgsoft.radiotape.data.model.Subgenre

class SubgenreAdapter(layoutInflater: LayoutInflater,
                      override val parentAdapter: GenreAdapter,
                      saveFilterChannel: ConflatedBroadcastChannel<Boolean>
) : BaseFiltersAdapter<Subgenre>(layoutInflater = layoutInflater,
        parentAdapter = parentAdapter,
        channel = saveFilterChannel) {

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE_SUBFILTER
    }
}
